/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : gipsy_ctry_it.h
** Author    : Leonid Savchenko
** Revision  : 1.0
** Updated   : 25-08-2020
**
** Description: Header for gipsy_ctry_it.c - This file contains the headers of the interrupt handlers.
**
*/


/* Revision Log:
**
** Rev 1.0  : 25-08-2020 -- Original version created.
**
**************************************************************************************
*/



/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef GIPSY_CTRY_IT_H
#define GIPSY_CTRY_IT_H

/* -----Includes -------------------------------------------------------------------*/

/* -----Definitions ----------------------------------------------------------------*/

/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/
void SysTick_Handler(void);
void CAN1_RX0_IRQHandler(void);

#ifdef _DEBUG_
void USART3_IRQHandler(void);
#endif

#endif /* GIPSY_CTRY_IT_H */

/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
