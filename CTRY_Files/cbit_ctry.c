/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : cbit_ctry.c
** Author    : Leonid Savchenko
** Revision  : 1.0
** Updated   : 19-10-20
**
** Description: This file contains procedures for CTRY's continues BIT
** 				.
*/


/* Revision Log:
**
** Rev 1.0  : 19-10-20 -- Original version created.
**
**************************************************************************************
*/


/* -----Includes ---------------------------------------------------------------------*/
#include "cbit_ctry.h"

#include "main.h"
#include "gipsy_hal_rcc.h"

#include "actuator_ctrl.h"
#include "current_sensor.h"
#include "fault_mngmnt_ctry.h"
#include "gpio_control_ctry.h"
#include "id_conf_ctry.h"
#include "module_init_ctry.h"
#include "motor_driver.h"
#include "system_params_ctry.h"
#include "vibration_meas.h"
#include "voltage_meas_ctry.h"

/* -----Definitions ----------------------------------------------------------------*/

/* -----Macros ---------------------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----Static Function prototypes -------------------------------------------------*/

/* -----Modules implementation -----------------------------------------------------*/




/*************************************************************************************
** Perform_CBIT - Performs CTRY's CBIT
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
void Perform_CBIT(void)
{
	/*Check 14.8V Input voltage source*/
	if(E_VOLT_TEST_FL == Test_OnBoard_Voltage(E_VOLT_14_8V_IN))
	{
		g_ModuleParams.Ctry_Failure_Desc |= CTRY_FLT_POWER_IN;
	}

	/*Check 5V voltage source*/
	if(E_VOLT_TEST_FL == Test_OnBoard_Voltage(E_VOLT_5V))
	{
		g_ModuleParams.Ctry_Failure_Desc |= CTRY_FLT_POWER_5V;
	}
	/*Check MDRV voltage when power switch is ON*/
	if(E_VOLT_TEST_FL == Test_OnBoard_Voltage(E_VOLT_14_8_MDRV_ON))
	{
		g_ModuleParams.Ctry_Failure_Desc |= CTRY_FLT_POW_SWITCH;
	}

	if(E_MON_TBL_FL == Monitor_Status_Table())
	{
		g_ModuleParams.Ctry_Failure_Desc |= CTRY_FLT_TABLE_MONIT;
	}

	/*Set CBIT result*/
	if(0x0000U == g_ModuleParams.Ctry_Failure_Desc)
	{
		g_ModuleParams.Cbit_Status = STATUS_OK; /*Mark CBIT status as OK*/
	}
	else
	{
		g_ModuleParams.Cbit_Status = STATUS_FL; /*Mark CBIT status as FAIL*/
	}
}

/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
