/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : voltage_meas_ctry.c
** Author    : Leonid Savchenko
** Revision  : 1.8
** Updated   : 07-05-2023
**
** Description: This file provides procedures for voltage tests and measurement
**              using  internal ADC.
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1  : 12-02-2020 -- Updated Test_OnBoard_Voltage()
** Rev 1.2  : 19-02-2020 -- Updated Test_OnBoard_Voltage() as a result of ADC HAL driver update
** Rev 1.3  : 24-08-2020 -- Fixed bug in Test_OnBoard_Voltage() at test status check
** Rev 1.4  : 09-10-2020 -- Added VOLTAGE_14_8V_MDRV_OFF voltage source for power switch testing
** Rev 1.5  : 20-11-2020 -- Static analysis update
** Rev 1.6  : 27-06-2021 -- Voltages limits updated
** Rev 1.7  : 11-07-2021 -- Added doxygen comments
** Rev 1.8  : 28-05-2023 -- Voltage 5VDC limits updated, increased number of samples to 20
**************************************************************************************
*/

/* -----Includes ---------------------------------------------------------------------*/
#include "voltage_meas_ctry.h"
#include "gipsy_hal_adc.h"
#include "mcu_config_ctry.h"

/* -----Definitions ----------------------------------------------------------------*/

#define VOLT_MEAS_NUM_OF_SAMPLES       20U


#define VOLTAGE_5V_AMP_FACTOR 			2
#define VOLTAGE_14_8V_IN_AMP_FACTOR      5.99
#define VOLTAGE_14_8V_MDRV_AMP_FACTOR 	 11

#define VOLTAGE_5V_LIM_MAX			     5.5
#define VOLTAGE_14_8V_LIM_MAX            16.0
#define VOLTAGE_14_8V_MDRV_ON_LIM_MAX 	 16.0
#define VOLTAGE_14_8V_MDRV_OFF_LIM_MAX 	 5.3

#define VOLTAGE_5V_LIM_MIN			     4.2
#define VOLTAGE_14_8V_LIM_MIN            13.0
#define VOLTAGE_14_8V_MDRV_ON_LIM_MIN 	 13.0
#define VOLTAGE_14_8V_MDRV_OFF_LIM_MIN 	 0.0

#define VOLTAGE_ERROR_VAL            99.0


/* -----Macros ---------------------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/*!Specifies temperature sensors general parameters (Range: var type | Units: see var declaration)*/
/*!\sa VoltageValue - Set function: Test_OnBoard_Voltage()*/
/*!\sa VoltageValue - Get function: CAN_Message_Transmit(), Test_OnBoard_Voltage()*/
/*!\sa MeasurementStatus - Set function: Test_OnBoard_Voltage()*/
/*!\sa MeasurementStatus - Get function: Test_OnBoard_Voltage()*/
/*!\sa VoltageTestResult - Set function: Test_OnBoard_Voltage()*/
/*!\sa VoltageTestResult - Get function: CAN_Message_Transmit()*/

st_VoltageMeasParameters g_VoltageMeasurement[E_VOLT_MAX_TYPES]=
{
		{.VoltageValue = VOLTAGE_ERROR_VAL,
         .MeasurementStatus = E_VOLT_MEAS_FL,
		 .VoltageTestResult = E_VOLT_TEST_FL,
		 .VoltageLimitMax = VOLTAGE_5V_LIM_MAX,
		 .VoltageLimitMin = VOLTAGE_5V_LIM_MIN
		},
		{.VoltageValue = VOLTAGE_ERROR_VAL,
         .MeasurementStatus = E_VOLT_MEAS_FL,
		 .VoltageTestResult = E_VOLT_TEST_FL,
		 .VoltageLimitMax = VOLTAGE_14_8V_LIM_MAX,
		 .VoltageLimitMin = VOLTAGE_14_8V_LIM_MIN
		},
		{.VoltageValue = VOLTAGE_ERROR_VAL,
         .MeasurementStatus = E_VOLT_MEAS_FL,
		 .VoltageTestResult = E_VOLT_TEST_FL,
		 .VoltageLimitMax = VOLTAGE_14_8V_MDRV_ON_LIM_MAX,
		 .VoltageLimitMin = VOLTAGE_14_8V_MDRV_ON_LIM_MIN
		},
		{.VoltageValue = VOLTAGE_ERROR_VAL,
         .MeasurementStatus = E_VOLT_MEAS_FL,
		 .VoltageTestResult = E_VOLT_TEST_FL,
		 .VoltageLimitMax = VOLTAGE_14_8V_MDRV_OFF_LIM_MAX,
		 .VoltageLimitMin = VOLTAGE_14_8V_MDRV_OFF_LIM_MIN
		}
};

struct st_VoltageMeasHwParameters
{
	const uint32_t ADC_Channel;
	const float AmplificationFactor;
}VoltHwParams[E_VOLT_MAX_TYPES] =


{
		{.ADC_Channel = ADC_CHANNEL_4,     .AmplificationFactor = VOLTAGE_5V_AMP_FACTOR},
		{.ADC_Channel = ADC_CHANNEL_5,     .AmplificationFactor = VOLTAGE_14_8V_IN_AMP_FACTOR},
		{.ADC_Channel = ADC_CHANNEL_2,     .AmplificationFactor = VOLTAGE_14_8V_MDRV_AMP_FACTOR},
		{.ADC_Channel = ADC_CHANNEL_2,     .AmplificationFactor = VOLTAGE_14_8V_MDRV_AMP_FACTOR}
};
/* -----Static Function prototypes -------------------------------------------------*/

/* -----Modules implementation -----------------------------------------------------*/

/*************************************************************************************
** Test_OnBoard_Voltage - This function measures and tests on board voltage sources.
**
** Params : e_VoltageSourceType voltage_type - Type of tested voltage.
**
** Returns: e_VoltageTestStatus - Status of voltage measurement test.(E_VOLT_MEAS_FL , E_VOLT_MEAS_OK)
** Notes:
*************************************************************************************/
e_VoltageTestStatus Test_OnBoard_Voltage(e_VoltageSourceType voltage_type)
{
	uint32_t adc_converted_value = 0U;
	uint16_t sum_of_samples = 0U;
	uint16_t voltage_samples[VOLT_MEAS_NUM_OF_SAMPLES] = {0U};
	uint8_t valid_samples_counter = 0U;
	float voltage_value = 0.0;

	/*Perform Voltage measurement*/
	for(uint8_t sample_num = 0U; sample_num < VOLT_MEAS_NUM_OF_SAMPLES; sample_num++)
	{
		/*Start the conversion process*/
		if(HAL_OK == HAL_ADC_Perform_Conversion(VoltHwParams[voltage_type].ADC_Channel, &adc_converted_value))
		{
			/*Get the converted value */
			voltage_samples[sample_num] = (uint16_t)(adc_converted_value & 0x00000FFFU);/*make sure to get 12 bit of data*/
			valid_samples_counter++;
			/*Compute valid samples sum*/
			sum_of_samples += voltage_samples[sample_num];
		}
		else
		{
			/*Mark sample as ERROR in case of conversion start failed*/
			voltage_samples[sample_num] = VOLTAGE_ERROR_VAL;
		}
	}

	/*Compute measured current value in case that valid samples available*/
	if(valid_samples_counter > 0U)
	{
		/*Calculate average value from measured voltage samples and convert it to measured voltage*/
		voltage_value = (sum_of_samples/valid_samples_counter) * ACD_RESOLUTION_VOLT;
		/*Get real  voltage value by removing External Amplification factor*/
		voltage_value = voltage_value * VoltHwParams[voltage_type].AmplificationFactor;

		if(voltage_value < 0U)
		{
			voltage_value = 0.0;
		}
		/*Save measured voltage value*/
		g_VoltageMeasurement[voltage_type].VoltageValue = voltage_value;
		g_VoltageMeasurement[voltage_type].MeasurementStatus = E_VOLT_MEAS_OK;
	}
	else
	{
		/*Set error measurement in case that no valid samples produced*/
		g_VoltageMeasurement[voltage_type].VoltageValue = VOLTAGE_ERROR_VAL;
		g_VoltageMeasurement[voltage_type].MeasurementStatus = E_VOLT_MEAS_FL;

	}
	/*Perform Test of measured voltage*/
	/*Check whether the voltage measurement succeeded*/
	if(E_VOLT_MEAS_OK == g_VoltageMeasurement[voltage_type].MeasurementStatus)
	{
		/*Check whether the measured voltage is inside limits*/
		if((g_VoltageMeasurement[voltage_type].VoltageLimitMin <= g_VoltageMeasurement[voltage_type].VoltageValue ) && \
		   (g_VoltageMeasurement[voltage_type].VoltageLimitMax >= g_VoltageMeasurement[voltage_type].VoltageValue))
		{
			g_VoltageMeasurement[voltage_type].VoltageTestResult = E_VOLT_TEST_OK;
		}
		else
		{
			g_VoltageMeasurement[voltage_type].VoltageTestResult = E_VOLT_TEST_FL;
		}
	}
	else
	{
		g_VoltageMeasurement[voltage_type].VoltageTestResult = E_VOLT_TEST_FL;
	}
	return g_VoltageMeasurement[voltage_type].VoltageTestResult;
}
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
