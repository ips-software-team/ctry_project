/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC,CTRY
**
** Filename  : wdog.c
** Author    : Leonid Savchenko
** Revision  : 1.4
** Updated   : 29-09-2021
**
** Description: This file contains procedures for Watch-Dog control and test.
**
** Notes:
*/


/* Revision Log:
**
** Rev 1.0  : 28-07-2020 -- Original version created.
** Rev 1.1  : 21-11-2020 -- Updated due to verification remarks
** Rev 1.2  : 27-06-2021 -- Updated due to integration remarks
** Rev 1.3  : 11-07-2021 -- Added doxygen comments
** 						 -- Updated WDOG_Perform_Test()
** Rev 1.4  : 29-09-2021 -- Updated WDOG_Perform_Test() - Added Reset loop interval
** 							value stored in RTC back domain(non volatile) register
**************************************************************************************
*/

/* -----Includes ---------------------------------------------------------------------*/
#include "wdog.h"

#include "gipsy_hal_rcc.h"
#ifdef _DIMC_
#include "mcu_config_dimc.h"
#elif defined (_CTRY_)
#include "mcu_config_ctry.h"
#else
#endif
/* -----Definitions ----------------------------------------------------------------*/

#define WDOG_PULSE_DELAY  200U /*"for loop" end value for making short pulse to strobe WDOG*/

/*Watch-dog Timeout Period tWD defined in data sheet is:
 * minimal = 1.12 second
 * typical = 1.60 second
 * maximal = 2.40 second
 * */
#define WDOG_TEST_LOOP_STEP_INTERVAL_MILIS 100U
#define WDOG_TEST_LOOP_MAX_STEPS 28U /*Loop maximal period : 2.8 second*/
#define WDOG_TEST_RESULT_STEPS_MAX_LIMIT 25U /*WDOG Test maximal "kick" time interval limit 2.5 second*/


/* Backup register 0 address */
#define BCKUP_REG_0_ADDRESS		0x40002850U

/*Reset flags masks located in RCC->CSR register*/
#define POWER_RESET_BIT_MASK	0x0E000000U
#define NRST_RESET_BIT_MASK		0x04000000U
/* -----Macros ---------------------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/
static e_WDOG_TestResult WDOG_Test_Result = E_WDOG_TEST_FAILED;

/*!Specifies MCU's reset source (Range: var type | Units: None)*/
/*!\sa Set function: MCU_Periph_Config()*/
/*!\sa Get function: WDOG_Perform_Test()*/
uint32_t g_Reset_Source = 0x00U;
/* -----External variables ---------------------------------------------------------*/
/* -----Static Function prototypes -------------------------------------------------*/

/* -----Modules implementation -----------------------------------------------------*/

/*************************************************************************************
** WDOG_Perform_Test - Performs on-board WDOG test. Test check correct WDOG expiration period.
**
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
void WDOG_Perform_Test(void)
{
	e_WDOG_TestResult test_result = E_WDOG_TEST_PASSED;
	uint32_t wdog_test_loop_cnt = 0U;
	uint32_t *bckup_reg_0 = (uint32_t *)BCKUP_REG_0_ADDRESS;


	WDOG_Trigger();/*refresh watch-dog before the test to eliminate INIT time period*/

	/*In case of power up reset, perform wdog test*/
	if (POWER_RESET_BIT_MASK == (g_Reset_Source & POWER_RESET_BIT_MASK))/*These bits are set by hardware when a power reset occurs*/
	{
		for(uint16_t loop_counter = 0U; loop_counter < WDOG_TEST_LOOP_MAX_STEPS; loop_counter++)
		{
			HAL_Delay(WDOG_TEST_LOOP_STEP_INTERVAL_MILIS);

			*bckup_reg_0 = loop_counter;
		}
		/*WDOG did not reseted the MCU within required time interval -> mark test as FAILED*/
		test_result = E_WDOG_TEST_FAILED;
	}
	/*In case of external NRST reset, perform check of WDOG expiration time period*/
	else if(NRST_RESET_BIT_MASK == (g_Reset_Source & NRST_RESET_BIT_MASK))/*This bit is set by hardware when a reset from the NRST pin occurs*/
	{
		wdog_test_loop_cnt = *bckup_reg_0; /*Read loop interval value stored in RTC back domain(non volatile) register*/

		/*Check if WDOG expiration time period is within the limits*/
		if(WDOG_TEST_RESULT_STEPS_MAX_LIMIT >= wdog_test_loop_cnt)
		{
			test_result = E_WDOG_TEST_PASSED;
		}
		else
		{
			test_result = E_WDOG_TEST_FAILED;
		}
	}
	else
	{
		/*Reset source undefined -> mark test as FAILED*/
		test_result = E_WDOG_TEST_UNDEF_RST_SOURCE;
	}
	/*Save WFOG test result*/
	WDOG_Test_Result = test_result;
	*bckup_reg_0 = 0U;/*Reset loop interval value stored in RTC back domain(non volatile) register*/
}
/*************************************************************************************
** WDOG_Get_Test_Result - Gets Watch-Dog Test Result
**
** Params : None
**
** Returns: e_WDOG_TestResult - Watch-Dog Test result.
** Notes:
*************************************************************************************/
e_WDOG_TestResult WDOG_Get_Test_Result(void)
{
	return WDOG_Test_Result;
}
/*************************************************************************************
** WDOG_Trigger - Triggers Watch Dog by strobing WDI pin
**
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
void WDOG_Trigger(void)
{

	HAL_GPIO_WritePin(WDI_PORT,WDI_PIN,E_GPIO_PIN_RESET); /*Make sure WDI_PIN is LOW*/
	HAL_GPIO_WritePin(WDI_PORT,WDI_PIN,E_GPIO_PIN_SET);   /*Set WDI_PIN to HIGH state*/
	/*Wait to form short pulse*/
	for(uint16_t delay = 0U; delay < WDOG_PULSE_DELAY ; delay++)
	{
		__NOP();
	}
	HAL_GPIO_WritePin(WDI_PORT,WDI_PIN,E_GPIO_PIN_RESET); /*Set WDI_PIN to LOW steady state*/
}
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
