/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : fault_mngmnt_ctry.c
** Author    : Leonid Savchenko
** Revision  : 1.2
** Updated   : 30-05-2023
**
** Description: This file contains procedures for CTRY's fault management
** 				.
*/


/* Revision Log:
**
** Rev 1.0  : 19-10-2020 -- Original version created.
** Rev 1.1  : 27-06-2021 -- Updated due to integration inputs, remove SW errors counter
** Rev 1.2  : 30-05-2023 -- Removed cbit header include
**************************************************************************************
*/


/* -----Includes ---------------------------------------------------------------------*/
#include "fault_mngmnt_ctry.h"
#include "main.h"
#include "gipsy_hal_rcc.h"
#include "actuator_ctrl.h"
#include "current_sensor.h"
#include "gpio_control_ctry.h"
#include "id_conf_ctry.h"
#include "module_init_ctry.h"
#include "motor_driver.h"
#include "system_params_ctry.h"
#include "vibration_meas.h"
#include "voltage_meas_ctry.h"

/* -----Definitions ----------------------------------------------------------------*/

/* -----Macros ---------------------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----Static Function prototypes -------------------------------------------------*/

/* -----Modules implementation -----------------------------------------------------*/


/*************************************************************************************
** CTRY_Fault_Management - Perform CTRY's fault management during STBY/SAFE modes
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
void CTRY_Fault_Management(void)
{
	HAL_StatusTypeDef hal_status = HAL_OK;

	/*Check system table monitoring fault*/
	if(CTRY_FLT_TABLE_MONIT == (g_ModuleParams.Ctry_Failure_Desc & CTRY_FLT_TABLE_MONIT))
	{
		Activate_Fault_Channel_C();
	}
	/*Check system sanity monitoring fault*/
	if(CTRY_FLT_SANITY_MONIT == (g_ModuleParams.Ctry_Failure_Desc & CTRY_FLT_SANITY_MONIT))
	{
		Activate_Fault_Channel_C();
	}

	/*Check CTRY's status as marked in system status table*/
	 if(E_CTRY_FL == Get_CTRY_Status(Get_CTRY_ID()))
	 {
		 g_ModuleParams.ModuleMode = E_SAFE_MODE;
		 g_ModuleParams.Cbit_Status = STATUS_FL; /*Mark CBIT status as FAIL*/
	 }


    /*Check if critical failure detected*/
	if((E_SAFE_MODE == Get_Module_Mode()) || \
	   (CTRY_FLT_POWER_IN == (g_ModuleParams.Ctry_Failure_Desc & CTRY_FLT_POWER_IN)) ||\
	   (CTRY_FLT_POWER_5V == (g_ModuleParams.Ctry_Failure_Desc & CTRY_FLT_POWER_5V)) ||\
	   (CTRY_FLT_POW_SWITCH == (g_ModuleParams.Ctry_Failure_Desc & CTRY_FLT_POW_SWITCH)) ||\
	   (CTRY_FLT_ACCELEROMETER == (g_ModuleParams.Ctry_Failure_Desc & CTRY_FLT_ACCELEROMETER)) ||\
	   (CTRY_FLT_CURR_CONSUMPTION == (g_ModuleParams.Ctry_Failure_Desc & CTRY_FLT_CURR_CONSUMPTION)) ||\
	   (CTRY_FLT_SW == (g_ModuleParams.Ctry_Failure_Desc & CTRY_FLT_SW)) ||\
	   (CTRY_FLT_OVER_TEMP == (g_ModuleParams.Ctry_Failure_Desc & CTRY_FLT_OVER_TEMP)))
	{

		/*Critical failure detected*/
		g_ModuleParams.ModuleMode = E_SAFE_MODE;
		g_ModuleParams.Cbit_Status = STATUS_FL; /*Mark CBIT status as FAIL*/
		Activate_Fault_Channel_C();
		MDRV_Shut_Down(MAX_NUM_OF_MOTOR_DRIVERS); /*Disable all motor drivers*/
		/*Update CTRY status to FL in case that CTRY ID is valid*/
		if(0x0000U == (g_ModuleParams.Pbit_Failure_Desc & PBIT_FLT_CTRY_ID))
		{
			/*Critical failure detected - mark CTRY status as FL and all actuators as FL*/
			hal_status = Set_CTRY_Status(Get_CTRY_ID(),E_CTRY_FL);
		}
		Set_Motor_PWR_EN(E_GPIO_PIN_RESET); /*Turn OFF power switch*/
	}

	if(HAL_OK != hal_status)
	{
		/*Input parameters failure -> increment SW error counter*/
		Increment_SW_Error_Counter();
	}
}


/*************************************************************************************
** Increment_SW_Error_Counter - Sets SW error detection flag in CTRY failure descriptor
**
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
void Increment_SW_Error_Counter(void)
{
	/*Input parameters failure = Software Failure */
	g_ModuleParams.Ctry_Failure_Desc |= CTRY_FLT_SW;
}
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
