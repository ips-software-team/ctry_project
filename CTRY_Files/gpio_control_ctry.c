/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : gpio_control_ctry.c
** Author    : Leonid Savchenko
** Revision  : 1.1
** Updated   : 15-03-2020
**
** Description: This file contains function for control and monitoring GPIOs.
**
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1  : 15-03-2020 -- Added Activate_Fault_Channel_C()
**************************************************************************************
*/


/* -----Includes ---------------------------------------------------------------------*/
#include "gpio_control_ctry.h"

#include "mcu_config_ctry.h"
#include "module_init_ctry.h"

/* -----Definitions ----------------------------------------------------------------*/

/* -----Macros ---------------------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Static Function prototypes -------------------------------------------------*/

/* -----Modules implementation -----------------------------------------------------*/


/*************************************************************************************
** Set_Motor_PWR_EN - Controls MOTOR_PWR_EN output pin
**
** Params : GPIO_PinState pin_state - Required pin's state (SET/RESET)
**
** Returns: None.
** Notes:
*************************************************************************************/
void Set_Motor_PWR_EN(GPIO_PinState pin_state)
{
	HAL_GPIO_WritePin(MOTOR_PWR_EN_PORT,MOTOR_PWR_EN_PIN,pin_state);
}

/*************************************************************************************
** Activate_Fault_Channel_C - Activation of Fault Channel C
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
void Activate_Fault_Channel_C(void)
{
	Fault_Channel_C_Control(E_FLT_CH_C_ON);

	/*Update g_ModuleParams.Flt_Ch_C_Status global variable*/
	g_ModuleParams.Flt_Ch_C_Status = 1U;
}
/*************************************************************************************
** Fault_Channel_C_Control - Control of Fault Channel C Output pin
**
** Params : e_Flt_Ch_C_State ch_c_state - Required Fault channel's state (E_LFT_CH_C_OFF/E_LFT_CH_C_OFF)
**
** Returns: None
** Notes:
*************************************************************************************/
void Fault_Channel_C_Control(e_Flt_Ch_C_State ch_c_state)
{
	HAL_GPIO_WritePin(CPU_CON_TRAY_FAULT_PORT,CPU_CON_TRAY_FAULT_PIN,(GPIO_PinState)ch_c_state);
}
/*************************************************************************************
** Read_MotOper_Input - Reads MOTOR_OPERATE input
**
** Params : None
**
** Returns: GPIO_PinState - State of the input pin
** Notes:
*************************************************************************************/
GPIO_PinState Read_MotOper_Input(void)
{
	return HAL_GPIO_ReadPin(MOTOR_OPERATE_PORT,MOTOR_OPERATE_PIN);
}
/*************************************************************************************
** Manage_OnBoard_Leds - On-Board Staus Leds management.
** 						1. Toggle SW_RUN_LED (LED1)
** 						2. Manage FLT_LED Status (LED2).
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
void Manage_OnBoard_Leds(void)
{
	HAL_GPIO_TogglePin(LED_PORT,SW_RUN_LED_PIN);/*Toggle SW_RUN Led*/
	/*Set state of FLT_LED*/

	/*FLT_LED management*/
	if((0U != g_ModuleParams.Cbit_Status) || (0U != g_ModuleParams.Pbit_Status)  ||\
	  (0U != g_ModuleParams.Flt_Ch_C_Status))
	{
		HAL_GPIO_WritePin(LED_PORT,FLT_LED_PIN,E_GPIO_PIN_RESET);/*Turn FLT_LED - ON*/
	}


}
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
