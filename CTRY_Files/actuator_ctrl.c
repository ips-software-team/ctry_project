/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : actuator_ctrl.c
** Author    : Leonid Savchenko
** Revision  : 1.11
** Updated   : 07-05-2023
**
** Description: This file contains procedures for CTRY's actuators control and monitoring
** 				.
*/


/* Revision Log:
**
** Rev 1.0  : 19-10-2020 -- Original version created.
** Rev 1.1  : 05-11-2020 -- Updated Monitor_MOTOR_OPER_Input_State(),Received_Actuators_Params_Sanity_Monitoring()
** Rev 1.2  : 24-12-2020 -- Updated due to verification remarks
** Rev 1.3  : 27-06-2021 -- Updated due integration inputs
** Rev 1.4  : 29-06-2021 -- Updated due to verification remarks
** Rev 1.5  : 11-07-2021 -- Added doxygen comments,  variable g_DIMC_Oper_Mode_Active changed to project scope
** Rev 1.6  : 21-07-2021 -- Updated Received_Actuators_Params_Sanity_Monitoring() - added activation mode check
** Rev 1.7  : 25-07-2021 -- Added g_OPER_Start_Time variable
** Rev 1.8  : 16-08-2021 -- Updated g_OPER_Start_Time description
** Rev 1.9  : 29-08-2021 -- Updated Monitor_MOTOR_OPER_Input_State()
** Rev 1.10  : 21-09-2021 -- Updated Mot_Oper_Input_Previous_State,Mot_Oper_Input_Current_State initial values to RESET
** Rev 1.11  : 28-05-2023 -- Updated Monitor_MOTOR_OPER_Input_State() to fix bug of Sanity check failure when entering
**                           OPER mode after partial IBIT mode.
**                           Added Actuators de-activation functionality to Actuators_Activation_Management()
**                           Updated Perform_Actuators_Deactivation() - De-activation time is set only when actuator was active
**                           Updated Actuators_Activation_Management() - changed if value
**************************************************************************************
*/


/* -----Includes ---------------------------------------------------------------------*/
#include "actuator_ctrl.h"
#include "fault_mngmnt_ctry.h"
#include "gpio_control_ctry.h"
#include "id_conf_ctry.h"
#include "module_init_ctry.h"
#include "motor_driver.h"
#include "system_params_ctry.h"
#include "gipsy_hal_rcc.h"
#include "current_sensor.h"

/* -----Definitions ----------------------------------------------------------------*/

#define ACT_NUM_PARM_MAX_VALUE      (uint8_t)7U
#define ACT_NUM_PARM_MIN_VALUE      (uint8_t)1U
#define ACT_ON_REQ_TIME_MAX_MILIS   (uint32_t)5200U
#define ACT_ON_REQ_TIME_MIN_MILIS   (uint32_t)4800U

#define ACT_IBIT_MODE_SPPED_VALUE_PERCENT    (uint8_t)30U
#define NUM_OF_ACT_PAIRS        47U
/* -----Macros ---------------------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/
extern st_CurrSensData g_MeasuredCurrent;
/* -----Global variables -----------------------------------------------------------*/
st_ActuatorsPair g_ActuatorsPair = {0U};
uint8_t Active_Actuator_Num = (uint8_t)NO_ACTIVE_ACTUATOR;
GPIO_PinState Mot_Oper_Input_Previous_State = E_GPIO_PIN_RESET;
GPIO_PinState Mot_Oper_Input_Current_State = E_GPIO_PIN_RESET;

uint8_t Act_Pair_Oper_Pointer = 0U;

/*!Specifies actuator's deactivation time (Range: var type | Units: milliseconds)*/
/*!\sa Set function: Perform_Actuators_Deactivation()*/
/*!\sa Get function: Periodic_Tasks_Scheduler()*/
uint32_t g_Act_Deactivation_Time = 0U;

/*!Specifies system OPER Mode start time (Range: var type | Units: milliseconds)*/
/*!\sa Set function: Monitor_MOTOR_OPER_Input_State(), Actuators_Activation_Management()*/
/*!\sa Get function: Periodic_Tasks_Scheduler()*/
uint32_t g_OPER_Start_Time = 0U;

/*!Specifies system OPER Mode (Range: 0 - 1 | Units: None)*/
/*!\sa Set function: Monitor_MOTOR_OPER_Input_State()*/
/*!\sa Get function: Periodic_Tasks_Scheduler(), Received_Actuators_Params_Sanity_Monitoring()*/
uint8_t g_DIMC_Oper_Mode_Active = FALSE;

const ActParams_st ACT_PAIR_ARRAY[NUM_OF_ACT_PAIRS][NUM_OF_WINGS] =
{
	/*  Right wing | Left wing*/
	/*   CTRY | ACT	     CTRY | ACT**/
		{{0U,	4U},	{1U,	6U}},/*Pair_01*/
		{{0U,	6U},	{1U,	4U}},/*Pair_02*/
		{{0U,	1U},	{1U,	3U}},/*Pair_03*/
		{{0U,	3U},	{1U,	1U}},/*Pair_04*/
		{{2U,	6U},	{3U,	4U}},/*Pair_05*/
		{{2U,	4U},	{3U,	6U}},/*Pair_06*/
		{{4U,	1U},	{5U,	3U}},/*Pair_07*/
		{{4U,	4U},	{5U,	6U}},/*Pair_08*/
		{{4U,	3U},	{5U,	1U}},/*Pair_09*/
		{{6U,	1U},	{7U,	3U}},/*Pair_10*/
		{{6U,	3U},	{7U,	1U}},/*Pair_11*/
		{{6U,	4U},	{7U,	6U}},/*Pair_12*/
		{{6U,	6U},	{7U,	4U}},/*Pair_13*/
		{{8U,	3U},	{9U,	1U}},/*Pair_14*/
		{{8U,	1U},	{9U,	3U}},/*Pair_15*/
		{{8U,	5U},	{9U,	5U}},/*Pair_16*/
		{{8U,	4U},	{9U,	6U}},/*Pair_17*/
		{{8U,	6U},	{9U,	4U}},/*Pair_18*/
		{{10U,	1U},	{11U,	3U}},/*Pair_19*/
		{{10U,	3U},	{11U,	1U}},/*Pair_20*/
		{{10U,	2U},	{11U,	2U}},/*Pair_21*/
		{{10U,	6U},	{11U,	4U}},/*Pair_22*/
		{{10U,	4U},	{11U,	6U}},/*Pair_23*/
		{{12U,	1U},	{13U,	3U}},/*Pair_24*/
		{{12U,	3U},	{13U,	1U}},/*Pair_25*/
		{{12U,	2U},	{13U,	2U}},/*Pair_26*/
		{{12U,	5U},	{13U,	5U}},/*Pair_27*/
		{{12U,	4U},	{13U,	6U}},/*Pair_28*/
		{{12U,	6U},	{13U,	4U}},/*Pair_29*/
		{{14U,	1U},	{15U,	3U}},/*Pair_30*/
		{{14U,	3U},	{15U,	1U}},/*Pair_31*/
		{{14U,	2U},	{15U,	2U}},/*Pair_32*/
		{{14U,	5U},	{15U,	5U}},/*Pair_33*/
		{{14U,	4U},	{15U,	6U}},/*Pair_34*/
		{{14U,	6U},	{15U,	4U}},/*Pair_35*/
		{{16U,	1U},	{17U,	3U}},/*Pair_36*/
		{{16U,	3U},	{17U,	1U}},/*Pair_37*/
		{{16U,	2U},	{17U,	2U}},/*Pair_38*/
		{{16U,	5U},	{17U,	5U}},/*Pair_39*/
		{{16U,	4U},	{17U,	6U}},/*Pair_40*/
		{{16U,	6U},	{17U,	4U}},/*Pair_41*/
		{{18U,	1U},	{19U,	3U}},/*Pair_42*/
		{{18U,	3U},	{19U,	1U}},/*Pair_43*/
		{{18U,	2U},	{19U,	2U}},/*Pair_44*/
		{{18U,	5U},	{19U,	5U}},/*Pair_45*/
		{{18U,	4U},	{19U,	6U}},/*Pair_46*/
		{{18U,	6U},	{19U,	4U}} /*Pair_47*/
};

/* -----Static Function prototypes -------------------------------------------------*/

/* -----Modules implementation -----------------------------------------------------*/

/*************************************************************************************
** Actuators_Activation_Management - Manages actuators activation after parameters
** 									 reception via CANBUs.
** 									 Performs system table sanity monitoring in OPER mode.
**
** Params : None
**
** Returns: e_ActuatorActivationState - Result of actuators activation management:
** 										1. Actuator activated
** 										2. No change in activation.
** Notes:
*************************************************************************************/

e_ActuatorActivationState Actuators_Activation_Management(void)
{
	e_CtrySanityCheckResult sanity_check_result = E_SANITY_OK;
	e_ActuatorActivationState activation_state = E_ACT_NO_CHANGE;
	HAL_StatusTypeDef hal_status = HAL_OK;

	if(E_ACT_PARAMS_UPDATED == g_ActuatorsPair.ActParamsStatus)
	{
		Perform_Actuators_Deactivation();
		/*Check parameters validity and perform sanity check*/
		sanity_check_result = Received_Actuators_Params_Sanity_Monitoring();
		g_ActuatorsPair.ActParamsStatus = E_ACT_PARAMS_CHECKED;
		g_OPER_Start_Time = 0U; /*Disable check of the time interval between OPER mode entry and first activation command reception*/
		if(E_SANITY_OK == sanity_check_result)
		{
			for(uint8_t wing_type = 0U; (NUM_OF_WINGS > wing_type) && (E_ACT_NO_CHANGE == activation_state) && (HAL_OK == hal_status); wing_type++)
			{
				/*Check if actuator activation required in current CTRY*/
				if((Get_CTRY_ID() == g_ActuatorsPair.ActParamsCurrent[wing_type].CtryId) &&\
						(DO_NOT_ACTIVATE_MOTOR != g_ActuatorsPair.ActParamsCurrent[wing_type].ActNum))
				{
					/*Check Actuator's MDRV*/
					if((uint8_t)E_MDRV_OK == (uint8_t)MDRV_ID_Check((uint8_t)g_ActuatorsPair.ActParamsCurrent[wing_type].ActNum))
					{
						/*Activate actuator with speed value received via CANBUS*/
						MDRV_Activate_Motor((uint8_t)g_ActuatorsPair.ActParamsCurrent[wing_type].ActNum,(uint8_t)g_ActuatorsPair.ActParamsCurrent[wing_type].ActRpm);
						Set_Active_Actuator_Num(g_ActuatorsPair.ActParamsCurrent[wing_type].ActNum);

						activation_state = E_ACT_ACTIVATED;
					}
					else /*Actuator's MDRV is fail*/
					{
						/*Mark active actuator as FL*/
						hal_status |= Set_Actuator_Status_FL(g_ActuatorsPair.ActParamsCurrent[wing_type].CtryId, (e_ActuatorsNum)g_ActuatorsPair.ActParamsCurrent[wing_type].ActNum);
						g_ModuleParams.Ctry_Failure_Desc |= (CTRY_FLT_MDRV_BASE << g_ActuatorsPair.ActParamsCurrent[wing_type].ActNum);
					}
				}
				else
				{
					g_ActuatorsPair.ActParamsStatus = E_ACT_PARAMS_ERROR;
				}
			}
		}
	}
	return activation_state;
}

/*************************************************************************************
** Perform_Actuators_Deactivation - Deactivates all CTRY's actuators and marks that
** 									there is no active actuator.
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
void Perform_Actuators_Deactivation(void)
{
	MDRV_Enable_Ctrl(MDRV_ALL_PINS,E_GPIO_PIN_RESET);/*Turn OFF all CTRY's actuators*/
	if((int8_t)NO_ACTIVE_ACTUATOR != Get_Active_Actuator_Num())
	{
		Set_Active_Actuator_Num(NO_ACTIVE_ACTUATOR);   /*Update active actuator value to : None*/
		g_Act_Deactivation_Time = HAL_GetTick();/*Set actuators de-activation time*/
	}
}

/*************************************************************************************
** Get_Active_Actuator_Num - Returns current active actuator number
**
** Params : None
**
** Returns: uint8_t - Active actuator number
** Notes:
*************************************************************************************/
uint8_t Get_Active_Actuator_Num(void)
{
	return Active_Actuator_Num;
}

/*************************************************************************************
** Set_Active_Actuator_Num - Sets current active actuator number
**
** Params : uint8_t act_num - Active actuator number
**
** Returns: None
** Notes:
*************************************************************************************/
void Set_Active_Actuator_Num(uint8_t act_num)
{
	if(NO_ACTIVE_ACTUATOR >= act_num)
	{
		Active_Actuator_Num = act_num;
	}
}

/*************************************************************************************
** Init_Actuator_Pair_Parameters - Initialization of actuator pair parameters
** 								   to initial values
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
void Init_Actuator_Pair_Parameters(void)
{
	Act_Pair_Oper_Pointer = 0U;

	g_ActuatorsPair.ActParamsCurrent[RIGHT_WING].CtryId = 0U;
	g_ActuatorsPair.ActParamsCurrent[RIGHT_WING].ActNum = 0U;
	g_ActuatorsPair.ActParamsCurrent[RIGHT_WING].ActRpm = 0U;

	g_ActuatorsPair.ActParamsCurrent[LEFT_WING].CtryId = 1U;
	g_ActuatorsPair.ActParamsCurrent[LEFT_WING].ActNum = 0U;
	g_ActuatorsPair.ActParamsCurrent[LEFT_WING].ActRpm = 0U;


	g_ActuatorsPair.Act_ON_Time_Current  = 0U;
	g_ActuatorsPair.Act_ON_Time_Previous = 0U;

	g_ActuatorsPair.ActParamsStatus = E_ACT_PARAMS_CLEARED;

}

/*************************************************************************************
** Monitor_MOTOR_OPER_Input_State - Performs monitoring of MOT_OPER input and detects
** 									level change occurrence.
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
void Monitor_MOTOR_OPER_Input_State(void)
{
	if(E_STBY_MODE == Get_Module_Mode())
	{
		Mot_Oper_Input_Current_State = Read_MotOper_Input();

		if((E_GPIO_PIN_SET == Mot_Oper_Input_Current_State) && (E_GPIO_PIN_RESET == Mot_Oper_Input_Previous_State))
		{
			/*MOT_OPER_Input changed from GROUND to OPEN(Motor_Operate_Pin changed from '0' to '1' ) -> OPER sequence ended -> re-init values*/
			g_DIMC_Oper_Mode_Active = FALSE;
			Init_Actuator_Pair_Parameters();
			g_OPER_Start_Time = 0U;
		}
		else if((E_GPIO_PIN_RESET == Mot_Oper_Input_Current_State) && (E_GPIO_PIN_SET == Mot_Oper_Input_Previous_State))
		{
			/*MOT_OPER_Input changed from OPEN to GROUND(Motor_Operate_Pin changed from '1' to '0' ) -> OPER sequence started*/
			g_DIMC_Oper_Mode_Active = TRUE;
			g_OPER_Start_Time = HAL_GetTick();
			Init_Actuator_Pair_Parameters();
		}
		else
		{
			/*No level change  -> Do nothing*/
		}
		/*Save current state for future use*/
		Mot_Oper_Input_Previous_State = Mot_Oper_Input_Current_State;
	}
	else
	{
		/*Make sure that OPER parameters were reset in SAFE mode*/
		g_DIMC_Oper_Mode_Active = FALSE;
		Init_Actuator_Pair_Parameters();
		g_OPER_Start_Time = 0U;
	}
}

/*************************************************************************************
** Received_Actuators_Params_Sanity_Monitoring - Performs monitoring activities to
** 												 ensure that the actuators operate in
** 												 pre-defined order
** 												 and actuators parameters are correct.
**
** Params : None
**
** Returns: e_CtrySanityCheckResult - Result of parameters check
** Notes:
*************************************************************************************/
e_CtrySanityCheckResult Received_Actuators_Params_Sanity_Monitoring(void)
{
	e_CtrySanityCheckResult sanity_check_result = E_SANITY_OK;
	e_ActuatorStatus act_status = E_ACT_OK;
	HAL_StatusTypeDef hal_status = HAL_OK;

	/*--------------------------------------------*/
	/*Check received actuators parameters validity*/
	/*--------------------------------------------*/

	/*Check CTRY's ID validity*/
	if((CTRY_MAX_NUM <= g_ActuatorsPair.ActParamsCurrent[LEFT_WING].CtryId) ||\
			((CTRY_MAX_NUM - 1U) <= g_ActuatorsPair.ActParamsCurrent[RIGHT_WING].CtryId))
	{
		/*Failure description: CTRY's ID value out of range */
		sanity_check_result = E_SANITY_PARAM_FL;
	}
	else
	{
		if(g_ActuatorsPair.ActParamsCurrent[LEFT_WING].CtryId == g_ActuatorsPair.ActParamsCurrent[RIGHT_WING].CtryId)
		{
			/*Failure description:  Pair CTRY's ID value in range but equal in both wings */
			sanity_check_result = E_SANITY_PARAM_FL;
		}
	}
	/*Check Left CTRY's Actuator port number validity*/
	if((ACT_NUM_PARM_MIN_VALUE > (g_ActuatorsPair.ActParamsCurrent[LEFT_WING].ActNum)) ||\
			(ACT_NUM_PARM_MAX_VALUE  < (g_ActuatorsPair.ActParamsCurrent[LEFT_WING].ActNum)))
	{
		/*Failure description: Left wing actuator port number out of range 1-6 or 7*/
		sanity_check_result = E_SANITY_PARAM_FL;
	}
	/*Check Right CTRY's Actuator port number validity*/
	if((ACT_NUM_PARM_MIN_VALUE > (g_ActuatorsPair.ActParamsCurrent[RIGHT_WING].ActNum)) ||\
			(ACT_NUM_PARM_MAX_VALUE  < (g_ActuatorsPair.ActParamsCurrent[RIGHT_WING].ActNum)))
	{
		/*Failure description: Right wing actuator port number out of range 1-6 or 7*/
		sanity_check_result = E_SANITY_PARAM_FL;
	}
	/*Check actuator activation mode validity :OPER/IBIT/MANUAL*/
	if((E_ACT_MODE_OPER != g_ActuatorsPair.ActMode) && (E_ACT_MODE_IBIT != g_ActuatorsPair.ActMode) && (E_ACT_MODE_MAN != g_ActuatorsPair.ActMode))
	{
		/*Failure description: Actuator activation mode invalid*/
		sanity_check_result = E_SANITY_PARAM_FL;
	}
	/*Update actuator's port number to suit system parameters table range 0-5*/
	if(E_SANITY_OK == sanity_check_result)
	{
		if(NO_ACTIVE_ACTUATOR != g_ActuatorsPair.ActParamsCurrent[LEFT_WING].ActNum)
		{
			g_ActuatorsPair.ActParamsCurrent[LEFT_WING].ActNum--;
		}
		if(NO_ACTIVE_ACTUATOR != g_ActuatorsPair.ActParamsCurrent[RIGHT_WING].ActNum)
		{
			g_ActuatorsPair.ActParamsCurrent[RIGHT_WING].ActNum--;
		}
	}
	/*----------------------------------------------------------------------*/
	/*Perform sanity monitoring*/
	/*Table Sanity monitoring performed in case that:
	 * 1. DIMC activated OPER mode (MOT_OPER Input changed from Low to High)
	 * 2. Actuator activation command received via CAN_BUS
	 * 3. CTRY is in STBY mode
	 * 4. Parameter validity check passed
	 *
	 * */
	/*----------------------------------------------------------------------*/

	if((E_SANITY_OK == sanity_check_result) &&\
			(TRUE == g_DIMC_Oper_Mode_Active) &&\
			(E_ACT_PARAMS_UPDATED == g_ActuatorsPair.ActParamsStatus) &&\
			(E_STBY_MODE == Get_Module_Mode()))
	{
		/*Check time interval between two consecutive 'ACTUATOR_ON_REQ' messages*/
		if(0U == g_ActuatorsPair.Act_ON_Time_Previous)
		{
			/*First actuators pair activation*/
			g_ActuatorsPair.Act_ON_Time_Previous = g_ActuatorsPair.Act_ON_Time_Current;
		}
		else
		{
			if((ACT_ON_REQ_TIME_MAX_MILIS < (g_ActuatorsPair.Act_ON_Time_Current - g_ActuatorsPair.Act_ON_Time_Previous)) ||\
				(ACT_ON_REQ_TIME_MIN_MILIS > (g_ActuatorsPair.Act_ON_Time_Current - g_ActuatorsPair.Act_ON_Time_Previous)))
			{
				/*ACT_ON_REQ_TIME message is out of time interval range - > Sanity check failed*/
				sanity_check_result = E_SANITY_OPER_INTERVAL_FL;
			}
			else
			{
				/* Actuators pair activation time update*/
				g_ActuatorsPair.Act_ON_Time_Previous = g_ActuatorsPair.Act_ON_Time_Current;
			}
		}
		/*Check that 'ACTUATOR_ON_REQ' message includes OPER mode value as Mode Parameter*/
		if(E_ACT_MODE_OPER != g_ActuatorsPair.ActMode)
		{
			sanity_check_result = E_SANITY_PARAM_FL;
		}

        /*-----------------------------------------------------------------------------------------------------*/
		/*Check received actuators pair CTRY ID and #port_num to required pair according to sequence structure */
		/*-----------------------------------------------------------------------------------------------------*/

		for(uint8_t wing_type = 0U; NUM_OF_WINGS > wing_type; wing_type++)
		{
			/*Compare to pointed pair parameters to received via CANBUS actuator pair current parameters*/
			/*Table Sanity check: CTRY's ID*/
			if(ACT_PAIR_ARRAY[Act_Pair_Oper_Pointer][wing_type].CtryId != g_ActuatorsPair.ActParamsCurrent[wing_type].CtryId)
			{
				/*Table Sanity check: CTRY's ID -- failure*/
				sanity_check_result = E_SANITY_CTRY_ID_FL;
			}
			else
			{
				/*Check received actuator port number*/
				if(NO_ACTIVE_ACTUATOR != g_ActuatorsPair.ActParamsCurrent[wing_type].ActNum)
				{
					/*Actuator port number is valid and not 0x7(NO_ACTIVE_ACTUATOR)*/
					/*Check if the port number equal to pointed one*/
					if((ACT_PAIR_ARRAY[Act_Pair_Oper_Pointer][wing_type].ActNum - 1U) != g_ActuatorsPair.ActParamsCurrent[wing_type].ActNum)
					{
						/*Table Sanity check: Actuators port number incorrect-- failure*/
						sanity_check_result = E_SANITY_ACT_PORT_MONIT_FL;
					}
					else
					{
						/*Actuator port number is correct*/
						/*Check if actuator number marked as FL in status table*/
						hal_status |= Get_Actuator_Status((uint8_t)g_ActuatorsPair.ActParamsCurrent[wing_type].CtryId, (e_ActuatorsNum)g_ActuatorsPair.ActParamsCurrent[wing_type].ActNum, &act_status);
						if(E_ACT_FL == act_status)
						{
							/*Table Sanity check: Check if Actuators port number marked as FL  -- failure*/
							sanity_check_result = E_SANITY_ACT_PORT_MONIT_FL;
						}
					}
				}
				else/*Actuator port number  0x7(NO_ACTIVE_ACTUATOR)*/
				{
					/*Verify that actuator number (0x07) marked as FL in status table*/
					hal_status |= Get_Actuator_Status((uint8_t)g_ActuatorsPair.ActParamsCurrent[wing_type].CtryId, (e_ActuatorsNum)(ACT_PAIR_ARRAY[Act_Pair_Oper_Pointer][wing_type].ActNum - 1U), &act_status);
					if(E_ACT_OK == act_status)
					{
						/*Table Sanity check: FL Actuators port number validation -- failure*/
						sanity_check_result = E_SANITY_ACT_PORT_MONIT_FL;
					}
				}
			}
			if(HAL_OK != hal_status)
			{
				Increment_SW_Error_Counter();
				hal_status = HAL_OK;
			}
		}
	}
	/*Set Sanity monitoring failure in CTRY failure descriptor*/
	if(E_SANITY_OK != sanity_check_result)
	{
		g_ModuleParams.Ctry_Failure_Desc |= CTRY_FLT_SANITY_MONIT;
	}
	else
	{
		/*Update actuators pair pointer to to point to the next pair in operational sequence*/
		if((NUM_OF_ACT_PAIRS - 1U) == Act_Pair_Oper_Pointer)
		{
			/*In case last pair activated, reset actuators pairs pointer*/
			Act_Pair_Oper_Pointer = 0U;
		}
		else
		{
			/*Increment actuators pair pointer to next pair*/
			Act_Pair_Oper_Pointer++;
		}
	}
	return sanity_check_result;
}

/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
