
/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : mcu_config_ctry.c
** Author    : Leonid Savchenko
** Revision  : 1.6
** Updated   : 11-07-2021
**
** Description: This file contains procedures for setup and initialization of used MCU's Pins,
** 		        clock and internal peripherals
**
*/


/* Revision Log:
**
** Rev 1.0  : 25-08-2020 -- Original version created.
** Rev 1.1  : 11-09-2020 -- Added definitions
** Rev 1.2  : 24-10-2020 -- Updated mcu_gpio_pins_init_ctry() initial pins states
** Rev 1.3  : 05-11-2020 -- Added USART defines, added CRC HW enable
** Rev 1.4  : 20-11-2020 -- Static analysis update
** Rev 1.5  : 27-06-2021 -- Updated due to WDOG test changes
** Rev 1.6  : 11-07-2021 -- Added doxygen comments
**************************************************************************************
*/

/* Includes ------------------------------------------------------------------*/
#include <gipsy_hal_cortex.h>
#include "mcu_config_ctry.h"
#include "main.h"
#include "gipsy_hal_rcc.h"
#ifdef _DEBUG_
#include "uart.h"
#endif
#include "gipsy_hal_adc.h"
#include "gipsy_hal_can.h"
#include "gipsy_hal_i2c_ctry.h"
/* -----Definitions ----------------------------------------------------------------*/
#define PWR_REGULATOR_VOLTAGE_SCALE1         PWR_CR_VOS             /* Scale 1 mode(default value at reset): the maximum value of fHCLK is 168 MHz.*/

#define RMVF_BIT_MASK     					 0x01000000U			/*Remove/Clear Reset Flags in RCC->CSR register*/


/********************  Bit definition for PWR_CR register  ********************/
#define PWR_CR_VOS_POS         (14U)
#define PWR_CR_VOS_MSK         (0x3UL << PWR_CR_VOS_POS)                        /*!< 0x0000C000 */
#define PWR_CR_VOS             PWR_CR_VOS_MSK                                  /*!< VOS[1:0] bits (Regulator voltage scaling output selection) */

/*I2C Buses definitions*/
#define I2C_DUTYCYCLE_2   			0x00000000U
#define I2C_OWNADDRESS1    			0x00000000U
#define I2C_ADDRESSINGMODE_7BIT  	0x00004000U
#define I2C_DUALADDRESS_DISABLE  	0x00000000U
#define I2C_OWNADDRESS2    			0x00000000U
#define I2C_GENERALCALL_DISABLE 	0x00000000U
#define I2C_NOSTRETCH_DISABLE 		0x00000000U
#define I2C_1_SPEED      100000U  /*100Khz*/
#define I2C_2_SPEED      100000U  /*100Khz*/
#define I2C_3_SPEED      400000U  /*400Khz*/

#define CAN1_IT_PREEMPT_PRIORITY  5U
#define CAN1_IT_SUB_PRIORITY      0U

/*USART Definitions*/

#define UART_WORDLENGTH_9B  	0x00001000U
#define UART_STOPBITS_1     	0x00000000U
#define UART_PARITY_ODD         (0x00000400U | 0x00000200U)
#define UART_MODE_TX_RX         (0x00000008U | 0x00000004U)
#define UART_HWCONTROL_NONE		0x00000000U
#define UART_OVERSAMPLING_16	0x00000000U

/********************  Bit definition for RCC_AHB1ENR register  ***************/
#define RCC_AHB1ENR_GPIOAEN_POS            (0U)
#define RCC_AHB1ENR_GPIOAEN_MSK            (0x1UL << RCC_AHB1ENR_GPIOAEN_POS)   /*!< 0x00000001 */
#define RCC_AHB1ENR_GPIOAEN                RCC_AHB1ENR_GPIOAEN_MSK
#define RCC_AHB1ENR_GPIOBEN_POS            (1U)
#define RCC_AHB1ENR_GPIOBEN_MSK            (0x1UL << RCC_AHB1ENR_GPIOBEN_POS)   /*!< 0x00000002 */
#define RCC_AHB1ENR_GPIOBEN                RCC_AHB1ENR_GPIOBEN_MSK
#define RCC_AHB1ENR_GPIOCEN_POS            (2U)
#define RCC_AHB1ENR_GPIOCEN_MSK            (0x1UL << RCC_AHB1ENR_GPIOCEN_POS)   /*!< 0x00000004 */
#define RCC_AHB1ENR_GPIOCEN                RCC_AHB1ENR_GPIOCEN_MSK
#define RCC_AHB1ENR_GPIODEN_POS            (3U)
#define RCC_AHB1ENR_GPIODEN_MSK            (0x1UL << RCC_AHB1ENR_GPIODEN_POS)   /*!< 0x00000008 */
#define RCC_AHB1ENR_GPIODEN                RCC_AHB1ENR_GPIODEN_MSK
#define RCC_AHB1ENR_GPIOEEN_POS            (4U)
#define RCC_AHB1ENR_GPIOEEN_MSK            (0x1UL << RCC_AHB1ENR_GPIOEEN_POS)   /*!< 0x00000010 */
#define RCC_AHB1ENR_GPIOEEN                RCC_AHB1ENR_GPIOEEN_MSK
#define RCC_AHB1ENR_CRCEN_POS              (12U)
#define RCC_AHB1ENR_CRCEN_MSK              (0x1UL << RCC_AHB1ENR_CRCEN_POS)     /*!< 0x00001000 */
#define RCC_AHB1ENR_CRCEN                  RCC_AHB1ENR_CRCEN_MSK

/********************  Bit definition for RCC_APB1ENR register  ***************/
#define RCC_APB1ENR_TIM3EN_POS             (1U)
#define RCC_APB1ENR_TIM3EN_MSK             (0x1UL << RCC_APB1ENR_TIM3EN_POS)    /*!< 0x00000002 */
#define RCC_APB1ENR_TIM3EN                 RCC_APB1ENR_TIM3EN_MSK
#define RCC_APB1ENR_TIM4EN_POS             (2U)
#define RCC_APB1ENR_TIM4EN_MSK             (0x1UL << RCC_APB1ENR_TIM4EN_POS)    /*!< 0x00000004 */
#define RCC_APB1ENR_TIM4EN                 RCC_APB1ENR_TIM4EN_MSK
#define RCC_APB1ENR_USART3EN_POS           (18U)
#define RCC_APB1ENR_USART3EN_MSK           (0x1UL << RCC_APB1ENR_USART3EN_POS)  /*!< 0x00040000 */
#define RCC_APB1ENR_USART3EN               RCC_APB1ENR_USART3EN_MSK
#define RCC_APB1ENR_I2C1EN_POS             (21U)
#define RCC_APB1ENR_I2C1EN_MSK             (0x1UL << RCC_APB1ENR_I2C1EN_POS)    /*!< 0x00200000 */
#define RCC_APB1ENR_I2C1EN                 RCC_APB1ENR_I2C1EN_MSK
#define RCC_APB1ENR_I2C2EN_POS             (22U)
#define RCC_APB1ENR_I2C2EN_MSK             (0x1UL << RCC_APB1ENR_I2C2EN_POS)    /*!< 0x00400000 */
#define RCC_APB1ENR_I2C2EN                 RCC_APB1ENR_I2C2EN_MSK
#define RCC_APB1ENR_I2C3EN_POS             (23U)
#define RCC_APB1ENR_I2C3EN_MSK             (0x1UL << RCC_APB1ENR_I2C3EN_POS)    /*!< 0x00800000 */
#define RCC_APB1ENR_I2C3EN                 RCC_APB1ENR_I2C3EN_MSK
#define RCC_APB1ENR_CAN1EN_POS             (25U)
#define RCC_APB1ENR_CAN1EN_MSK             (0x1UL << RCC_APB1ENR_CAN1EN_POS)    /*!< 0x02000000 */
#define RCC_APB1ENR_CAN1EN                 RCC_APB1ENR_CAN1EN_MSK
#define RCC_APB1ENR_PWREN_POS              (28U)
#define RCC_APB1ENR_PWREN_MSK              (0x1UL << RCC_APB1ENR_PWREN_POS)     /*!< 0x10000000 */
#define RCC_APB1ENR_PWREN                  RCC_APB1ENR_PWREN_MSK

/********************  Bit definition for RCC_APB2ENR register  ***************/
#define RCC_APB2ENR_TIM1EN_POS             (0U)
#define RCC_APB2ENR_TIM1EN_MSK             (0x1UL << RCC_APB2ENR_TIM1EN_POS)    /*!< 0x00000001 */
#define RCC_APB2ENR_TIM1EN                 RCC_APB2ENR_TIM1EN_MSK
#define RCC_APB2ENR_USART1EN_POS           (4U)
#define RCC_APB2ENR_USART1EN_MSK           (0x1UL << RCC_APB2ENR_USART1EN_POS)  /*!< 0x00000010 */
#define RCC_APB2ENR_USART1EN               RCC_APB2ENR_USART1EN_MSK
#define RCC_APB2ENR_ADC1EN_POS             (8U)
#define RCC_APB2ENR_ADC1EN_MSK             (0x1UL << RCC_APB2ENR_ADC1EN_POS)    /*!< 0x00000100 */
#define RCC_APB2ENR_ADC1EN                 RCC_APB2ENR_ADC1EN_MSK
/* -----Macros ---------------------------------------------------------------------*/

/* @brief  macros configure the main internal regulator output voltage.
  * @param  _REGULATOR_ specifies the regulator output voltage to achieve
  *         a tradeoff between performance and power consumption when the device does
  *         not operate at the maximum frequency (refer to the datasheets for more details).
  *          This parameter can be one of the following values:
  *            @arg PWR_REGULATOR_VOLTAGE_SCALE1: Regulator voltage output Scale 1 mode
  *            @arg PWR_REGULATOR_VOLTAGE_SCALE2: Regulator voltage output Scale 2 mode
  *            @arg PWR_REGULATOR_VOLTAGE_SCALE3: Regulator voltage output Scale 3 mode
  * @retval None
  */
#define HAL_PWR_VOLTAGESCALING_CONFIG(_REGULATOR_) do {                                                     \
                                                            __IO uint32_t tmpreg = 0x00U;                        \
                                                            MODIFY_REG(PWR->CR, PWR_CR_VOS, (_REGULATOR_));   \
                                                            /* Delay after an RCC peripheral clock enabling */  \
                                                            tmpreg = READ_BIT(PWR->CR, PWR_CR_VOS);             \
                                                            UNUSED(tmpreg);                                     \
                                                          } while(FALSE)

#define HAL_RCC_PWR_CLK_ENABLE()     do { \
                                        __IO uint32_t tmpreg = 0x00U; \
                                        SET_BIT(RCC->APB1ENR, RCC_APB1ENR_PWREN);\
                                        /* Delay after an RCC peripheral clock enabling */ \
                                        tmpreg = READ_BIT(RCC->APB1ENR, RCC_APB1ENR_PWREN);\
                                        UNUSED(tmpreg); \
                                          } while(FALSE)




/* -----External variables ---------------------------------------------------------*/
extern uint32_t g_Reset_Source;
/* -----Global variables -----------------------------------------------------------*/
/*Global Peripherals handlers*/

/*!Specifies I2C_1 bus parameters (Range: var type | Units: see var declaration)*/
/*!\sa Set function: mcu_i2c_busses_init()*/
/*!\sa Get function: mcp9804_write(), mcp9804_read()*/
I2C_HandleTypeDef g_HandleI2C_1_TempDrv = {0U};

/*!Specifies I2C_2 bus parameters (Range: var type | Units: see var declaration)*/
/*!\sa Set function: mcu_i2c_busses_init()*/
/*!\sa Get function: mcp9804_write(), mcp9804_read()*/
I2C_HandleTypeDef g_HandleI2C_2_TempMcu = {0U};

/*!Specifies I2C_3 bus parameters (Range: var type | Units: see var declaration)*/
/*!\sa Set function: mcu_i2c_busses_init()*/
/*!\sa Get function: accel_read_multi_reg(), accel_read_reg(), accel_write_reg()*/
I2C_HandleTypeDef g_HandleI2C_3_Accel = {0U};

/*!Specifies TIM3 timer parameters (Range: var type | Units: see var declaration)*/
/*!\sa Set function: mcu_tim_pwm_channels_init()*/
/*!\sa Get function: mdrv_set_pwm_value()*/
TIM_HandleTypeDef g_HandleTim3 = {0U};

/*!Specifies TIM4 timer parameters (Range: var type | Units: see var declaration)*/
/*!\sa Set function: mcu_tim_pwm_channels_init()*/
/*!\sa Get function: mdrv_set_pwm_value()*/
TIM_HandleTypeDef g_HandleTim4 = {0U};


#ifdef _DEBUG_
UART_HandleTypeDef g_HandleUart3Debug;
#endif



/* -----Static Function prototypes -------------------------------------------------*/
static void system_clock_config(void);
static void mcu_gpio_pins_init_ctry(void);
static void mcu_tim_pwm_channels_init(void);
static void mcu_i2c_busses_init(void);


#ifdef _DEBUG_
static void debug_uart_usart3_init(void);
#endif

/* -----Modules implementation -----------------------------------------------------*/

/*************************************************************************************
** MCU_Periph_Config - Initialization of MCUs peripherals
** This function initializes and configures  MCUs peripherals
**
** Params : None.
**
** Returns: None.
** Notes:
*************************************************************************************/
void MCU_Periph_Config(void)
{
	/* Get the last reset source of the MCU */
	g_Reset_Source = RCC->CSR;

	/* Clear the reset source register */
	SET_BIT(RCC->CSR,RMVF_BIT_MASK);

	/* HAL_INIT - Reset of all peripherals, Initializes the Flash interface. */
	/* INSTRUCTION_CACHE_ENABLE */
	FLASH->ACR |= (volatile uint32_t)FLASH_ACR_ICEN;

	/* DATA_CACHE_ENABLE */
	FLASH->ACR |= (volatile uint32_t)FLASH_ACR_DCEN;

	/* PREFETCH_ENABLE */
	FLASH->ACR |= (volatile uint32_t)FLASH_ACR_PRFTEN;

	/* Set Interrupt Group Priority */
	HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
	/*HAL Init Ends*/

	system_clock_config(); /* Configure the system clock */

	/* Initialize all required MCU's peripherals */
	mcu_gpio_pins_init_ctry();
	mcu_tim_pwm_channels_init();
	mcu_i2c_busses_init();
	HAL_ADC_Init();

#ifdef _DEBUG_
	debug_uart_usart3_init();
	UART3_Serial_Init();
#endif

}

/*************************************************************************************
** system_clock_config - System Clock Configuration
** 						 Configures and enables internal MCU's clock mechanism
**
** Params : None.
**
** Returns: None
** Notes:
*************************************************************************************/
static void system_clock_config(void)
{
	HAL_StatusTypeDef hal_status = HAL_OK;

	/*Configure the main internal regulator output voltage*/
	HAL_RCC_PWR_CLK_ENABLE();
	HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	/*Initialization of the CPU, AHB and APB busses clocks */
	hal_status |= HAL_RCC_OscConfig();

	/*Initialization of the CPU, AHB and APB busses clocks*/
	hal_status |= HAL_RCC_ClockConfig();

	/*Initialization of RTC bus clock*/
	hal_status |= HAL_RCCEx_PeriphCLKConfig();

	/*In case of clocks initialization error -> Enter infinite loop */
	if(HAL_OK != hal_status)
	{
		Error_Handler();
	}

	/*Initialization of Peripherals clocks*/
	SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_GPIOAEN);/*Enable clock for GPIO_A port*/
	SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_GPIOBEN);/*Enable clock for GPIO_B port*/
	SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_GPIOCEN);/*Enable clock for GPIO_C port*/
	SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_GPIODEN);/*Enable clock for GPIO_D port*/
	SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_GPIOEEN);/*Enable clock for GPIO_E port*/
	SET_BIT(RCC->APB1ENR, RCC_APB1ENR_I2C1EN);/*Enable clock for I2C_1 bus*/
	SET_BIT(RCC->APB1ENR, RCC_APB1ENR_I2C2EN);/*Enable clock for I2C_2 bus*/
	SET_BIT(RCC->APB1ENR, RCC_APB1ENR_I2C3EN);/*Enable clock for I2C_3 bus*/
	SET_BIT(RCC->APB1ENR, RCC_APB1ENR_CAN1EN);/*Enable clock for CAN1 bus*/
	SET_BIT(RCC->APB2ENR, RCC_APB2ENR_ADC1EN);/*Enable clock for ADC1*/
	SET_BIT(RCC->APB1ENR, RCC_APB1ENR_TIM3EN);/*Enable clock for TIM_3*/
	SET_BIT(RCC->APB1ENR, RCC_APB1ENR_TIM4EN);/*Enable clock for TIM_4*/
	SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_CRCEN);/*Enable CRC HW peripheral*/

#ifdef _DEBUG_
	SET_BIT(RCC->APB1ENR, RCC_APB1ENR_USART3EN);/*Enable clock for UART_3 bus*/
#endif

}

/*************************************************************************************
** mcu_i2c_busses_init - Configure and init I2C Port1/2/3 buses for temperature sensors
** 								   and accelerometer
**
** Params : None.
**
** Returns: None.
** Notes:
*************************************************************************************/
static void mcu_i2c_busses_init(void)
{
	g_HandleI2C_1_TempDrv.Instance =             I2C1;
	g_HandleI2C_1_TempDrv.Init.ClockSpeed =      I2C_1_SPEED;
	g_HandleI2C_1_TempDrv.Init.DutyCycle =       I2C_DUTYCYCLE_2;
	g_HandleI2C_1_TempDrv.Init.OwnAddress1 =     I2C_OWNADDRESS1;
	g_HandleI2C_1_TempDrv.Init.AddressingMode =  I2C_ADDRESSINGMODE_7BIT;
	g_HandleI2C_1_TempDrv.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	g_HandleI2C_1_TempDrv.Init.OwnAddress2 =     I2C_OWNADDRESS2;
	g_HandleI2C_1_TempDrv.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	g_HandleI2C_1_TempDrv.Init.NoStretchMode =   I2C_NOSTRETCH_DISABLE;

	/*Initialize I2C1 peripheral*/
	HAL_I2C_Init(&g_HandleI2C_1_TempDrv);

	g_HandleI2C_2_TempMcu.Instance =             I2C2;
	g_HandleI2C_2_TempMcu.Init.ClockSpeed =      I2C_2_SPEED;
	g_HandleI2C_2_TempMcu.Init.DutyCycle =       I2C_DUTYCYCLE_2;
	g_HandleI2C_2_TempMcu.Init.OwnAddress1 =     I2C_OWNADDRESS1;
	g_HandleI2C_2_TempMcu.Init.AddressingMode =  I2C_ADDRESSINGMODE_7BIT;
	g_HandleI2C_2_TempMcu.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	g_HandleI2C_2_TempMcu.Init.OwnAddress2 =     I2C_OWNADDRESS2;
	g_HandleI2C_2_TempMcu.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	g_HandleI2C_2_TempMcu.Init.NoStretchMode =   I2C_NOSTRETCH_DISABLE;

	/*Initialize I2C2 peripheral*/
	HAL_I2C_Init(&g_HandleI2C_2_TempMcu);

	g_HandleI2C_3_Accel.Instance =             I2C3;
	g_HandleI2C_3_Accel.Init.ClockSpeed =      I2C_3_SPEED;
	g_HandleI2C_3_Accel.Init.DutyCycle =       I2C_DUTYCYCLE_2;
	g_HandleI2C_3_Accel.Init.OwnAddress1 =     I2C_OWNADDRESS1;
	g_HandleI2C_3_Accel.Init.AddressingMode =  I2C_ADDRESSINGMODE_7BIT;
	g_HandleI2C_3_Accel.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	g_HandleI2C_3_Accel.Init.OwnAddress2 =     I2C_OWNADDRESS2;
	g_HandleI2C_3_Accel.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	g_HandleI2C_3_Accel.Init.NoStretchMode =   I2C_NOSTRETCH_DISABLE;

	/*Initialize I2C3 peripheral*/
	HAL_I2C_Init(&g_HandleI2C_3_Accel);

}

/*************************************************************************************
** mcu_tim_pwm_channels_init - TIM3/4 Initialization Function for MDRV PWM Inputs
**
** Params : None.
**
** Returns: None
** Notes:
*************************************************************************************/
static void mcu_tim_pwm_channels_init(void)
{
	GPIO_InitTypeDef   gpio_init_struct = {0U};
	HAL_StatusTypeDef hal_status = HAL_OK;
	uint32_t const TIM_PRESCALER = 46U;
	uint32_t const TIM_PERIOD    = 100U;

	g_HandleTim3.Instance = TIM3;
	g_HandleTim4.Instance = TIM4;


	/*Initialize and Configure PWM Channels*/
	hal_status |= HAL_TIM_PWM_Init(&g_HandleTim3, TIM_PRESCALER, TIM_PERIOD);
	hal_status |= HAL_TIM_PWM_Init(&g_HandleTim4, TIM_PRESCALER, TIM_PERIOD);

	if(HAL_OK == hal_status)
	{
		/*HW Post Init*/
		/**TIM3 GPIO Configuration
		 PWMIN1-->PC6 ------> TIM3_CH1
		 PWMIN2-->PC7 ------> TIM3_CH2
		 PWMIN3-->PC8 ------> TIM3_CH3
		 */
		gpio_init_struct.Pin = PWMIN1_PIN|PWMIN2_PIN|PWMIN3_PIN;
		gpio_init_struct.Mode = GPIO_MODE_AF_PP;
		gpio_init_struct.Pull = GPIO_NOPULL;
		gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
		gpio_init_struct.Alternate = GPIO_AF2_TIM3;
		hal_status |= HAL_GPIO_Init(GPIOC, &gpio_init_struct);

		/**TIM4 GPIO Configuration
		 PWMIN4-->PD13 ------> TIM4_CH2
		 PWMIN5-->PD14 ------> TIM4_CH3
		 PWMIN6-->PD15 ------> TIM4_CH4
		 */
		gpio_init_struct.Pin = PWMIN4_PIN|PWMIN5_PIN|PWMIN6_PIN;
		gpio_init_struct.Mode = GPIO_MODE_AF_PP;
		gpio_init_struct.Pull = GPIO_NOPULL;
		gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
		gpio_init_struct.Alternate = GPIO_AF2_TIM4;
		hal_status |= HAL_GPIO_Init(GPIOD, &gpio_init_struct);
	}
	/*In case of TIM3/4 initialization error -> Enter infinite loop */
	if(HAL_OK != hal_status)
	{
		Error_Handler();
	}
}

 /*************************************************************************************
 ** MCU_CAN_Interface_Init - Initialization and configuration of CAN1 communication interface
 **
 ** Params : None.
 **
 ** Returns: HAL_StatusTypeDef - Status
 ** Notes:
 *************************************************************************************/
 HAL_StatusTypeDef MCU_CAN_Interface_Init(void)
 {
	 /* NVIC configuration for CAN1 Reception complete interrupt */
	 HAL_NVIC_SetPriority(CAN1_RX0_IRQn, CAN1_IT_PREEMPT_PRIORITY, CAN1_IT_SUB_PRIORITY);
	 HAL_NVIC_EnableIRQ(CAN1_RX0_IRQn);

	 /*CAN peripheral :Init, Start, Enable interrupts and perform LoopBack Test*/
	 return HAL_CAN_Init();
 }

/*************************************************************************************
** mcu_gpio_pins_init_ctry - Initialization and configuration of GPIO's
**
** Params : None.
**
** Returns: None.
** Notes:
*************************************************************************************/
static void mcu_gpio_pins_init_ctry(void)
{
	GPIO_InitTypeDef gpio_init_struct = {0U};
	HAL_StatusTypeDef hal_status = HAL_OK;

	 /*Configure I2C port 3 GPIOs*/
	 /**I2C3 GPIO Configuration
    PA8     ------> I2C3_SCL
    PC9     ------> I2C3_SDA
	  */
	 gpio_init_struct.Pin =       GPIO_PIN_8;
	 gpio_init_struct.Mode =      GPIO_MODE_AF_OD;
	 gpio_init_struct.Pull =      GPIO_NOPULL;
	 gpio_init_struct.Speed =     GPIO_SPEED_FREQ_VERY_HIGH;
	 gpio_init_struct.Alternate = GPIO_AF4_I2C3;
	 hal_status |= HAL_GPIO_Init(GPIOA, &gpio_init_struct);

	 gpio_init_struct.Pin =       GPIO_PIN_9;
	 gpio_init_struct.Mode =      GPIO_MODE_AF_OD;
	 gpio_init_struct.Pull =      GPIO_NOPULL;
	 gpio_init_struct.Speed =     GPIO_SPEED_FREQ_VERY_HIGH;
	 gpio_init_struct.Alternate = GPIO_AF4_I2C3;
	 hal_status |= HAL_GPIO_Init(GPIOC, &gpio_init_struct);

	/*Configure I2C port 2 GPIOs*/
	/**I2C2 GPIO Configuration
    PB10     ------> I2C2_SCL
    PB11     ------> I2C2_SDA
	 */
	gpio_init_struct.Pin = GPIO_PIN_10|GPIO_PIN_11;
	gpio_init_struct.Mode = GPIO_MODE_AF_OD;
	gpio_init_struct.Pull = GPIO_NOPULL;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	gpio_init_struct.Alternate = GPIO_AF4_I2C2;
	hal_status |= HAL_GPIO_Init(GPIOB, &gpio_init_struct);

	/*Configure I2C port 1 GPIOs*/
	/**I2C1 GPIO Configuration
     PB6     ------> I2C1_SCL
     PB7     ------> I2C1_SDA
	 */
	gpio_init_struct.Pin = GPIO_PIN_6|GPIO_PIN_7;
	gpio_init_struct.Mode = GPIO_MODE_AF_OD;
	gpio_init_struct.Pull = GPIO_NOPULL;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	gpio_init_struct.Alternate = GPIO_AF4_I2C1;
	hal_status |= HAL_GPIO_Init(GPIOB, &gpio_init_struct);

	/*ADC1 GPIO Configuration*/
	/*PA4     ------> ADC1_IN4--->Voltage_5VDC*/
	/*PA5     ------> ADC1_IN5--->Voltage_14.8VDC*/
	/*PA2     ------> ADC1_IN2--->V_DRIVER_MONIT_PIN*/
	/*PA3     ------> ADC1_IN3--->MTR_CUR_LVL_CPU_PIN*/
	gpio_init_struct.Pin =  V5VDCLVL_PIN|V14_8VDCLVL_PIN|V_DRIVER_MONIT_PIN|MTR_CUR_LVL_CPU_PIN;
	gpio_init_struct.Mode = GPIO_MODE_ANALOG;
	gpio_init_struct.Pull = GPIO_NOPULL;
	hal_status |= HAL_GPIO_Init(ADC1_PORT, &gpio_init_struct);

	/* CAN1 TX GPIO pin configuration */
	gpio_init_struct.Pin = TXCAN_1_PIN;
	gpio_init_struct.Mode = GPIO_MODE_AF_PP;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	gpio_init_struct.Pull = GPIO_PULLUP;
	gpio_init_struct.Alternate =  GPIO_AF9_CAN1;
	hal_status |= HAL_GPIO_Init(CAN_PORT, &gpio_init_struct);

	/* CAN1 RX GPIO pin configuration */
	gpio_init_struct.Pin = RXCAN_1_PIN;
	gpio_init_struct.Mode = GPIO_MODE_AF_PP;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	gpio_init_struct.Pull = GPIO_PULLUP;
	gpio_init_struct.Alternate =  GPIO_AF9_CAN1;
	hal_status |= HAL_GPIO_Init(CAN_PORT, &gpio_init_struct);

	/*Init Debug LEDs Outputs*/
	gpio_init_struct.Pin = SW_RUN_LED_PIN | FLT_LED_PIN;
	gpio_init_struct.Mode = GPIO_MODE_OUTPUT_PP;
	gpio_init_struct.Pull = GPIO_NOPULL;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(LED_PORT, &gpio_init_struct);
	/*Set GPIOs default state*/
	HAL_GPIO_WritePin(LED_PORT, SW_RUN_LED_PIN | FLT_LED_PIN, E_GPIO_PIN_SET);

	/*Init MDRV WAKE Outputs*/
	gpio_init_struct.Pin = MDRV_WAKE1_PIN|MDRV_WAKE2_PIN|MDRV_WAKE3_PIN|MDRV_WAKE4_PIN|MDRV_WAKE5_PIN|MDRV_WAKE6_PIN;
	gpio_init_struct.Mode = GPIO_MODE_OUTPUT_PP;
	gpio_init_struct.Pull = GPIO_NOPULL;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	/*Set GPIOs default state*/
	hal_status |= HAL_GPIO_Init(MDRV_WAKE_PORT, &gpio_init_struct);
	HAL_GPIO_WritePin(MDRV_WAKE_PORT, MDRV_WAKE1_PIN|MDRV_WAKE2_PIN |\
			MDRV_WAKE3_PIN|MDRV_WAKE4_PIN|MDRV_WAKE5_PIN|MDRV_WAKE6_PIN, E_GPIO_PIN_RESET);

	/*Init MDRV SPI CLK Output*/
	gpio_init_struct.Pin = MDRV_SPI_CLK_PIN;
	gpio_init_struct.Mode = GPIO_MODE_OUTPUT_PP;
	gpio_init_struct.Pull = GPIO_NOPULL;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(MDRV_SPI_PORT, &gpio_init_struct);
	/*Set GPIOs default state*/
	HAL_GPIO_WritePin(MDRV_SPI_PORT, MDRV_SPI_CLK_PIN, E_GPIO_PIN_RESET);

	/*Init MDRV SPI MOSI Output*/
	gpio_init_struct.Pin = MDRV_SPI_MOSI_PIN;
	gpio_init_struct.Mode = GPIO_MODE_OUTPUT_PP;
	gpio_init_struct.Pull = GPIO_NOPULL;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(MDRV_SPI_PORT, &gpio_init_struct);
	/*Set GPIOs default state*/
	HAL_GPIO_WritePin(MDRV_SPI_PORT, MDRV_SPI_MOSI_PIN, E_GPIO_PIN_RESET);

	/*Init MDRV SPI MISO Input*/
	gpio_init_struct.Pin = MDRV_SPI_MISO_PIN;
	gpio_init_struct.Mode = GPIO_MODE_INPUT;
	gpio_init_struct.Pull = GPIO_PULLUP;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(MDRV_SPI_PORT, &gpio_init_struct);

	/*Init MDRV CS Outputs*/
	gpio_init_struct.Pin = MDRV_SPI_CS1_PIN|MDRV_SPI_CS2_PIN|MDRV_SPI_CS3_PIN|MDRV_SPI_CS4_PIN|MDRV_SPI_CS5_PIN|MDRV_SPI_CS6_PIN;
	gpio_init_struct.Mode = GPIO_MODE_OUTPUT_PP;
	gpio_init_struct.Pull = GPIO_NOPULL;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(MDRV_SPI_CS_PORT, &gpio_init_struct);
	/*Set GPIOs default state*/
	HAL_GPIO_WritePin(MDRV_SPI_CS_PORT, MDRV_SPI_CS1_PIN|MDRV_SPI_CS2_PIN|MDRV_SPI_CS3_PIN| \
			MDRV_SPI_CS4_PIN|MDRV_SPI_CS5_PIN|MDRV_SPI_CS6_PIN, E_GPIO_PIN_SET);

	/*Init MDRV EN Outputs*/
	gpio_init_struct.Pin = MDRV_EN1_PIN|MDRV_EN2_PIN|MDRV_EN3_PIN|MDRV_EN4_PIN|MDRV_EN5_PIN|MDRV_EN6_PIN;
	gpio_init_struct.Mode = GPIO_MODE_OUTPUT_PP;
	gpio_init_struct.Pull = GPIO_NOPULL;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(MDRV_EN_PORT, &gpio_init_struct);
	/*Set GPIOs default state*/
	HAL_GPIO_WritePin(MDRV_EN_PORT, MDRV_EN1_PIN|MDRV_EN2_PIN\
			|MDRV_EN3_PIN|MDRV_EN4_PIN|MDRV_EN5_PIN|MDRV_EN6_PIN, E_GPIO_PIN_RESET);


	/*Init CONF ID Reset Output*/
	gpio_init_struct.Pin = CONF_ID_RESET_PIN;
	gpio_init_struct.Mode = GPIO_MODE_OUTPUT_PP;
	gpio_init_struct.Pull = GPIO_NOPULL;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(CONF_ID_RESET_PORT, &gpio_init_struct);
	/*Set GPIOs default state*/
	HAL_GPIO_WritePin(CONF_ID_RESET_PORT, CONF_ID_RESET_PIN, E_GPIO_PIN_SET);

	/*Init CONF ID CS Output*/
	gpio_init_struct.Pin = CONF_ID_SPI_CS_PIN;
	gpio_init_struct.Mode = GPIO_MODE_OUTPUT_PP;
	gpio_init_struct.Pull = GPIO_NOPULL;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(CONF_ID_SPI_CS_PORT, &gpio_init_struct);
	/*Set GPIOs default state*/
	HAL_GPIO_WritePin(CONF_ID_SPI_CS_PORT, CONF_ID_SPI_CS_PIN, E_GPIO_PIN_SET);

	/*Init CONF ID SEL0 Output*/
	gpio_init_struct.Pin = CONF_ID_SEL0_PIN;
	gpio_init_struct.Mode = GPIO_MODE_OUTPUT_PP;
	gpio_init_struct.Pull = GPIO_NOPULL;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(CONF_ID_SEL0_PORT, &gpio_init_struct);
	/*Set GPIOs default state*/
	HAL_GPIO_WritePin(CONF_ID_SEL0_PORT, CONF_ID_SEL0_PIN, E_GPIO_PIN_SET);

	/*Init CONF ID SEL1 Output*/
	gpio_init_struct.Pin = CONF_ID_SEL1_PIN;
	gpio_init_struct.Mode = GPIO_MODE_OUTPUT_PP;
	gpio_init_struct.Pull = GPIO_NOPULL;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(CONF_ID_SEL1_PORT, &gpio_init_struct);
	/*Set GPIOs default state*/
	HAL_GPIO_WritePin(CONF_ID_SEL1_PORT, CONF_ID_SEL1_PIN, E_GPIO_PIN_SET);

	/*Init Accelerometer's Int1 Input*/
	gpio_init_struct.Pin = ACCEL_INT1_PIN;
	gpio_init_struct.Mode = GPIO_MODE_INPUT;
	gpio_init_struct.Pull = GPIO_PULLUP;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(ACCEL_INT1_PORT, &gpio_init_struct);

	/*Init Accelerometer's Int2 Input*/
	gpio_init_struct.Pin = ACCEL_INT2_PIN;
	gpio_init_struct.Mode = GPIO_MODE_INPUT;
	gpio_init_struct.Pull = GPIO_PULLUP;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(ACCEL_INT2_PORT, &gpio_init_struct);

	/*Init Motor Operate Input from DIMC*/
	gpio_init_struct.Pin = MOTOR_OPERATE_PIN;
	gpio_init_struct.Mode = GPIO_MODE_INPUT;
	gpio_init_struct.Pull = GPIO_NOPULL;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(MOTOR_OPERATE_PORT, &gpio_init_struct);

	/*Init Power Enable Output*/
	gpio_init_struct.Pin = MOTOR_PWR_EN_PIN;
	gpio_init_struct.Mode = GPIO_MODE_OUTPUT_PP;
	gpio_init_struct.Pull = GPIO_NOPULL;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(MOTOR_PWR_EN_PORT, &gpio_init_struct);
	/*Set GPIOs default state*/
	HAL_GPIO_WritePin(MOTOR_PWR_EN_PORT, MOTOR_PWR_EN_PIN, E_GPIO_PIN_RESET);

	/*Init CTRY CH_C fault Output*/
	gpio_init_struct.Pin = CPU_CON_TRAY_FAULT_PIN;
	gpio_init_struct.Mode = GPIO_MODE_OUTPUT_PP;
	gpio_init_struct.Pull = GPIO_NOPULL;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(CPU_CON_TRAY_FAULT_PORT, &gpio_init_struct);
	/*Set GPIOs default state*/
	HAL_GPIO_WritePin(CPU_CON_TRAY_FAULT_PORT, CPU_CON_TRAY_FAULT_PIN, E_GPIO_PIN_RESET);


	/*Init heaters control Outputs*/
	gpio_init_struct.Pin = CPU_CARD_HEATING_ON_PIN|DRV_CARD_HEATING_ON_PIN;
	gpio_init_struct.Mode = GPIO_MODE_OUTPUT_PP;
	gpio_init_struct.Pull = GPIO_NOPULL;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(CARD_HEATING_ON_PORT, &gpio_init_struct);
	/*Set GPIOs default state*/
	HAL_GPIO_WritePin(CARD_HEATING_ON_PORT, CPU_CARD_HEATING_ON_PIN|DRV_CARD_HEATING_ON_PIN, E_GPIO_PIN_RESET);


	/*Init External Watch-Dog strobe Output*/
	gpio_init_struct.Pin = WDI_PIN;
	gpio_init_struct.Mode = GPIO_MODE_OUTPUT_PP;
	gpio_init_struct.Pull = GPIO_NOPULL;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(WDI_PORT, &gpio_init_struct);
	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(WDI_PORT, WDI_PIN, E_GPIO_PIN_RESET);

	/*In case of GPIO initialization error -> Enter infinite loop */
	if(HAL_OK != hal_status)
	{
		Error_Handler();
	}
}

#ifdef _DEBUG_
/*************************************************************************************
** debug_uart_usart3_init - Initialization and configuration of USART bus
**                          communication interface for DEBUG port interface
** Params : None.
**
** Returns: None.
** Notes:
*************************************************************************************/
static void debug_uart_usart3_init(void)
{
	GPIO_InitTypeDef gpio_init_struct = {0U};
	HAL_StatusTypeDef hal_status = HAL_OK;

	g_HandleUart3Debug.Instance = USART3;
	g_HandleUart3Debug.Init.BaudRate = (uint32_t)115200U;
	g_HandleUart3Debug.Init.WordLength = UART_WORDLENGTH_9B;
	g_HandleUart3Debug.Init.StopBits = UART_STOPBITS_1;
	g_HandleUart3Debug.Init.Parity = UART_PARITY_ODD;
	g_HandleUart3Debug.Init.Mode = UART_MODE_TX_RX;
	g_HandleUart3Debug.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	g_HandleUart3Debug.Init.OverSampling = UART_OVERSAMPLING_16;

	/*Configuration of hardware resources used by USART3*/
	/**USART3 GPIO Configuration
	    PC10     ------> U3_TX_Pin
	    PC11     ------> U3_RX_Pin
	 */
	gpio_init_struct.Pin = U3_TX_PIN|U3_RX_PIN;
	gpio_init_struct.Mode = GPIO_MODE_AF_PP;
	gpio_init_struct.Pull = GPIO_PULLUP;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	gpio_init_struct.Alternate = GPIO_AF7_USART3;
	hal_status |= HAL_GPIO_Init(DEBUG_UART_PORT, &gpio_init_struct);

	/* UART3 interrupt Init */
	HAL_NVIC_SetPriority(USART3_IRQn, 6U, 0U);
	HAL_NVIC_EnableIRQ(USART3_IRQn);
	HAL_UART_Init(&g_HandleUart3Debug);
}
#endif
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
