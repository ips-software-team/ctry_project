/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : can_protocol_ctry.c
** Author    : Leonid Savchenko
** Revision  : 1.10
** Updated   : 21-05-2023
**
** Description: This file contains definition of CAN BUS interface protocol used by
** 		        CTRY unit.
*/


/* Revision Log:
**
** Rev 1.0  : 22-03-2020 -- Original version created.
** Rev 1.1  : 23-03-2020 -- Update according to requirements
** Rev 1.2  : 27-06-2020 -- g_ModuleParams.Module_ID changed to Get_CTRY_ID()
** Rev 1.3  : 27-08-2020 -- Added CAN commands,Updated both functions
** Rev 1.4  : 17-11-2020 -- Added MOT_Oper In state in status respond message
** Rev 1.5  : 20-11-2020 -- Static analysis update
** Rev 1.6  : 01-01-2021 -- Updated due to verification remarks
** Rev 1.7  : 01-05-2021 -- Added CAN commands for limits update
** Rev 1.8  : 28-06-2021 -- Updated CAN commands
** Rev 1.9  : 19-07-2021 -- Updated Limits change command active in STBY mode only
** Rev 1.10 : 21-05-2023 -- Updated ID_ALL_ACTUATORS_OFF_REQ,
** 							Added IBIT limits receive process
**************************************************************************************
*/

/* -----Includes ---------------------------------------------------------------------*/
#include "can_protocol_ctry.h"
#include "gipsy_hal_can.h"
#include "gipsy_hal_rcc.h"
#include "actuator_ctrl.h"
#include "current_sensor.h"
#include "gpio_control_ctry.h"
#include "id_conf_ctry.h"
#include "mcp9804.h"
#include "module_init_ctry.h"
#include "motor_driver.h"
#include "system_params_ctry.h"
#include "temperature_control.h"
#include "version_ctry.h"
#include "vibration_meas.h"
#include "voltage_meas_ctry.h"
#include "fault_mngmnt_ctry.h"


/* -----Definitions ----------------------------------------------------------------*/
#define CAN_MSG_DATA_LENGH_BYTES      8U            /*CAN message DLC*/

#define DIMC_CAN_ADDR                 30U
#define CAN_BROADCAST_ADDR            0x00U
#define CAN_UNIT_ADDR_LENGTH          5U
#define CAN_UNIT_ADDR_MASK            0x1FU
#define UNDEFINED_LEFT_CTRY_ID        0xFFU
#define UNDEFINED_RIGHT_CTRY_ID       0xFEU

#define ID_ALL_ACTUATORS_OFF_REQ      0x00U
#define ID_CTRY_PBIT_RESULT_REQ       0x01U
#define ID_ACTUATOR_ON_REQ            0x02U
#define ID_CTRY_STATUS_REQ            0x03U
#define ID_CTRYS_STATUS_UPDATE_REQ    0x04U
#define ID_CPU_BRD_TEMP_REQ           0x05U
#define ID_DRV_BRD_TEMP_REQ           0x06U
#define ID_CTRY_CHANNEL_C_CTRL_REQ    0x07U
#define ID_ACTUATORS_LIM_UPD_REQ      0x08U

#define ID_CTRY_PBIT_RESULT_RESP      0x20U
#define ID_CTRY_STATUS_RESP           0x21U
#define ID_CPU_BRD_TEMP_RESP          0x22U
#define ID_DRV_BRD_TEMP_RESP          0x23U
#define ID_ACTUATORS_LIM_UPD_RESP     0x24U

#define ID_DBG_HEATER_CTRL_REQ        0x36U
#define ID_DBG_CTRY_VOLTAGES_REQ      0x37U
#define ID_DBG_CTRY_VOLTAGES_RESP     0x38U

#define ID_DBG_WRITE_MDRV_REG_REQ     0x2AU
#define ID_DBG_READ_MDRV_REGS_REQ     0x2BU
#define ID_DBG_READ_MDRV_REGS_RESP    0x2CU
/* -----Macros ---------------------------------------------------------------------*/
#define COMBINE_CAN_HEADER(msg_id,rec_id)  (uint32_t)(((msg_id) << CAN_UNIT_ADDR_LENGTH) | (rec_id))
#define GET_CAN_MSG_ID_FROM_HEADER(header)  (uint8_t)((header) >> CAN_UNIT_ADDR_LENGTH)
#define GET_CAN_UNIT_ADDR_FROM_HEADER(header)  (uint8_t)((header) & CAN_UNIT_ADDR_MASK)

/* -----External variables ---------------------------------------------------------*/
extern ModuleParameters_st g_ModuleParams;
extern BoardTempSens_t g_BoardTempSens[E_CTRY_NUM_OF_BOARDS];
extern st_VoltageMeasParameters g_VoltageMeasurement[E_VOLT_MAX_TYPES];
extern unsigned char LV8907_ConfReg[24U][2U];

extern uint8_t g_ActLimIbit_Vib_Max;
extern uint8_t g_ActLimIbit_Vib_Min;
extern uint8_t g_ActLimIbit_Curr_Max;
extern uint8_t g_ActLimIbit_Curr_Min;

extern e_ActuatorActivationState g_Act_Off_Req_Status;
/* -----Global variables -----------------------------------------------------------*/
uint32_t CanFailuresCounter = 0U;

#ifdef _DEBUG_
uint32_t g_Can_Status_Msg_Counter = 0U;
#endif

uint8_t Act_Number = 0U;
uint8_t Act_Lim_Mode_Type = 0U;
/* -----Static Function prototypes -------------------------------------------------*/

/* -----Modules implementation -----------------------------------------------------*/


/************************************************************************************
** HAL_CAN_RxFifo0MsgPendingCallback - Rx Fifo 0 message pending callback
**
** Params : None.
**
** Returns: None.
** Notes:
*************************************************************************************/
void HAL_CAN_RxFifo0MsgPendingCallback(void)
{
	CAN_RxHeaderTypeDef   can_rx_header = {0U};
	uint8_t               can_msg_rx_data[CAN_MSG_DATA_LENGH_BYTES] = {0U};
	uint8_t               node_addr = 0U;
	uint32_t			  all_ctrys_status = 0U;
	HAL_StatusTypeDef     hal_status = HAL_OK;
	e_CtryStatus          mon_ctry_status = E_CTRY_OK;
	uint8_t               mon_ctry_id = 0U;
	uint8_t               actuators_status = 0U;

	/* Get RX message */
	if (HAL_OK == HAL_CAN_GetRxMessage(&can_rx_header, can_msg_rx_data))
	{
		node_addr = GET_CAN_UNIT_ADDR_FROM_HEADER(can_rx_header.StdId);

		switch (GET_CAN_MSG_ID_FROM_HEADER(can_rx_header.StdId))/*Check message type*/
		{
		case ID_ALL_ACTUATORS_OFF_REQ:
			/*Command executed by broadcasting or by addressed node*/
			if((CAN_BROADCAST_ADDR == node_addr) || ((Get_CTRY_ID() + 1U) == node_addr))
			{
				/**Turn OFF all motors*/
				/*Set actuators's parameters status as Actuators OFF*/
				g_Act_Off_Req_Status = E_ACT_OFF_REQ;
			}
			break;
		case ID_CTRY_PBIT_RESULT_REQ:
			/*Command executed by addressed node*/
			if((Get_CTRY_ID() + 1U) == node_addr)
			{
				CAN_Message_Transmit(ID_CTRY_PBIT_RESULT_RESP);
			}
			break;
		case ID_ACTUATOR_ON_REQ:
			/*Command executed by broadcasting or by addressed node*/
			if(CAN_BROADCAST_ADDR == node_addr)
			{
				if(E_STBY_MODE == Get_Module_Mode())
				{
					/*Update Left Wing actuator's parameters*/
					if(can_msg_rx_data[0U] > 0U)
					{
						g_ActuatorsPair.ActParamsCurrent[LEFT_WING].CtryId = can_msg_rx_data[0U] - 1U;/*Decrement CTRY's ID to fit range 0 - 19 */
					}
					else
					{
						g_ActuatorsPair.ActParamsCurrent[LEFT_WING].CtryId = UNDEFINED_LEFT_CTRY_ID;
					}
					g_ActuatorsPair.ActParamsCurrent[LEFT_WING].ActNum = can_msg_rx_data[1U];
					g_ActuatorsPair.ActParamsCurrent[LEFT_WING].ActRpm = can_msg_rx_data[2U];

					/*Update Right Wing actuator's parameters*/
					if(can_msg_rx_data[3U] > 0U)
					{
						g_ActuatorsPair.ActParamsCurrent[RIGHT_WING].CtryId = can_msg_rx_data[3U] - 1U;/*Decrement CTRY's ID to fit range 0 - 19 */
					}
					else
					{
						g_ActuatorsPair.ActParamsCurrent[RIGHT_WING].CtryId = UNDEFINED_RIGHT_CTRY_ID;
					}
					g_ActuatorsPair.ActParamsCurrent[RIGHT_WING].ActNum = can_msg_rx_data[4U];
					g_ActuatorsPair.ActParamsCurrent[RIGHT_WING].ActRpm = can_msg_rx_data[5U];

					/*Update actuators's activation mode parameter*/
					g_ActuatorsPair.ActMode = (e_ActuatorActivationMode)can_msg_rx_data[6U];
					/*Set message reception time for further activation interval calculation*/
					g_ActuatorsPair.Act_ON_Time_Current = HAL_GetTick();
					/*Set actuators's parameters status as Updated*/
					g_ActuatorsPair.ActParamsStatus = E_ACT_PARAMS_UPDATED;
				}
				else
				{
					/*Ignore command if CTRY is not in STBY mode*/
					g_ActuatorsPair.ActParamsStatus = E_ACT_PARAMS_IGNORED;
				}
			}
			break;
		case ID_CTRY_STATUS_REQ:
			/*Command executed by addressed node*/
			if((Get_CTRY_ID() + 1U) == node_addr)
			{
				CAN_Message_Transmit(ID_CTRY_STATUS_RESP);
			}
			break;
		case ID_CTRY_STATUS_RESP:
			/*Command monitored by all CTRYs nodes*/
			if(DIMC_CAN_ADDR == node_addr)
			{
				/*CTRYs monitoring - Get monitored CTRY's ID and status*/
				mon_ctry_status = (e_CtryStatus)((can_msg_rx_data[0U] >> 7U) & 0x01U);
				mon_ctry_id = (can_msg_rx_data[0U] & 0x1FU) - 1U;
				/*CTRYs monitoring - Perform status update only in case that monitored CTRY's ID is in range (0-19)*/
				if(CTRY_MAX_NUM > mon_ctry_id)
				{
					/*CTRYs monitoring - Update CTRY's status in status table*/
					hal_status |= Set_CTRY_Status(mon_ctry_id, mon_ctry_status);
					/*CTRYs monitoring - Update CTRY's actuators' status in status table*/
					if(E_CTRY_OK == mon_ctry_status)
					{
						actuators_status = (can_msg_rx_data[5U] >> 2U) & 0x3FU;
						/*CTRYs monitoring - Update OK CTRY's actuators' status in status table from message's data byte*/
						for(uint8_t act_num = 0U; MAX_NUM_OF_MOTOR_DRIVERS > act_num; act_num++)
						{
							/*Update only actuators marked as 'FL'*/
							if((e_ActuatorState)((actuators_status >> act_num) & 0x01U) == 1U)
							{
								hal_status |= Set_Actuator_Status_FL(mon_ctry_id,(e_ActuatorsNum)act_num);
							}
							if(HAL_OK != hal_status)
							{
								Increment_SW_Error_Counter();
							}
						}
					}
				}
			}
			break;
		case ID_CTRYS_STATUS_UPDATE_REQ:
			/*Command executed by broadcasting*/
			if(CAN_BROADCAST_ADDR == node_addr)
			{
				/*Get CTRYs status from RX message data*/
				all_ctrys_status = (uint32_t)(((uint32_t)can_msg_rx_data[0U]) << 16U) | (uint32_t)(((uint32_t)can_msg_rx_data[1U]) << 8U) | (uint32_t)(can_msg_rx_data[2U]);
				for(uint8_t ctry_num = 0U; ctry_num < CTRY_MAX_NUM; ctry_num++)
				{
					/*Update all CTRYs' status in system status table except current one*/
					if(ctry_num != Get_CTRY_ID())
					{
						hal_status |= Set_CTRY_Status(ctry_num,(e_CtryStatus)((all_ctrys_status >> ctry_num) & 0x00000001U));
					}
				}
				if(HAL_OK != hal_status)
				{
					Increment_SW_Error_Counter();
				}
			}
			break;
		case ID_CPU_BRD_TEMP_REQ:
			/*Command executed by addressed node*/
			if((Get_CTRY_ID() + 1U) == node_addr)
			{
				CAN_Message_Transmit(ID_CPU_BRD_TEMP_RESP);
			}
			break;
		case ID_DRV_BRD_TEMP_REQ:
			/*Command executed by addressed node*/
			if((Get_CTRY_ID() + 1U) == node_addr)
			{
				CAN_Message_Transmit(ID_DRV_BRD_TEMP_RESP);
			}
			break;
		case ID_CTRY_CHANNEL_C_CTRL_REQ:
			/*Command executed by broadcasting or by addressed node*/
			if((CAN_BROADCAST_ADDR == node_addr) ||((Get_CTRY_ID() + 1U) == node_addr))
			{
				/*Set required Channel_C output state*/
				Fault_Channel_C_Control((e_Flt_Ch_C_State)can_msg_rx_data[0U]);
				g_ModuleParams.Flt_Ch_C_Status = can_msg_rx_data[0U];
			}
			break;
		case ID_DBG_CTRY_VOLTAGES_REQ:
			/*Command executed by addressed node*/
			if((Get_CTRY_ID() + 1U) == node_addr)
			{
				CAN_Message_Transmit(ID_DBG_CTRY_VOLTAGES_RESP);
			}
			break;
		case ID_ACTUATORS_LIM_UPD_REQ:
			if(E_STBY_MODE == Get_Module_Mode())/*Limits can be updated only in STBY mode*/
			{
				/*Command executed by addressed node*/
				if((Get_CTRY_ID() + 1U) == node_addr)
				{
					/*Save OPER limits into configuration table*/
					Act_Lim_Mode_Type = (uint8_t)(can_msg_rx_data[0U] & 0x80U);
					Act_Number = (uint8_t)(can_msg_rx_data[0U] & 0x07U);/*Save actuator's number for response*/

					/*Set OPER Limits*/
					if(ACT_LIM_OPER_MODE == Act_Lim_Mode_Type)
					{
						Set_Actuator_Limits(Act_Lim_Mode_Type,(e_ActuatorsNum)(Act_Number),can_msg_rx_data[1U],can_msg_rx_data[2U],can_msg_rx_data[3U],can_msg_rx_data[4U]);
					}
					/*Set IBIT Limits*/
					if(ACT_LIM_IBIT_MODE == Act_Lim_Mode_Type)
					{
						g_ActLimIbit_Vib_Max  = can_msg_rx_data[1U];
						g_ActLimIbit_Vib_Min  = can_msg_rx_data[2U];
						g_ActLimIbit_Curr_Max = can_msg_rx_data[3U];
						g_ActLimIbit_Curr_Min = can_msg_rx_data[4U];
					}

					CAN_Message_Transmit(ID_ACTUATORS_LIM_UPD_RESP);
				}
			}
			break;
            /*----------------------------------------*/
			/*Commands for Debug purposes*/
			/*----------------------------------------*/
#ifdef _DEBUG_
		case ID_DBG_HEATER_CTRL_REQ:
			/*Command executed by addressed node*/
			if((Get_CTRY_ID() + 1U) == node_addr)
			{
				/*Set required heater state*/
				CTRY_Control_Heaters(can_msg_rx_data[0U], can_msg_rx_data[1U]);
			}
			break;
#endif

		default:
			break;
		}
	}
	else
	{
		/*Error reading CAN message from FIFO buffer*/
		CanFailuresCounter++;
	}
}
/************************************************************************************
** CAN_Message_Transmit - This function transmits CAN message via CAN BUS interface
**
** Params : uint8_t message_type - Type of message to transmit
**
** Returns: None.
** Notes:
*************************************************************************************/
void CAN_Message_Transmit(uint8_t message_type)
{
	uint8_t 	valid_message = TRUE;
	uint8_t     can_msg_tx_data[CAN_MSG_DATA_LENGH_BYTES] = {0U};
	uint32_t    msg_std_id = 0U;
	uint8_t 	all_act_status = 0U;
	HAL_StatusTypeDef     hal_status = HAL_OK;

	/*Combine TX message according to it's type*/
	switch(message_type)
	{
	case ID_CTRY_PBIT_RESULT_RESP:
		/*Combine PBIT_RESULT_RESP message data*/
		msg_std_id = COMBINE_CAN_HEADER( message_type, DIMC_CAN_ADDR);
		can_msg_tx_data[0U] = Get_CTRY_ID() + 1U; /*Increment CTRY's ID to fit range 1 - 20 */
		can_msg_tx_data[1U] = g_ModuleParams.Pbit_Status & 0x01U;
		can_msg_tx_data[2U] = _VER_MAJOR_;
		can_msg_tx_data[3U] = _VER_MINOR_;
		can_msg_tx_data[4U] = (uint8_t)((g_ModuleParams.Pbit_Failure_Desc >> 8U) & 0x00FFU);
		can_msg_tx_data[5U] = (uint8_t)(g_ModuleParams.Pbit_Failure_Desc & 0x00FFU);
		break;
	case ID_CTRY_STATUS_RESP:
		/*Combine CTRY_STATUS_RESP message data*/
		msg_std_id = COMBINE_CAN_HEADER( message_type, DIMC_CAN_ADDR);
		can_msg_tx_data[0U] = Get_CTRY_ID() + 1U; /*Increment CTRY's ID to fit range 1 - 20 */
		if(0U != g_ModuleParams.Cbit_Status)
		{
			can_msg_tx_data[0U] |= 0x80U;
		}
		if(0U != g_ModuleParams.Flt_Ch_C_Status)
		{
			can_msg_tx_data[0U] |= 0x40U;
		}
		if(E_GPIO_PIN_SET == Read_MotOper_Input()) /*Read MOT_Oper input state*/
		{
			can_msg_tx_data[0U] |= 0x20U;
		}
		can_msg_tx_data[1U] = Get_Last_Acceleration_Val();
		can_msg_tx_data[3U] = g_MeasuredCurrent.CurrentValue;
		can_msg_tx_data[4U] = g_BoardTempSens[E_CPU_BOARD].BoardSensorsStatus & 0x0FU;
		can_msg_tx_data[4U] |= ((g_BoardTempSens[E_DRV_BOARD].BoardSensorsStatus << 4U ) & 0xF0U);
		if (HAL_OK == Get_CTRY_All_Actuators_Status(Get_CTRY_ID(),&all_act_status))
		{
			can_msg_tx_data[5U] = (all_act_status << 2U) & 0xFCU;
		}
		else
		{
			valid_message = FALSE;/*don't send the message*/
		}
		can_msg_tx_data[5U] |= g_BoardTempSens[E_CPU_BOARD].BoardHeaterState & 0x01U;
		can_msg_tx_data[5U] |= ((g_BoardTempSens[E_DRV_BOARD].BoardHeaterState << 1U) & 0x02U);
		can_msg_tx_data[6U] = (uint8_t)((g_ModuleParams.Ctry_Failure_Desc >> 8U) & 0x00FFU);
		can_msg_tx_data[7U] = (uint8_t)(g_ModuleParams.Ctry_Failure_Desc & 0x00FFU);
		break;
	case ID_CPU_BRD_TEMP_RESP:
		/*Combine CPU_BRD_TEMP_RESP message data*/
		msg_std_id = COMBINE_CAN_HEADER( message_type, DIMC_CAN_ADDR);
		can_msg_tx_data[0U] = Get_CTRY_ID() + 1U; /*Increment CTRY's ID to fit range 1 - 20 */
		for(uint8_t i = 0U; i < MAX_SENSORS_ON_BOARD; i++)
		{
			can_msg_tx_data[i+1U] = (uint8_t)g_BoardTempSens[E_CPU_BOARD].Temperatue[i];
		}
		can_msg_tx_data[5U] = g_BoardTempSens[E_CPU_BOARD].BoardSensorsStatus & 0x0FU;
		can_msg_tx_data[5U] |= ((g_BoardTempSens[E_DRV_BOARD].BoardSensorsStatus << 4U ) & 0xF0U);
		can_msg_tx_data[6U] = g_BoardTempSens[E_CPU_BOARD].BoardHeaterState & 0x01U;
		can_msg_tx_data[6U] |= ((g_BoardTempSens[E_DRV_BOARD].BoardHeaterState << 1U) & 0x02U);
		break;
	case ID_DRV_BRD_TEMP_RESP:
		/*Combine DRV_BRD_TEMP_RESP message data*/
		msg_std_id = COMBINE_CAN_HEADER( message_type, DIMC_CAN_ADDR);
		can_msg_tx_data[0U] = Get_CTRY_ID() + 1U; /*Increment CTRY's ID to fit range 1 - 20 */
		for(uint8_t i = 0U; i < MAX_SENSORS_ON_BOARD; i++)
		{
			can_msg_tx_data[i+1U] = (uint8_t)g_BoardTempSens[E_DRV_BOARD].Temperatue[i];
		}
		can_msg_tx_data[5U] = g_BoardTempSens[E_CPU_BOARD].BoardSensorsStatus & 0x0FU;
		can_msg_tx_data[5U] |= ((g_BoardTempSens[E_DRV_BOARD].BoardSensorsStatus << 4U ) & 0xF0U);
		can_msg_tx_data[6U] = g_BoardTempSens[E_CPU_BOARD].BoardHeaterState & 0x01U;
		can_msg_tx_data[6U] |= ((g_BoardTempSens[E_DRV_BOARD].BoardHeaterState << 1U) & 0x02U);
		break;
	case ID_DBG_CTRY_VOLTAGES_RESP:
		/*Combine ID_DBG_CTRY_VOLTAGES_RESP message data*/
		msg_std_id = COMBINE_CAN_HEADER( message_type, DIMC_CAN_ADDR);
		can_msg_tx_data[0U] = Get_CTRY_ID() + 1U; /*Increment CTRY's ID to fit range 1 - 20 */
		for(uint8_t i = 0U; i < (uint8_t)E_VOLT_MAX_TYPES; i++)
		{
			can_msg_tx_data[i+1U] = (uint8_t)(g_VoltageMeasurement[i].VoltageValue * 10U);
			can_msg_tx_data[4U] |= ((uint8_t)(g_VoltageMeasurement[i].VoltageTestResult) << i);
		}
		break;
	case ID_ACTUATORS_LIM_UPD_RESP:
		/*Combine ID_ACTUATORS_LIM_UPD_RESP message data*/
		msg_std_id = COMBINE_CAN_HEADER( message_type, DIMC_CAN_ADDR);
		can_msg_tx_data[0U] = Get_CTRY_ID() + 1U; /*Increment CTRY's ID to fit range 1 - 20 */
		can_msg_tx_data[1U] = Act_Number;
		can_msg_tx_data[1U] |= Act_Lim_Mode_Type;

		if(ACT_LIM_OPER_MODE == Act_Lim_Mode_Type)
		{
			hal_status = Get_Actuator_VibrLimits(Get_CTRY_ID(), (e_ActuatorsNum)Act_Number, &can_msg_tx_data[2U], &can_msg_tx_data[3U]);
			hal_status |= Get_Actuator_CurrLimits(Get_CTRY_ID(), (e_ActuatorsNum)Act_Number, &can_msg_tx_data[4U], &can_msg_tx_data[5U]);
		}
		if(ACT_LIM_IBIT_MODE == Act_Lim_Mode_Type)
		{
			can_msg_tx_data[2U] = g_ActLimIbit_Vib_Max;
			can_msg_tx_data[3U] = g_ActLimIbit_Vib_Min;
			can_msg_tx_data[4U] = g_ActLimIbit_Curr_Max;
			can_msg_tx_data[5U] = g_ActLimIbit_Curr_Min;
		}

		if(HAL_OK != hal_status)
		{
			/*In case that reading limits from the table failed, fill limits data bytes with zeroes*/
			can_msg_tx_data[2U] = 0x00U;
			can_msg_tx_data[3U] = 0x00U;
			can_msg_tx_data[4U] = 0x00U;
			can_msg_tx_data[5U] = 0x00U;
		}
		break;
	default:
		valid_message = FALSE;
		break;
	}
	if(TRUE == valid_message)
	{
		/* Start the Transmission process */
		if (HAL_OK != HAL_CAN_AddTxMessage(msg_std_id, can_msg_tx_data))
		{
			/* Transmission request Error */
			CanFailuresCounter++;
		}
	}
}

 /************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
