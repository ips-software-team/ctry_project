/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : temperature_control.c
** Author    : Leonid Savchenko
** Revision  : 1.8
** Updated   : 07-05-2023
**
** Description: This file contains procedures for monitoring CTRY's temperature
**              and controlling heating circuit.
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1  : 17-01-2020 -- Updated "CTRY_Control_Heaters" method.
** Rev 1.2  : 01-10-2020 -- Updated CTRY_Temperature_Management()
** Rev 1.3  : 20-11-2020 -- Static analysis update
** Rev 1.4  : 02-06-2021 -- Updated temperature defines and temperature management scenarios
** Rev 1.5  : 27-06-2021 -- Removed under-temperature case
** Rev 1.6  : 11-07-2021 -- Added doxygen comments
** Rev 1.7  : 21-09-2021 -- Added manage_boards_temperature(), updated CTRY_Temperature_Management()
** Rev 1.8  : 07-05-2023 -- Fixed temperature read bug in manage_boards_temperature() that limited temperature
**                          read to 63 deg.C. (0x3F changed to 0x7F)
**                          Updated Over-Temp detection: By 2 or more sensors.
**************************************************************************************
*/


/* -----Includes ---------------------------------------------------------------------*/

#include "temperature_control.h"

#include "actuator_ctrl.h"
#include "mcp9804.h"
#include "mcu_config_ctry.h"
#include "module_init_ctry.h"
#include "fault_mngmnt_ctry.h"
/* -----Definitions ----------------------------------------------------------------*/
#define ALL_BOARD_SENSORS_FAILED  0x0FU

/*Temperature values are Celsius degrees*/
#define TEMPERATURE_OVER_TEMP_MAIN_VALUE	     (int8_t)(100)
#define TEMPERATURE_OVER_TEMP_SECONDARY_VALUE	 (int8_t)(70)
#define TEMPERATURE_UNDER_TEMP	                 (int8_t)(-42)
#define TEMPERATURE_LOW_HYST_MIN                 (int8_t)(-5)
#define TEMPERATURE_LOW_HYST_MAX                 (int8_t)(25)

#define MIN_NUM_OF_SENS_FOR_MNGMT                (uint8_t)2U
/* -----Macros ---------------------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/
extern ModuleParameters_st g_ModuleParams;
/* -----Global variables -----------------------------------------------------------*/
/*!Specifies temperature sensors general parameters (Range: var type | Units: see var declaration)*/
/*!\sa Temperatue - Set function: Temperaure_Sensors_Read()*/
/*!\sa Temperatue - Get function: CAN_Message_Transmit(), manage_boards_temperature()*/
/*!\sa SensorID - Set function: Temperature_Sensor_Check_ID()*/
/*!\sa SensorID - Get function: */
/*!\sa BoardTemperatureStatus - Set function: manage_boards_temperature()*/
/*!\sa BoardTemperatureStatus - Get function: manage_boards_temperature()*/
/*!\sa BoardHeaterState - Set function: CTRY_Control_Heaters()*/
/*!\sa BoardHeaterState - Get function: CAN_Message_Transmit()*/
/*!\sa BoardSensorsStatus - Set function: CTRY_Temperature_Management()*/
/*!\sa BoardSensorsStatus - Get function: CAN_Message_Transmit(), CTRY_Temperature_Management()*/

BoardTempSens_t g_BoardTempSens[E_CTRY_NUM_OF_BOARDS] =
{
		{
			{TEMP_ERR_VALUE,TEMP_ERR_VALUE,TEMP_ERR_VALUE,TEMP_ERR_VALUE},
			{0x0000U, 0x0000U, 0x0000U, 0x0000U},
			E_BOARD_TEMP_OK,
			HEATER_OFF,
			0U
		},
		{
			{TEMP_ERR_VALUE,TEMP_ERR_VALUE,TEMP_ERR_VALUE,TEMP_ERR_VALUE},
			{0x0000U, 0x0000U, 0x0000U, 0x0000U},
			E_BOARD_TEMP_OK,
			HEATER_OFF,
			0U
		},
};

/* -----Static Function prototypes -------------------------------------------------*/
static void manage_boards_temperature(BoardTempSens_t *);
/* -----Modules implementation -----------------------------------------------------*/

/*************************************************************************************
** CTRY_Temperature_Management - This function performs CTRY's temperature management
** 								 by reading all temperature sensors, defining current
** 								 temperature status, controlling heaters and setting
** 								 board's thermal state.
** Params : None.
**
** Returns: None.
** Notes: Temperature management performed for each one of CTRY's boards (CPU/DRV)
**        independently.
*************************************************************************************/
void CTRY_Temperature_Management(void)
{
	uint8_t temp_sens_status = 0U;

	/*Read all Temperature Sensors*/
	temp_sens_status = Temperaure_Sensors_Read(g_BoardTempSens);

	/*Set board's sensors status as bit mask of lower 4 bits*/
	g_BoardTempSens[E_CPU_BOARD].BoardSensorsStatus = temp_sens_status & 0x0FU;
	g_BoardTempSens[E_DRV_BOARD].BoardSensorsStatus = (temp_sens_status >> 4U) & 0x0FU;

	manage_boards_temperature(g_BoardTempSens);
}

/*************************************************************************************
** manage_board_temperature - This function performs boards temperature management
** 								 by getting all temperature sensors values, defining current
** 								 temperature status and controlling heaters.
** Params : None.
**
** Returns: None.
** Notes:
*************************************************************************************/
static void manage_boards_temperature(BoardTempSens_t *brd_temp_data_st)
{
	int8_t temp_value_signed[E_CTRY_NUM_OF_BOARDS][MAX_SENSORS_ON_BOARD] = {0U};/*2D array for signed temperature values storage*/
	uint8_t board_fl_sensors_counter = 0U;
	uint8_t over_temp_main_value_counter = 0U;
	uint8_t over_temp_sens_counter = 0U;
	uint8_t low_temp_sens_counter = 0U;
	uint8_t high_temp_sens_counter = 0U;

	/*Check input parameters validity*/
	if(NULL != brd_temp_data_st)
	{
		/*Perform board's temperature management*/
	    for(uint8_t board_num = 0U; board_num < (uint8_t)E_CTRY_NUM_OF_BOARDS ; board_num++)
	    {
	    	board_fl_sensors_counter = 0U;
	    	over_temp_main_value_counter = 0U;
	    	over_temp_sens_counter = 0U;
	    	low_temp_sens_counter = 0U;
	    	high_temp_sens_counter = 0U;

	    	for(uint8_t sens_num = 0U; MAX_SENSORS_ON_BOARD > sens_num ; sens_num++)
	    	{
	    		/*Convert valid temperature values to signed ones*/
	    		if(TEMP_ERR_VALUE != brd_temp_data_st[board_num].Temperatue[sens_num]) /*Check if the measurement valid*/
				{
					/*Check temperature value sigh and convert to signed value*/
					if (0x80U == (brd_temp_data_st[board_num].Temperatue[sens_num] & 0x80U))
					{
						temp_value_signed[board_num][sens_num] = (brd_temp_data_st[board_num].Temperatue[sens_num] & 0x7FU) * -1;
					}
					else
					{
						temp_value_signed[board_num][sens_num] = (int8_t)(brd_temp_data_st[board_num].Temperatue[sens_num] & 0x7FU);
					}
				}
				else
				{
					temp_value_signed[board_num][sens_num] = TEMP_ERR_VALUE;
					board_fl_sensors_counter++;
				}

	    		/*Check each valid temperature value according to specified limits*/
	    		if(TEMP_ERR_VALUE != brd_temp_data_st[board_num].Temperatue[sens_num]) /*Check if the measurement valid*/
	    		{
	    			/*Case Check: Over-Temperature - Max temperature detection(TEMPERATURE_OVER_TEMP_MAIN_VALUE)*/
	    			if(TEMPERATURE_OVER_TEMP_MAIN_VALUE <= temp_value_signed[board_num][sens_num])
	    			{
	    				over_temp_main_value_counter++;
	    			}
	    			/*Case Check: If sensor value above Over-Temp threshold(TEMPERATURE_OVER_TEMP_SECONDARY_VALUE)*/
	    			if((TEMPERATURE_OVER_TEMP_SECONDARY_VALUE < temp_value_signed[board_num][sens_num]))
	    			{
	    				over_temp_sens_counter++;
	    			}
	    			/*Case Check:If sensor value below  Low-Temperature threshold*/
	    			if(TEMPERATURE_LOW_HYST_MIN > temp_value_signed[board_num][sens_num])
					{
	    				low_temp_sens_counter++;
					}
	    			/*Case Check:If sensor value above  High-Temperature threshold*/
					if(TEMPERATURE_LOW_HYST_MAX < temp_value_signed[board_num][sens_num])
					{
						high_temp_sens_counter++;
					}
	    		}
	    	}

			/*Manage heaters according to temperature values*/
			/*Case Check: If detected Over-Temp and all valid sensors' value above Over-Temp threshold(TEMPERATURE_OVER_TEMP_SECONDARY_VALUE)*/
			if((MIN_NUM_OF_SENS_FOR_MNGMT <= over_temp_main_value_counter) && (MAX_SENSORS_ON_BOARD == (over_temp_sens_counter + board_fl_sensors_counter)))
			{
				/*Set error of OverTemperature*/
				brd_temp_data_st[board_num].BoardTemperatureStatus = (uint8_t)E_BOARD_OVER_TEMP;
				/*Make sure that board's heaters are OFF*/
				CTRY_Control_Heaters(board_num,HEATER_OFF);
				/*Set failure*/
				g_ModuleParams.Ctry_Failure_Desc |= CTRY_FLT_OVER_TEMP;
			}
			/*Case Check: Enter High-Temperature*/
			else if(MIN_NUM_OF_SENS_FOR_MNGMT <= high_temp_sens_counter)
			{
				/*Set board's temperature status*/
				brd_temp_data_st[board_num].BoardTemperatureStatus = (uint8_t)E_BOARD_TEMP_OK;
				/*Turn OFF board's heaters*/
				CTRY_Control_Heaters(board_num,HEATER_OFF);
			}
			/*Case Check: Enter High-Temperature*/
			else if(MIN_NUM_OF_SENS_FOR_MNGMT <= low_temp_sens_counter)
			{
				/*Set board's temperature status*/
				brd_temp_data_st[board_num].BoardTemperatureStatus = (uint8_t)E_BOARD_TEMP_OK;
				/*Turn ON board's heaters*/
				CTRY_Control_Heaters(board_num,HEATER_ON);
			}
			else
			{
				/*Case Check: More than 2 board sensors are fail*/
				if(MIN_NUM_OF_SENS_FOR_MNGMT < board_fl_sensors_counter)
				{
					/*Set board's temperature status*/
					brd_temp_data_st[board_num].BoardTemperatureStatus = (uint8_t)E_BOARD_TEMP_FL;
					/*Turn OFF board's heaters to prevent unmanaged heating*/
					CTRY_Control_Heaters(board_num,HEATER_OFF);
				}
			}
	    }
	}
}
/*************************************************************************************
** CTRY_Control_Heaters - This function controls CPU/DRV boards heaters.
** Params : uint8_t board_type - Type of the board(CPU/DRV).
** 			uint8_t heater_state - Heater's required state (SET/RESET)
**
** Returns: None.
** Notes:
*************************************************************************************/
void CTRY_Control_Heaters(uint8_t board_type, uint8_t heater_state)
{
	if((HEATER_ON == heater_state) || (HEATER_OFF == heater_state))
	{
		if(E_CPU_BOARD == board_type)
		{
			HAL_GPIO_WritePin(CARD_HEATING_ON_PORT,CPU_CARD_HEATING_ON_PIN,(GPIO_PinState)heater_state);
			g_BoardTempSens[board_type].BoardHeaterState = heater_state;
		}
		if(E_DRV_BOARD == board_type)
		{
			HAL_GPIO_WritePin(CARD_HEATING_ON_PORT,DRV_CARD_HEATING_ON_PIN,(GPIO_PinState)heater_state);
			g_BoardTempSens[board_type].BoardHeaterState = heater_state;
		}
	}
}
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
