/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : motor_driver.c
** Author    : Leonid Savchenko
** Revision  : 1.6
** Updated   : 19-06-2021
**
** Description: This file provides procedures for control, monitor and initialization of BLDC controller
*/


/* Revision Log:
**
** Rev 1.0  : 22-07-2020 -- Original version created.
** Rev 1.1  : 24-08-2020 -- Fixed bug in mdrv_set_pwm_value() and added range and input parameter check
** 						- MDRV_Enable_Ctrl()
** 						- added WDOG_Trigger() in MDRV_Config_All()
** 						- added register write verification in MDRV_Config_All()
** 						- Changed delay in mdrv_spi_delay()
** 						- Renamed and Updated MDRV_Config()  (was -MDRV_Config_All())
** 						- Changed return type in MDRV_ID_Check()
** 						- Changed TIM3/4 handle variables names
** 						- Updated mdrv_set_pwm_value()
** 						- PWM_MAX_VAL changed to 95
** Rev 1.2  : 12-11-2020  -- Added missing "else" in MDRV_Config()
** Rev 1.3  : 20-11-2020 -- Static analysis update
** Rev 1.4  : 22-11-2020 -- Updated due to verification remarks
** Rev 1.5  : 09-02-2021 -- Updated MDRV_Config() - added registers configuration selection
** Rev 1.6  : 19-06-2021 -- Updated MDRV_Config() - removed registers configuration selection
**************************************************************************************
*/

/* -----Includes ---------------------------------------------------------------------*/
#include "motor_driver.h"
#include "gipsy_hal_rcc.h"
#include "mcu_config_ctry.h"
#include "id_conf_ctry.h"

/* -----Definitions ----------------------------------------------------------------*/

#define PWM_MIN_VAL 0U
#define PWM_MAX_VAL 95U
#define PWM_SETUP_TIME_MILIS 250U

#define MDRV_TEST_VALUE   (uint8_t)0x55U
#define MDRV_PIN_READ_ERR 0xFFU

/*BLDC register addresses*/
#define MRCONF0					0x00U
#define MRCONF2					0x02U
#define MRCONF4					0x04U
#define MRCONF7					0x07U
#define MRCONF11				0x0BU
#define MRSPCT0					0x10U
#define MRSPCT1					0x11U
#define MRSPCT2					0x12U
#define MRSPCT3					0x13U
#define MRSPCT4					0x14U
#define MRSPCT5					0x15U
#define MRSPCT6					0x16U
#define MRSPCT7					0x17U
#define MRSPCT8					0x18U
#define MRSPCT9					0x19U
#define MRSPCT10				0x1AU
#define MRDIAG0					0x34U
#define MRDIAG1					0x35U
#define MRACS					0x20U
#define MRACK					0x30U
#define MRRST					0x32U

#define MDRV_NUM_OF_REG   24U

#define ADDRESS_ROW	 0U
#define ACT_REG_DATA_ROW 1U


/* -----Macros ---------------------------------------------------------------------*/
/* -----External variables ---------------------------------------------------------*/
/* -----Global variables -----------------------------------------------------------*/


uint8_t LV8907_ConfReg[MDRV_NUM_OF_REG][3U] =
{
/*Info:REG_ADDR    BIG/SMALL_ACT*/
		{0x00U,		0xFDU},
		{0x01U,		0x20U},
		{0x02U,		0x30U},
		{0x03U,		0xC0U},
		{0x04U,		0x0AU},
		{0x05U,		0xFFU},
		{0x06U,		0x00U},
		{0x07U,	 	0x18U},
		{0x08U,	 	0x18U},
		{0x09U,	 	0x3EU},
		{0x0AU,	 	0x00U},
		{0x0BU,	 	0x06U},
		{0x0CU,	 	0x00U},

		{0x10U,	 	0x20U},
		{0x11U,	 	0x14U},
		{0x12U,	    0x0BU},
		{0x13U,	 	0x14U},
		{0x14U,	 	0x1FU},
		{0x15U,	 	0x2AU},
		{0x16U,	 	0x35U},
		{0x17U,	 	0x40U},
		{0x18U,	 	0x4BU},
		{0x19U,	 	0x56U},
		{0x1AU,	 	0x5FU}
};

const st_MdrvPinDescriptor MdrvPins[MAX_NUM_OF_MOTOR_DRIVERS]=
{
		{
				{MDRV_SPI_CS2_PIN, MDRV_SPI_CS_PORT},
				{MDRV_WAKE2_PIN,   MDRV_WAKE_PORT  },
				{MDRV_EN2_PIN,     MDRV_EN_PORT    },
				{MDRV_DIAG2_PIN,   MDRV_DIAG2_PORT },
		},
		{
				{MDRV_SPI_CS3_PIN, MDRV_SPI_CS_PORT},
				{MDRV_WAKE3_PIN,   MDRV_WAKE_PORT  },
				{MDRV_EN3_PIN,     MDRV_EN_PORT    },
				{MDRV_DIAG3_PIN,   MDRV_DIAG3_PORT },
		},
		{
				{MDRV_SPI_CS4_PIN, MDRV_SPI_CS_PORT},
				{MDRV_WAKE4_PIN,   MDRV_WAKE_PORT  },
				{MDRV_EN4_PIN,     MDRV_EN_PORT    },
				{MDRV_DIAG4_PIN,   MDRV_DIAG4_PORT },
		},
		{
				{MDRV_SPI_CS5_PIN, MDRV_SPI_CS_PORT},
				{MDRV_WAKE5_PIN,   MDRV_WAKE_PORT  },
				{MDRV_EN5_PIN,     MDRV_EN_PORT    },
				{MDRV_DIAG5_PIN,   MDRV_DIAG5_PORT },
		},
		{
				{MDRV_SPI_CS6_PIN, MDRV_SPI_CS_PORT},
				{MDRV_WAKE6_PIN,   MDRV_WAKE_PORT  },
				{MDRV_EN6_PIN,     MDRV_EN_PORT    },
				{MDRV_DIAG6_PIN,   MDRV_DIAG6_PORT },
		},
		{
				{MDRV_SPI_CS1_PIN, MDRV_SPI_CS_PORT},
				{MDRV_WAKE1_PIN,   MDRV_WAKE_PORT  },
				{MDRV_EN1_PIN,     MDRV_EN_PORT    },
				{MDRV_DIAG1_PIN,   MDRV_DIAG1_PORT },
		}
};


st_MdrvPwmChannels MdrvPwmCh[MAX_NUM_OF_MOTOR_DRIVERS]=
{
	{&g_HandleTim3,	TIM_CHANNEL_2},
	{&g_HandleTim3,	TIM_CHANNEL_3},
	{&g_HandleTim4,	TIM_CHANNEL_2},
	{&g_HandleTim4,	TIM_CHANNEL_3},
	{&g_HandleTim4,	TIM_CHANNEL_4},
	{&g_HandleTim3,	TIM_CHANNEL_1}
};

/* -----Static Function prototypes -------------------------------------------------*/

static void mdrv_set_pwm_value(uint8_t mdrv_num, uint8_t pwm_percent_val);
static void mdrv_set_spi_clk(GPIO_PinState pin_state);
static void mdrv_spi_delay(void);
static void mdrv_set_cs_pin(uint8_t mdrv_num, GPIO_PinState pin_state);

/* -----Modules implementation -----------------------------------------------------*/


/*************************************************************************************
** MDRV_Wake_All - This function makes the motor drivers exit from sleep mode.
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
void MDRV_Wake_All(void)
{
	MDRV_Enable_Ctrl(MDRV_ALL_PINS,E_GPIO_PIN_RESET);/*Disable all motor drivers*/
	MDRV_Wake_Ctrl(MDRV_ALL_PINS,E_GPIO_PIN_SET);/*wake all motor drivers*/
}
/*************************************************************************************
** MDRV_Config - This function performs configuration of MDRVs internal registers
** 					 by writing values to register addresses located at LV8907_ConfReg array.
** 					 In addition it sets PWM input to minimal value (0).
**
** Params : uint8_t mdrv_num - motor driver number(0-5)
**
** Returns: e_MDRVStatus - MDRVs Init status (E_MDRV_OK = OK | E_MDRV_FL = Fail)
** Notes:
*************************************************************************************/
e_MDRVStatus MDRV_Config(uint8_t mdrv_num)
{
	e_MDRVStatus mdrv_init_status = E_MDRV_OK;
	uint8_t conf_data_rd = 0U;
	uint8_t reg_data_rd = 0U;

	if(MAX_NUM_OF_MOTOR_DRIVERS > mdrv_num )
	{
		MDRV_Enable_Ctrl(mdrv_num,E_GPIO_PIN_RESET);/*disable motor operation */
		if(E_MDRV_OK == MDRV_ID_Check(mdrv_num))
		{
			for(uint8_t i = 0U; i < MDRV_NUM_OF_REG; i++)
			{
				MDRV_SPI_Write(mdrv_num,LV8907_ConfReg[i][ADDRESS_ROW], LV8907_ConfReg[i][ACT_REG_DATA_ROW]);
			}
			/*Verify register value by reading it*/
			if(E_MDRV_OK == mdrv_init_status)
			{
				for(uint8_t i = 0U; (i < (uint8_t)MDRV_NUM_OF_REG) && (E_MDRV_OK == mdrv_init_status); i++)
				{
					MDRV_SPI_Read(mdrv_num,LV8907_ConfReg[i][ADDRESS_ROW],&conf_data_rd,&reg_data_rd);
					if( LV8907_ConfReg[i][ACT_REG_DATA_ROW] != reg_data_rd)
					{
						/*Register verification failed*/
						mdrv_init_status = E_MDRV_FL;
					}
				}
			}
		}
		else
		{
			/*MDRV ID Test failed*/
			mdrv_init_status = E_MDRV_FL;
		}
		/*set PWM input to minimal value - 0*/
		mdrv_set_pwm_value(mdrv_num, PWM_MIN_VAL);
	}
	else
	{
		mdrv_init_status = E_MDRV_FL; /*Undefined motor driver number*/
	}

	if(E_MDRV_FL == mdrv_init_status)
	{
		MDRV_Wake_Ctrl(mdrv_num,E_GPIO_PIN_RESET);/*Disable failed motor drivers*/
	}
	return mdrv_init_status;
 }
/*************************************************************************************
** MDRV_Shut_Down -    This function performs complete shut down motor driver
** 						by disabling the motor driver,setting input PWM to minimal value
** 						end entering the driver into sleep mode by setting Wake pin to '0'
**
** Params : uint8_t mdrv_num - Motor driver number (0-5 or NUM_OF_MOTOR_DRIVERS for shutting
** 							   down all motor drivers)
**
** Returns: None
** Notes: This function called in safety condition when total motor driver's shut down required.
*************************************************************************************/
void MDRV_Shut_Down(uint8_t mdrv_num)
{
	if(MAX_NUM_OF_MOTOR_DRIVERS > mdrv_num)
	{
		MDRV_Enable_Ctrl(mdrv_num,E_GPIO_PIN_RESET);/*disable motor operation */
		MDRV_Wake_Ctrl(mdrv_num,E_GPIO_PIN_RESET);/*deactivate motor driver */
		mdrv_set_pwm_value(mdrv_num, PWM_MIN_VAL);/*set PWM input to minimal value - 0*/
	}
	else if(MAX_NUM_OF_MOTOR_DRIVERS == mdrv_num)
	{
		for(uint8_t i = 0U; i < MAX_NUM_OF_MOTOR_DRIVERS; i++)
		{
			MDRV_Enable_Ctrl(i,E_GPIO_PIN_RESET);/*disable motor operation */
			MDRV_Wake_Ctrl(i,E_GPIO_PIN_RESET);/*deactivate motor driver */
			mdrv_set_pwm_value(i, PWM_MIN_VAL);/*set PWM input to minimal value - 0*/
		}
	}
	else
	{
		/*Do nothing*/
	}
}
/*************************************************************************************
** MDRV_Activate_Motor - This function performs motor's activation at required speed.
**
** Params : uint8_t mdrv_num - Motor driver number (0-5)
** 			uint8_t mot_speed - Motor's required speed
**
** Returns: None
** Notes:
*************************************************************************************/
void MDRV_Activate_Motor(uint8_t mdrv_num,uint8_t mot_speed)
{
	mdrv_set_pwm_value(mdrv_num, mot_speed);/*set PWM input to required value - 0-100*/
	HAL_Delay(PWM_SETUP_TIME_MILIS);
	MDRV_Enable_Ctrl(mdrv_num,E_GPIO_PIN_SET);/*enable motor operation */
}
/*************************************************************************************
** MDRV_ID_Check - This function performs SPI bus test
** 						by reading the value of MRACK register. Correct value shall be 0x55
**
** Params : uint8_t mdrv_num -  Number of the Motor Driver (0-5)
**
** Returns: e_MDRVStatus  - Result of the SPI test (E_MDRV_OK or E_MDRV_FL)
** Notes:
*************************************************************************************/
e_MDRVStatus MDRV_ID_Check(uint8_t mdrv_num)
{
	e_MDRVStatus test_result = E_MDRV_OK;
	uint8_t mdrv_config_data = 0U;
	uint8_t reg_rd_data = 0U;
	if(MAX_NUM_OF_MOTOR_DRIVERS > mdrv_num)
	{
		MDRV_SPI_Read(mdrv_num,MRACK,&mdrv_config_data,&reg_rd_data); /*Read test register value*/
		if(MDRV_TEST_VALUE != reg_rd_data)
		{
			test_result = E_MDRV_FL;
		}
	}
	else
	{
		test_result = E_MDRV_FL;
	}
	return test_result;
}

/*************************************************************************************
** mdrv_set_pwm_value - This function activates PWM signal to Motor Driver
**
** Params : uint8_t mdrv_num -  Number of the Motor Driver (0-5)
**          uint8_t pwm_percent_val - Value that represents required motor's speed
**
** Returns: None.
** Notes:
*************************************************************************************/
static void mdrv_set_pwm_value(uint8_t mdrv_num, uint8_t pwm_percent_val)
{
	/*Check mdrv_num validity*/
	if(MAX_NUM_OF_MOTOR_DRIVERS > mdrv_num)
	{
		/*Check PWM value range. In case it greater than maximal value -> set it to maximal value*/
		if(PWM_MAX_VAL < pwm_percent_val)
		{
			pwm_percent_val = PWM_MAX_VAL;
		}
		/*Set PWM signal pulse parameter value*/
		switch(MdrvPwmCh[mdrv_num].timChannel)
		{
		case TIM_CHANNEL_1:
			MdrvPwmCh[mdrv_num].htimMdrv->Instance->CCR1 = (uint32_t)pwm_percent_val;
			break;
		case TIM_CHANNEL_2:
			MdrvPwmCh[mdrv_num].htimMdrv->Instance->CCR2 = (uint32_t)pwm_percent_val;
			break;
		case TIM_CHANNEL_3:
			MdrvPwmCh[mdrv_num].htimMdrv->Instance->CCR3 = (uint32_t)pwm_percent_val;
			break;
		case TIM_CHANNEL_4:
			MdrvPwmCh[mdrv_num].htimMdrv->Instance->CCR4 = (uint32_t)pwm_percent_val;
			break;
		default:
			break;
		}
	}
}

/*************************************************************************************
** MDRV_SPI_Write - This function writes Motor Driver's register value by implementation of
** 				   Bit-Bang SPI communication.
**
** Params : uint8_t mdrv_num - Motor Driver's number (0-5)
**          uint8_t mdrv_reg_addr - Motor Driver's register address.
**          uint8_t data_byte_to_write - Register's data value.
**
** Returns: None.
** Notes:
*************************************************************************************/
void MDRV_SPI_Write(uint8_t mdrv_num,uint8_t mdrv_reg_addr, uint8_t data_byte_to_write)
{
	if(MAX_NUM_OF_MOTOR_DRIVERS > mdrv_num)
	{
		uint16_t spi_wr_data = 0U;
		uint16_t spi_bit_mask = 0x8000U;

		mdrv_reg_addr = mdrv_reg_addr | 0x80U;  /*add '1' msb for write to MDRV register*/
		spi_wr_data = spi_wr_data | mdrv_reg_addr;
		spi_wr_data = spi_wr_data << 8U;
		spi_wr_data |=(uint16_t)data_byte_to_write;

		/*Set CLK and CS pins into initial state*/
		mdrv_set_spi_clk(E_GPIO_PIN_RESET);
		mdrv_set_cs_pin(MDRV_ALL_PINS,E_GPIO_PIN_SET);/*Disable SPI Bus of all Motor Drivers*/
		mdrv_set_cs_pin(mdrv_num,E_GPIO_PIN_RESET);/*Enable SPI bus of selected Motor Driver (Active Low)*/
		HAL_Delay(1U);

		/*Perform write process when MSB bit sent first*/
		for(uint8_t i = 0U; i < 16U; i++)
		{
			if(0U != (spi_wr_data & spi_bit_mask))
			{
				HAL_GPIO_WritePin(MDRV_SPI_PORT,MDRV_SPI_MOSI_PIN,E_GPIO_PIN_SET);
			}
			else
			{
				HAL_GPIO_WritePin(MDRV_SPI_PORT,MDRV_SPI_MOSI_PIN,E_GPIO_PIN_RESET);
			}
			mdrv_spi_delay();
			mdrv_set_spi_clk(E_GPIO_PIN_SET);
			mdrv_spi_delay();
			mdrv_set_spi_clk(E_GPIO_PIN_RESET);
			mdrv_spi_delay();
			spi_bit_mask = spi_bit_mask >> 1U;
		}
		/*Set CLK and CS pins into default state*/
		mdrv_set_cs_pin(MDRV_ALL_PINS,E_GPIO_PIN_SET);/*Disable SPI Bus of all Motor Drivers*/
		mdrv_set_spi_clk(E_GPIO_PIN_RESET);
	}
}
/*************************************************************************************
** MDRV_SPI_Read - This function reads Motor Driver's register by implementation of
** 				   Bit-Bang SPI communication. In addition, it returns
** 				   Motor Driver's current status.
**
** Params : uint8_t mdrv_num - Motor Driver's number (0-5)
**          uint8_t mdrv_reg_addr - Motor Driver's register address.
**          uint8_t *config_data - Pointer to status byte value
**          uint8_t *reg_data - Pointer to register's data value.
**
** Returns: None.
** Notes:
*************************************************************************************/
 void MDRV_SPI_Read(uint8_t mdrv_num,uint8_t mdrv_reg_addr, uint8_t *config_data, uint8_t *reg_data)
{
	uint16_t spi_wr_data  = 0x0000U;
	uint16_t spi_rd_data  = 0x0000U;
	uint16_t spi_bit_mask = 0x8000U;

	mdrv_reg_addr = mdrv_reg_addr & 0x7FU;  /*add '0' msb for read from MDRV register*/
	spi_wr_data = spi_wr_data | mdrv_reg_addr;
	spi_wr_data = spi_wr_data << 8U;

	/*Set CLK and CS pins into initial state*/
	mdrv_set_spi_clk(E_GPIO_PIN_RESET);
	mdrv_set_cs_pin(MDRV_ALL_PINS,E_GPIO_PIN_SET);/*Disable SPI Bus of all Motor Drivers*/
	mdrv_set_cs_pin(mdrv_num,E_GPIO_PIN_RESET);/*Enable SPI bus of selected Motor Driver (Active Low)*/
	HAL_Delay(1U);

	/*Perform write-read process when MSB bit sent first*/
	for(uint8_t i = 0U; i < 16U; i++)
	{
		if(0U != (spi_wr_data & spi_bit_mask))
		{
			HAL_GPIO_WritePin(MDRV_SPI_PORT,MDRV_SPI_MOSI_PIN,E_GPIO_PIN_SET);
		}
		else
		{
			HAL_GPIO_WritePin(MDRV_SPI_PORT,MDRV_SPI_MOSI_PIN,E_GPIO_PIN_RESET);
		}
		mdrv_spi_delay();
		mdrv_set_spi_clk(E_GPIO_PIN_SET);
		mdrv_spi_delay();
		if(E_GPIO_PIN_SET == HAL_GPIO_ReadPin(MDRV_SPI_PORT,MDRV_SPI_MISO_PIN))
		{
			spi_rd_data = spi_rd_data | spi_bit_mask;
		}
		mdrv_set_spi_clk(E_GPIO_PIN_RESET);
		mdrv_spi_delay();
		spi_bit_mask = spi_bit_mask >> 1U;
	}

	/*Set CLK and CS pins into default state*/
	mdrv_set_cs_pin(MDRV_ALL_PINS,E_GPIO_PIN_SET);/*Disable SPI Bus of all Motor Drivers*/
	mdrv_set_spi_clk(E_GPIO_PIN_RESET);

	*config_data = (uint8_t)((spi_rd_data & 0xFF00U) >> 8U);/*set highest 8 bits as motor driver's status value*/
	*reg_data    = (uint8_t)(spi_rd_data & 0x00FFU);/*set lowest 8 bits as register's data value*/
}

 /*************************************************************************************
 ** mdrv_set_spi_clk - This function controls CLK input of the Motor Driver's SPI PORT
 **
 ** Params : GPIO_PinState pin_state - Required pin's state ( E_GPIO_PIN_RESET/E_GPIO_PIN_SET )
 **
 ** Returns: None.
 ** Notes:
 *************************************************************************************/
 static void mdrv_set_spi_clk(GPIO_PinState pin_state)
 {
	 HAL_GPIO_WritePin(MDRV_SPI_PORT,MDRV_SPI_CLK_PIN,pin_state);
 }
 /*************************************************************************************
 ** mdrv_spi_delay - This function implements Motor Driver SPI delay
 **
 ** Params : None.
 **
 ** Returns: None.
 ** Notes:
 *************************************************************************************/
 static void mdrv_spi_delay(void)
 {
	 /*Approx 5.5 uSec delay*/
	 for(uint8_t i = 0U; i < 0x40U; i++)
	 {
		 __NOP();
	 }
 }
/*************************************************************************************
** mdrv_set_cs_pin - This function controls CS input of the Motor Drivers
**
** Params : uint8_t mdrv_num -  Number of the Motor Driver (0-5) or MDRV_ALL_PINS
**          GPIO_PinState pin_state - Required pin's state ( E_GPIO_PIN_RESET/E_GPIO_PIN_SET )
**
** Returns: None.
** Notes:
*************************************************************************************/
static void mdrv_set_cs_pin(uint8_t mdrv_num, GPIO_PinState pin_state)
{
	if((MAX_NUM_OF_MOTOR_DRIVERS > mdrv_num) && (E_GPIO_PIN_RESET == pin_state))
	{
		/*Enable SPI bus of selected Motor Driver (Active Low)*/
		HAL_GPIO_WritePin(MdrvPins[mdrv_num].MDRV_CS_Pin.Pin_port,MdrvPins[mdrv_num].MDRV_CS_Pin.Pin_num,E_GPIO_PIN_RESET);
	}
	else if(MDRV_ALL_PINS == mdrv_num)
	{
		/*Disable SPI bus of all Motor Drivers*/
		for(uint8_t i = 0U; i < MAX_NUM_OF_MOTOR_DRIVERS; i++)
		{
			HAL_GPIO_WritePin(MdrvPins[i].MDRV_CS_Pin.Pin_port,MdrvPins[i].MDRV_CS_Pin.Pin_num,E_GPIO_PIN_SET);
		}
	}
	else
	{
		/*Do nothing*/
	}
}
/*************************************************************************************
** MDRV_Wake_Ctrl - This function controls WAKE input of the Motor Driver
**
** Params : uint8_t mdrv_num -  Number of the Motor Driver (0-5) or MDRV_ALL_PINS
**          GPIO_PinState pin_state - Required pin's state ( E_GPIO_PIN_RESET/E_GPIO_PIN_SET )
**
** Returns: None.
** Notes:
*************************************************************************************/
void MDRV_Wake_Ctrl(uint8_t mdrv_num, GPIO_PinState pin_state)
{
	if(MAX_NUM_OF_MOTOR_DRIVERS > mdrv_num)
	{
		HAL_GPIO_WritePin(MdrvPins[mdrv_num].MDRV_WAKE_Pin.Pin_port,MdrvPins[mdrv_num].MDRV_WAKE_Pin.Pin_num,pin_state);
	}
	else if(MDRV_ALL_PINS == mdrv_num)
	{
		for(uint8_t i = 0U; i < MAX_NUM_OF_MOTOR_DRIVERS; i++)
		{
			HAL_GPIO_WritePin(MdrvPins[i].MDRV_WAKE_Pin.Pin_port,MdrvPins[i].MDRV_WAKE_Pin.Pin_num,pin_state);
		}
	}
	else
	{
		/*Do nothing*/
	}
}
/*************************************************************************************
** MDRV_Enable_Ctrl - This function controls EN input of the Motor Driver
**
** Params : uint8_t mdrv_num -  Number of the Motor Driver (0-5) or MDRV_ALL_PINS
**          GPIO_PinState pin_state - Required pin's state ( E_GPIO_PIN_RESET/E_GPIO_PIN_SET )
**
** Returns: None.
** Notes:
*************************************************************************************/
void MDRV_Enable_Ctrl(uint8_t mdrv_num, GPIO_PinState pin_state)
{
	if(MAX_NUM_OF_MOTOR_DRIVERS > mdrv_num)
	{
		HAL_GPIO_WritePin(MdrvPins[mdrv_num].MDRV_EN_Pin.Pin_port,MdrvPins[mdrv_num].MDRV_EN_Pin.Pin_num,pin_state);
	}
	else if(MDRV_ALL_PINS == mdrv_num)
	{
		/*Turn OFF all motors*/
		for(uint8_t i = 0U; i < MAX_NUM_OF_MOTOR_DRIVERS; i++)
		{
			HAL_GPIO_WritePin(MdrvPins[i].MDRV_EN_Pin.Pin_port,MdrvPins[i].MDRV_EN_Pin.Pin_num,E_GPIO_PIN_RESET);
			/*set PWM input to minimal value - 0*/
			mdrv_set_pwm_value(i, PWM_MIN_VAL);
		}
	}
	else
	{
		/*Do nothing*/
	}
}
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
