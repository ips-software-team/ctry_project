/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : current_sensor.c
** Author    : Leonid Savchenko
** Revision  : 1.6
** Updated   : 11-07-2021
**
** Description: This file provides procedures for current measurement using on-board
**              current sensor and internal ADC.
*/


/* Revision Log:
**
** Rev 1.0  : 05-02-2020 -- Original version created.
** Rev 1.1  : 24-10-2020 -- Updated Test_Current_Consumption()
** Rev 1.2  : 20-11-2020 -- Static analysis update
** Rev 1.3  : 22-10-2020 -- Updated due to verification remarks
** Rev 1.4  : 01-02-2021 -- Updated Measure_Current_Consumption()
** Rev 1.5  : 22-06-2021 -- Updated during integration
** Rev 1.6  : 11-07-2021 -- Added doxygen comments
**************************************************************************************
*/

/* -----Includes ---------------------------------------------------------------------*/
#include "current_sensor.h"
#include "gipsy_hal_adc.h"
#include "mcu_config_ctry.h"
/* -----Definitions ----------------------------------------------------------------*/
#define CURR_SENS_NUM_OF_SAMPLES       200U


#define CURR_SENS_RESOLUTION_AMP       0.060   /*60mV/A*/
#define CURR_SENS_ZERO_OFFSET          0.498
#define VOLTAGE_AMP_FACTOR 			   1.06

#define CURR_SENS_ERROR_VAL            255U
#define CURR_MEAS_ERROR_VAL            0xFFFFU
/* -----Macros ---------------------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/
/*!Specifies current measurement parameters (Range: var type | Units: see var declaration)*/
/*!\sa Set function: Measure_Current_Consumption()*/
/*!\sa Get function: Test_Current_Consumption(), CAN_Message_Transmit()*/
st_CurrSensData g_MeasuredCurrent =
{
		.CurrentValue = 0U,
		.MeasurementStatus = E_CURR_MEAS_OK
};
/* -----Static Function prototypes -------------------------------------------------*/

/* -----Modules implementation -----------------------------------------------------*/
/*************************************************************************************
** Measure_Current_Consumption - This function measures CTRY's current consumption using
** 								 on-board current sensor and internal ADC channel.
**
** Params : None.
**
** Returns: None.
** Notes:   Measurement result and status are saved in global structure "g_MeasuredCurrent"
*************************************************************************************/
void Measure_Current_Consumption(void)
{
	uint32_t adc_converted_value = 0U;
	uint32_t sum_of_samples = 0U;
	uint8_t valid_samples_counter = 0U;
	float curr_value = 0.0;
	uint16_t sample_current = 0U;
	uint16_t sample_previous = 0U;


	/*Perform current measurement*/
	for(uint8_t sample_num = 0U; sample_num < CURR_SENS_NUM_OF_SAMPLES; sample_num++)
	{
		/*Start the conversion process*/
		if(HAL_OK == HAL_ADC_Perform_Conversion(ADC_CHANNEL_3, &adc_converted_value))
		{
			/*Get the converted value */
			sample_current = (uint16_t)(adc_converted_value & 0x00000FFFU);/*make sure to get 12 bit of data*/

			/*Average samples after first one*/
			if(valid_samples_counter > 0U)
			{
				sample_current = (sample_current + sample_previous)/2U; /*calculate mean value of 2 samples*/
				sum_of_samples += sample_current;/*Accumulate sum of all samples*/
			}
			sample_previous = sample_current;/*save sample value for next loop iteration*/
			valid_samples_counter++;
			/*Interval between samples approx 1uec*/
			for(uint8_t i = 0U; i < 12U; i++)
			{
				__NOP();
			}
		}
	}
	/*First sample not accumulated into SUM -> ignore it by decreasing the counter*/
	if(valid_samples_counter > 1U)
	{
		valid_samples_counter = valid_samples_counter - 1U;
	}

	/*Compute measured current value in case that valid samples available*/
	if(valid_samples_counter > 0U)
	{
		/*Calculate average value from measured current samples and convert it to measured voltage*/
		curr_value = (float)(sum_of_samples/valid_samples_counter) * (float)ACD_RESOLUTION_VOLT;
		/*Get Current Sensor's output voltage value by removing External Amplification factor*/
		curr_value = curr_value / (float)VOLTAGE_AMP_FACTOR;
		/*Remove Current Sensor's Zero voltage (0.5V) from the measurement*/
		curr_value = curr_value - (float)CURR_SENS_ZERO_OFFSET;

		if(curr_value < 0U)
		{
			curr_value = 0U;
		}
		/*Convert Current Sensor's output voltage to measured current value*/
		curr_value = curr_value / (float)CURR_SENS_RESOLUTION_AMP;


		/*Save measured current value in resolution of 0.1A as single byte (0-255  -> 0- 25.5 Amperes)*/
		g_MeasuredCurrent.CurrentValue = (uint8_t)(curr_value * 10U);
		g_MeasuredCurrent.MeasurementStatus = E_CURR_MEAS_OK;
	}
	else
	{
		/*Set error measurement in case that no valid samples produced*/
		g_MeasuredCurrent.CurrentValue = CURR_SENS_ERROR_VAL;
		g_MeasuredCurrent.MeasurementStatus = E_CURR_MEAS_FL;
	}
}
/*************************************************************************************
** Test_Current_Consumption - This function tests CTRY's current consumption with supplied
** 							  limits.
**
** Params : uint8_t Curr_Limit_High - Upper current limit
**          uint8_t Curr_Limit_Low - Lower current limit
**
** Returns: e_CurrentTestStatus - Current test result (E_CURR_TEST_FL | E_CURR_TEST_OK)
** Notes:   Tested current value taken from global structure "g_MeasuredCurrent" after measurement
*************************************************************************************/
e_CurrentTestStatus Test_Current_Consumption(uint8_t Curr_Limit_High, uint8_t Curr_Limit_Low)
{
	e_CurrentTestStatus test_result = E_CURR_TEST_OK;
	Measure_Current_Consumption();
	if((g_MeasuredCurrent.CurrentValue < Curr_Limit_Low) || (g_MeasuredCurrent.CurrentValue > Curr_Limit_High) ||(E_CURR_MEAS_FL == g_MeasuredCurrent.MeasurementStatus))
	{
		test_result = E_CURR_TEST_FL;
	}

	return test_result;
}
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
