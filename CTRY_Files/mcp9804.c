/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : mcp9804.c
** Author    : Leonid Savchenko
** Revision  : 1.7
** Updated   : 29-06-2021
**
** Description: This file contains procedures for control and configuration of
** 				the Temperature sensor mcp9804
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1  : 25-08-2020 -- Updated I2C bus handle variables names
** Rev 1.2  : 01-10-2020 -- Updated Temperaure_Sensors_Read()
** Rev 1.3  : 09-10-2020 -- Updated temperature_sensor_check_id() - changed to Temperature_Sensor_Check_ID()
** Rev 1.4  : 12-11-2020 -- Updated temperature_sensors_write_all()
** Rev 1.5  : 20-11-2020 -- Static analysis update,added defines
** Rev 1.6  : 25-06-2021 -- Static analysis update
** Rev 1.7  : 29-06-2021 -- Updated due to verification remarks
**************************************************************************************
*/


/* -----Includes ---------------------------------------------------------------------*/
#include "mcp9804.h"

#include "mcu_config_ctry.h"
#include "temperature_control.h"


/* -----Definitions ----------------------------------------------------------------*/
#define TEMPERATURE_ABS_MIN		 (int8_t)(-65)
#define TEMPERATURE_ABS_MAX		 (int8_t)(110)

#define TEMP_SENS_TIMEOUT_MILIS   10U /*I2C bus read timeout value*/

#define TEMP_SENS_WR_BYTES    2U /*Each I2C write cycle sends 2 bytes*/
#define TEMP_SENS_RD_BYTES    2U /*Each I2C read cycle receives 2 bytes*/

#define TEMP_SENS_DEVICE_ID   0X0054U
#define TEMP_SENS_UPPER_TA_BYTE    0U
#define TEMP_SENS_LOWER_TA_BYTE    1U
/*Temperature sensor resolution
 * 0.5degC    = 0x00
 * 0.25degC   = 0x01
 * 0.125degC  = 0x02
 * 0.0625degC = 0x03
 */
#define TEMP_SENS_RESOLUTION  0x00U /*CTRY Temp sensors resolution is 0.5degC*/
#define TEMP_SENS_MODE        0x00U /*CTRY Temp sensors operate in Continues_Mode*/
/* -----Macros ---------------------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/
const BoardTempSensAddr_st BrdTempSensAddr[E_CTRY_NUM_OF_BOARDS] =
{
		{
			&g_HandleI2C_2_TempMcu,
			{0x30U,0x32U,0x34U,0x35U},
		},
		{
			&g_HandleI2C_1_TempDrv,
			{0x38U,0x3AU,0x3CU,0x3EU},
		},
};


/* -----Static Function prototypes -------------------------------------------------*/
static HAL_StatusTypeDef mcp9804_write(I2C_HandleTypeDef *hi2c_temp_sensor,uint8_t sensor_addr, uint8_t *sensor_data);
static HAL_StatusTypeDef mcp9804_read(I2C_HandleTypeDef *hi2c_temp_sensor,uint8_t sensor_addr, uint8_t *sensor_data);
static uint8_t temperature_sensors_write_all( uint8_t reg_addr,uint16_t reg_data);

/* -----Modules implementation -----------------------------------------------------*/


/*************************************************************************************
** Temperature_Sensors_Config - This function configures all 8 CTRY's temperature sensors
**
** Params : None
**
** Returns: uint8_t - Status of WR status(each bit represents status of a sensor
**                    ('1' - write failed , '0' - write succeeded)
** Notes:
*************************************************************************************/
uint8_t Temperature_Sensors_Config(void)
{
	uint8_t wr_status = 0X00U;
	wr_status = temperature_sensors_write_all((uint8_t)REG_RESOLUTION,(uint16_t)TEMP_SENS_RESOLUTION);
	return wr_status;
}

/*************************************************************************************
** Temperaure_Sensors_Read - This function reads 8 temperature sensors' measuted temperature.
** Params : BoardTempSens_t *board_temp_sens - Pointer to Temperature Sensors' data structure
**
**
**
** Returns: uint8_t - Status of sensors ID test and measurement read (4 lsb bits for CPU board, 4 msb bits for DRV board)
**
** Notes:
*************************************************************************************/
uint8_t Temperaure_Sensors_Read(BoardTempSens_t *board_temp_sens)
{
	uint8_t sensors_status = 0X00U;
	uint8_t sensors_mask = 0X01U;
	HAL_StatusTypeDef i2c_bus_status = HAL_ERROR;
	uint8_t sensor_data[TEMP_SENS_WR_BYTES] = {0U};
	int8_t temperature_value = 0U;

	for(uint8_t board_num = 0U; board_num < (uint8_t)E_CTRY_NUM_OF_BOARDS ; board_num++)
	{
		for(uint8_t sens_num = 0U; sens_num < MAX_SENSORS_ON_BOARD; sens_num++)
		{
			/*Read sensor's temperature in case ID check passed*/
			if(E_TEMP_SENS_ID_OK == Temperature_Sensor_Check_ID(board_num, sens_num, &board_temp_sens[board_num].SensorID[sens_num]))
			{
				sensor_data[0U] = (uint8_t)REG_TA;  /*Ambient Temperature Register Address*/
				sensor_data[1U] = 0X00U;

				i2c_bus_status = mcp9804_read(BrdTempSensAddr[board_num].hi2cSensBus,BrdTempSensAddr[board_num].TempSensAddr[sens_num],(uint8_t*)sensor_data);
				if(HAL_OK == i2c_bus_status)
				{
					sensor_data[TEMP_SENS_LOWER_TA_BYTE] &= 0xF0U;/*Remove floating point from temperature raw value */
					sensor_data[TEMP_SENS_UPPER_TA_BYTE] &= 0x1FU;/*Remove 3 msb unused bits */
					/*Check temperature value sign*/
					if (0x10 == (sensor_data[TEMP_SENS_UPPER_TA_BYTE] & 0x10U))
					{
						/*convert to negative value*/
						sensor_data[TEMP_SENS_UPPER_TA_BYTE] &= 0x0FU;/*Remove negative sign bit */
						temperature_value = 256U -  ((sensor_data[TEMP_SENS_UPPER_TA_BYTE] * 16U) + (sensor_data[TEMP_SENS_LOWER_TA_BYTE] / 16U));
						board_temp_sens[board_num].Temperatue[sens_num] = (uint8_t)(temperature_value | 0x80U);/*Mark temperature value as negative*/
						temperature_value = temperature_value * -1;/*Set temperature value as signed negative for further comparison*/
					}
					else
					{
						temperature_value = (sensor_data[TEMP_SENS_UPPER_TA_BYTE] * 16U) + (sensor_data[TEMP_SENS_LOWER_TA_BYTE] / 16U);
						board_temp_sens[board_num].Temperatue[sens_num] = (uint8_t)temperature_value;
					}
					/*Check if the temperature is in normal range*/
					if((TEMPERATURE_ABS_MIN > temperature_value) || (TEMPERATURE_ABS_MAX < temperature_value))
					{
						sensors_status = sensors_status | sensors_mask;
						board_temp_sens[board_num].Temperatue[sens_num] = TEMP_ERR_VALUE;
					}
				}
				else /*read error occurred*/
				{
					sensors_status = sensors_status | sensors_mask;
					board_temp_sens[board_num].Temperatue[sens_num] = TEMP_ERR_VALUE;
				}
			}
			else /*Sensor ID read failed -> mark failed sensor with TEMP_ERR_VALUE*/
			{
				sensors_status = sensors_status | sensors_mask;
				board_temp_sens[board_num].Temperatue[sens_num] = TEMP_ERR_VALUE;
			}
			sensors_mask = sensors_mask << 1U;/*Update mask for next sensor*/
		}
	}
	return sensors_status;
}
/*************************************************************************************
** Temperature_Sensor_Check_ID - This function checks 8 sensor's ID
** Params : uint8_t board_type - Type of the board (CPU/DRV)
** 			uint8_t sensor_num - Sensor's number (0-3)
** 			uint16_t *sens_id  - Pointer to Sensor's ID value
**
**
** Returns: e_Temp_Sens_ID_Status - Status of ID check ALL ( TEMP_SENS_ID_OK / TEMP_SENS_ID_FAIL)
** Notes:
*************************************************************************************/
e_Temp_Sens_ID_Status Temperature_Sensor_Check_ID(uint8_t board_type, uint8_t sensor_num, uint16_t *sens_id)
{
	e_Temp_Sens_ID_Status sensor_check_id_status = E_TEMP_SENS_ID_OK;
	HAL_StatusTypeDef i2c_bus_status = HAL_ERROR;
	uint8_t sensor_data[TEMP_SENS_WR_BYTES] = {0U};
	uint16_t sensor_id_value = 0U;

	/*Check input parameters*/
	if((E_CTRY_NUM_OF_BOARDS > (e_CTRY_Boards)board_type) && ((uint8_t)MAX_SENSORS_ON_BOARD > sensor_num) && (NULL != sens_id))
	{
		sensor_data[0U] = (uint8_t)REG_MANUFACTURER_ID;  /*Device ID Register Address*/
		i2c_bus_status = mcp9804_read(BrdTempSensAddr[board_type].hi2cSensBus,BrdTempSensAddr[board_type].TempSensAddr[sensor_num],sensor_data);
		/*Combine sensor's ID */
		sensor_id_value = (uint16_t)((sensor_data[0U]*256U) + sensor_data[1U]);
		*sens_id = sensor_id_value;

		if((TEMP_SENS_DEVICE_ID != sensor_id_value) || (HAL_OK != i2c_bus_status)) /*failed to read device id*/
		{
			sensor_check_id_status = E_TEMP_SENS_ID_FAIL;
		}
		else
		{
			sensor_check_id_status = E_TEMP_SENS_ID_OK;
		}
	}
	else
	{
		sensor_check_id_status = E_TEMP_SENS_ID_FAIL;
	}
	return sensor_check_id_status;
}
/*************************************************************************************
** temperature_sensors_write_all - This function writes to all 8 temperature sensors
** Params : uint8_t reg_addr - Address of internal register
** 			uint16_t reg_data - Data byte that will be written to mc8904
**
**
** Returns: uint8_t - Status of write ALL process ( 0x00 - write succeeded)
** Notes: Return other than 0x00 defines that one or more sensors write failed
*************************************************************************************/
static uint8_t temperature_sensors_write_all(uint8_t reg_addr,uint16_t reg_data)
{
	uint8_t sensors_status = 0X00U;
	uint8_t sensors_mask   = 0X01U;
	HAL_StatusTypeDef i2c_bus_status = HAL_ERROR;
	uint8_t sensor_data[TEMP_SENS_WR_BYTES] = {0U};
	sensor_data[0U] = reg_addr;
	sensor_data[1U] = (uint8_t)reg_data;


	for(uint8_t board_num = 0U; board_num < (uint8_t)E_CTRY_NUM_OF_BOARDS ; board_num++)
	{
		for(uint8_t sens_num = 0U; sens_num < MAX_SENSORS_ON_BOARD; sens_num++)
		{
			i2c_bus_status = mcp9804_write(BrdTempSensAddr[board_num].hi2cSensBus,BrdTempSensAddr[board_num].TempSensAddr[sens_num],sensor_data);
			if(HAL_OK != i2c_bus_status)
			{
				sensors_status = sensors_status | sensors_mask;
			}
			sensors_mask = sensors_mask << 1U;
		}
	}
	return sensors_status;
}
/*************************************************************************************
** mcp9804_write - This function writes internal register's value of mc8904
** Params : I2C_HandleTypeDef *hi2c_temp_sensor - Pointer to instance of I2C bus
** 			uint8_t sensor_addr - Address of internal register
** 			uint8_t *sensor_data - Pointer to data that will be written to mc8904
**
** Returns: HAL_StatusTypeDef - Status of write process
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef mcp9804_write(I2C_HandleTypeDef *hi2c_temp_sensor,uint8_t sensor_addr, uint8_t *sensor_data)
{
	HAL_StatusTypeDef i2c_status = HAL_ERROR;
	i2c_status = HAL_I2C_Master_Transmit(hi2c_temp_sensor, (uint16_t)sensor_addr, sensor_data, TEMP_SENS_WR_BYTES, TEMP_SENS_TIMEOUT_MILIS);
	return i2c_status;
}

/*************************************************************************************
** mcp9804_read - This function reads internal register's value of mc8904
** Params : I2C_HandleTypeDef *hi2c_temp_sensor - Pointer to instance of I2C bus
** 			uint8_t sensor_addr - Address of internal register
** 			uint8_t *sensor_data - Pointer to data read from mc8904
**
** Returns: HAL_StatusTypeDef - Status of read process
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef mcp9804_read(I2C_HandleTypeDef *hi2c_temp_sensor,uint8_t sensor_addr, uint8_t *sensor_data)
{
	HAL_StatusTypeDef i2c_status = HAL_ERROR;
	/*Transfer register's address to set internal pointer to point required register*/
	i2c_status = HAL_I2C_Master_Transmit(hi2c_temp_sensor, (uint16_t)sensor_addr,sensor_data, 1U, TEMP_SENS_TIMEOUT_MILIS);
	if(HAL_OK == i2c_status)
	{
		sensor_addr = sensor_addr | 0x01U;
		i2c_status = HAL_I2C_Master_Receive(hi2c_temp_sensor, (uint16_t)sensor_addr, sensor_data, TEMP_SENS_RD_BYTES, TEMP_SENS_TIMEOUT_MILIS);
	}
	return i2c_status;
}
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
