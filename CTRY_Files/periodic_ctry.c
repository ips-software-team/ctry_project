/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : periodic_ctry.c
** Author    : Leonid Savchenko
** Revision  : 1.4
** Updated   : 21-05-2023
**
** Description: This file contains periodic scheduling procedure.
**
** Notes:
*/


/* Revision Log:
**
** Rev 1.0  : 02-10-2019 -- Original version created.
** Rev 1.1  : 23-06-2021 -- Updated during integration.
** Rev 1.2  : 11-07-2021 -- Updated Periodic function
** Rev 1.3  : 25-07-2021 -- Added Check interval between OPER mode entry and reception of first activation command
** Rev 1.4  : 21-05-2023 -- Changed STBY_CURRENT_LIM_MAX to 3A
**                          Removed actuators driver communication continues test
**                          Updated STBY current consumption failure latch process:
**                          	A. Over Current is defined by 10 continues failed tests in 1 second intervals.
**                          Updated STBY voltage test failure latch process:
**                          	A. Fail voltage is defined by 10 continues failed tests in 1 second intervals.
**                          Updated ACTUATOR_ON_IBIT_PERIOD_MILIS to 4400ms from 2000ms
**                          Removed defines for IBIT limits, added external global IBIT limits
**                          Added actuators de-activation CAN_BUS execution
**************************************************************************************
*/

/* -----Includes ---------------------------------------------------------------------*/
#include "periodic_ctry.h"
#include "gipsy_hal_rcc.h"
#include "actuator_ctrl.h"
#include "current_sensor.h"
#include "fault_mngmnt_ctry.h"
#include "gpio_control_ctry.h"
#include "id_conf_ctry.h"
#include "module_init_ctry.h"
#include "motor_driver.h"
#include "system_params_ctry.h"
#include "temperature_control.h"
#include "vibration_meas.h"
#include "voltage_meas_ctry.h"
#include "wdog.h"
/* -----Definitions ----------------------------------------------------------------*/
#define ACT_MEAS_INIT_DELAY_PERIOD_MILIS (uint32_t)1100U /*msec*/

#define STBY_CURRENT_LIM_MIN      (uint8_t)0U    /*0A*/
#define STBY_CURRENT_LIM_MAX      (uint8_t)30U   /*3A*/

#define MAN_CURRENT_LIM_MIN       (uint8_t)0U    /*0A*/
#define MAN_CURRENT_LIM_MAX       (uint8_t)250U  /*25A*/
#define MAN_VIBRATION_LIM_MIN     (uint8_t)0U    /*0G*/
#define MAN_VIBRATION_LIM_MAX     (uint8_t)100U  /*100G*/

#define SW_LED_TOGGLE_PERIOD_MILIS        (uint32_t)500U  /*msec*/
#define TEMP_MNGMT_PERIOD_MILIS           (uint32_t)1500U /*msec*/
#define CBIT_PERIOD_MILIS                 (uint32_t)1000U /*msec*/
#define TBL_MONIT_PERIOD_MILIS            (uint32_t)1000U /*msec*/
#define ACTUATOR_ON_PERIOD_MILIS          (uint32_t)4400U /*msec*/
#define ACTUATOR_ON_IBIT_PERIOD_MILIS     (uint32_t)4400U /*msec*/

#define CURR_MSRMT_PERIOD_MILIS           (uint32_t)1000U /*msec*/
#define VIBRATION_MSRMT_PERIOD_MILIS      (uint32_t)1000U /*msec*/
#define MOT_ENABLE_PERIOD_MILIS           (uint32_t)100U  /*msec*/
#define MONITOR_ACCEL_CURR_PERIOD_MILIS   (uint32_t)1000U /*msec*/
#define MDRV_COMM_TEST_PERIOD_MILIS       (uint32_t)1000U /*msec*/

#define OPER_MODE_FIRST_ACTIVATION_COMM_TIME_MAX_INTERVAL_MILIS (uint32_t)10000 /*msec*/

#define STBY_CURRENT_MAX_FAILURES_NUM              10U
#define STBY_CURRENT_FAILURES_NUM_FOR_SWITCH_OFF   5U
#define STBY_VOLTAGE_MAX_FAILURES_NUM              10U
/* -----Macros ---------------------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/
/*!Specifies status of actuators de-activation (Range: 0 - 3 | Units: None)*/
/*!\sa Set function: Periodic_Tasks_Scheduler(), HAL_CAN_RxFifo0MsgPendingCallback()*/
/*!\sa Get function: Periodic_Tasks_Scheduler()*/
e_ActuatorActivationState g_Act_Off_Req_Status = E_ACT_NO_CHANGE;

/* -----External variables ---------------------------------------------------------*/
extern uint32_t g_Act_Deactivation_Time;
extern uint8_t  g_DIMC_Oper_Mode_Active;
extern uint32_t g_OPER_Start_Time;

extern uint8_t g_ActLimIbit_Vib_Max;
extern uint8_t g_ActLimIbit_Vib_Min;
extern uint8_t g_ActLimIbit_Curr_Max;
extern uint8_t g_ActLimIbit_Curr_Min;
/* -----Static Function prototypes -------------------------------------------------*/
static void monitor_actuator_current_and_vibration(void);
/* -----Modules implementation -----------------------------------------------------*/


/*************************************************************************************
** Periodic_Tasks_Scheduler - The application main loop
**
** Params : None.
**
** Returns: None
** Notes:
*************************************************************************************/
void Periodic_Tasks_Scheduler(void)
{
	st_Task_Last_Execution_Time stLastExecTime = {0U};
	uint8_t stby_current_test_failures_counter = 0U;
	uint8_t voltage_input_test_failures_counter = 0U;
	uint8_t voltage_power_switch_test_failures_counter = 0U;

	Set_Motor_PWR_EN(E_GPIO_PIN_SET); /*enable power to high current motor driver stage*/
	Perform_Actuators_Deactivation();
	WDOG_Trigger();
	Init_Actuator_Pair_Parameters();


	while(TRUE)
	{
		WDOG_Trigger();

		/*-------------------------------------------------------------------------*/
		/*Check interval between OPER mode entry and reception of first activation command*/
		/*-------------------------------------------------------------------------*/
		if(0U != g_OPER_Start_Time)
		{
			if(OPER_MODE_FIRST_ACTIVATION_COMM_TIME_MAX_INTERVAL_MILIS < (HAL_GetTick() - g_OPER_Start_Time))
			{
				g_ModuleParams.Ctry_Failure_Desc |= CTRY_FLT_SANITY_MONIT;
			}
		}

		/*-------------------------------------------------------------------------*/
		/*Manage on-board LEDs*/
		/*-------------------------------------------------------------------------*/
		if(SW_LED_TOGGLE_PERIOD_MILIS <= (HAL_GetTick() - stLastExecTime.Sw_Led_Toggle))
		{
			Manage_OnBoard_Leds();
			stLastExecTime.Sw_Led_Toggle = HAL_GetTick();
		}

		/*-------------------------------------------------------------------------*/
		/*Perform temperature measurement and heaters management*/
		/*-------------------------------------------------------------------------*/
		if(TEMP_MNGMT_PERIOD_MILIS <= (HAL_GetTick() - stLastExecTime.TemperatureManagement))
		{
			CTRY_Temperature_Management();
			stLastExecTime.TemperatureManagement= HAL_GetTick();
		}

		/*-------------------------------------------------------------------------*/
		/*Perform CBIT when NOT in OPER system mode*/
		/*-------------------------------------------------------------------------*/
		if(CBIT_PERIOD_MILIS <= (HAL_GetTick() - stLastExecTime.Cbit))
		{
			if(FALSE == g_DIMC_Oper_Mode_Active)
			{
				/*Check 14.8V Input voltage source*/
				if(E_VOLT_TEST_FL == Test_OnBoard_Voltage(E_VOLT_14_8V_IN))
				{
					voltage_input_test_failures_counter++;/*Increment input power failure counter*/
				}
				else
				{
					voltage_input_test_failures_counter = 0U;/*Reset input power failure counter*/
				}
				if(STBY_VOLTAGE_MAX_FAILURES_NUM == voltage_input_test_failures_counter)
				{
					g_ModuleParams.Ctry_Failure_Desc |= CTRY_FLT_POWER_IN;/*Latch input power failure */
				}

				/*Check MDRV voltage when power switch is ON*/
				if(E_VOLT_TEST_FL == Test_OnBoard_Voltage(E_VOLT_14_8_MDRV_ON))
				{
					voltage_power_switch_test_failures_counter++;/*Increment motor driver power failure counter*/
				}
				else
				{
					voltage_power_switch_test_failures_counter = 0U;/*Reset motor driver power failure counter*/
				}
				if(STBY_VOLTAGE_MAX_FAILURES_NUM == voltage_power_switch_test_failures_counter)
				{
					g_ModuleParams.Ctry_Failure_Desc |= CTRY_FLT_POW_SWITCH;/*Latch motor driver power failure */
				}
				stLastExecTime.Cbit = HAL_GetTick();
			}
		}

		/*-------------------------------------------------------------------------*/
		/*Perform System Status Table monitoring*/
		/*-------------------------------------------------------------------------*/
		if(TBL_MONIT_PERIOD_MILIS <= (HAL_GetTick() - stLastExecTime.Status_Tbl_Monitoring))
		{
			/*Perform System status table monitoring*/
			if(E_MON_TBL_FL == Monitor_Status_Table())
			{
				g_ModuleParams.Ctry_Failure_Desc |= CTRY_FLT_TABLE_MONIT;
			}
			stLastExecTime.Status_Tbl_Monitoring = HAL_GetTick();
		}

		/*-------------------------------------------------------------------------*/
		/*Sample MOT_OPER Input discrete*/
		/*-------------------------------------------------------------------------*/
		if(MOT_ENABLE_PERIOD_MILIS <= (HAL_GetTick() - stLastExecTime.MotOperInput))
		{
			Monitor_MOTOR_OPER_Input_State();
			stLastExecTime.MotOperInput = HAL_GetTick();
		}

		/*-------------------------------------------------------------------------*/
		/*Check if actuators activation required and perform sanity check*/
		/*-------------------------------------------------------------------------*/
		if(E_STBY_MODE == Get_Module_Mode())
		{
			if(E_ACT_ACTIVATED == Actuators_Activation_Management())
			{
				stLastExecTime.ActuatorStartTime = HAL_GetTick();
				voltage_input_test_failures_counter = 0U;/*Reset input power failure counter*/
				voltage_power_switch_test_failures_counter = 0U;/*Reset motor driver power failure counter*/
				stby_current_test_failures_counter = 0U;/*Reset over-current failure counter*/
			}
		}

		/*-------------------------------------------------------------------------*/
		/*Check if actuators de-activation required*/
		/*-------------------------------------------------------------------------*/
		if(E_ACT_OFF_REQ == g_Act_Off_Req_Status)
		{
			Perform_Actuators_Deactivation();
			g_Act_Off_Req_Status = E_ACT_DEACTIVATED;
		}

		/*-------------------------------------------------------------------------*/
		/*Deactivate active actuator when activation period elapsed*/
		/*-------------------------------------------------------------------------*/
		if((E_ACT_MODE_OPER == g_ActuatorsPair.ActMode) || (E_ACT_MODE_MAN == g_ActuatorsPair.ActMode))
		{
			if((NO_ACTIVE_ACTUATOR != Get_Active_Actuator_Num()) && (ACTUATOR_ON_PERIOD_MILIS <= (HAL_GetTick() - stLastExecTime.ActuatorStartTime)))
			{
				Perform_Actuators_Deactivation();
				stLastExecTime.Cbit = HAL_GetTick();/*Add 1000msec delay before CBIT will start*/
			}
		}
		else if(E_ACT_MODE_IBIT == g_ActuatorsPair.ActMode)
		{
			if((NO_ACTIVE_ACTUATOR != Get_Active_Actuator_Num()) && (ACTUATOR_ON_IBIT_PERIOD_MILIS <= (HAL_GetTick() - stLastExecTime.ActuatorStartTime)))
			{
				Perform_Actuators_Deactivation();
				stLastExecTime.Cbit = HAL_GetTick();/*Add 1000msec delay before CBIT will start*/
			}
		}
		else
		{
			/*Undefined mode*/
			Perform_Actuators_Deactivation();
			Increment_SW_Error_Counter();
		}

		/*-------------------------------------------------------------------------*/
		/*Measure and test Current consumption when actuator is NOT active*/
		/*-------------------------------------------------------------------------*/
		if(CURR_MSRMT_PERIOD_MILIS <= (HAL_GetTick() - stLastExecTime.CurrentConsumption))
		{
			if((NO_ACTIVE_ACTUATOR == Get_Active_Actuator_Num()) && (CURR_MSRMT_PERIOD_MILIS <= (HAL_GetTick() - g_Act_Deactivation_Time)))
			{
				if(E_CURR_TEST_FL == Test_Current_Consumption(STBY_CURRENT_LIM_MAX,STBY_CURRENT_LIM_MIN))
				{
					stby_current_test_failures_counter++;/*Increment over-current failure counter*/
				}
				else
				{
					stby_current_test_failures_counter = 0U;/*Reset over-current failure counter*/
				}
				/*Disable power to motor drivers in case of over-current detection after 5 failures*/
				if(STBY_CURRENT_FAILURES_NUM_FOR_SWITCH_OFF <= stby_current_test_failures_counter)
				{
					Set_Motor_PWR_EN(E_GPIO_PIN_RESET); /*Turn OFF power switch*/
					g_ModuleParams.Ctry_Failure_Desc |= CTRY_FLT_POW_SWITCH;/*Latch motor driver power failure */
				}
				/*Latch over-current failure after 10 continues failures*/
				if(STBY_CURRENT_MAX_FAILURES_NUM == stby_current_test_failures_counter)
				{
					g_ModuleParams.Ctry_Failure_Desc |= CTRY_FLT_CURR_CONSUMPTION;
				}
			}
			stLastExecTime.CurrentConsumption = HAL_GetTick();
		}

		/*-------------------------------------------------------------------------*/
		/* Monitor activated actruator's current consumption and vibration
		 * after pre-defined period passed(actuator is stable)*/
		/*-------------------------------------------------------------------------*/
		if(NO_ACTIVE_ACTUATOR != Get_Active_Actuator_Num())
		{
			if((MONITOR_ACCEL_CURR_PERIOD_MILIS <= (HAL_GetTick() - stLastExecTime.Mon_Accel_Curr)) && \
			   (ACT_MEAS_INIT_DELAY_PERIOD_MILIS <= (HAL_GetTick() - stLastExecTime.ActuatorStartTime)))
			{
				monitor_actuator_current_and_vibration();
				stLastExecTime.Mon_Accel_Curr = HAL_GetTick();
			}
		}
		/*-------------------------------------------------------------------------*/
		/*Perform fault management*/
		/*-------------------------------------------------------------------------*/
		CTRY_Fault_Management();
	}
}

/*************************************************************************************
** monitor_actuator_current_and_vibration - Monitors CTRY's current consumption
**                                          and vibration during actuator's activation
**
** Params : None.
**
** Returns: None
** Notes:
*************************************************************************************/
static void monitor_actuator_current_and_vibration(void)
{
	uint8_t curr_lim_max  = 0U;
	uint8_t curr_lim_min  = 0U;
	uint8_t accel_lim_max = 0U;
	uint8_t accel_lim_min = 0U;
	HAL_StatusTypeDef hal_status = HAL_OK;
	e_CurrentTestStatus curr_test_status = E_CURR_TEST_OK;
	e_AccelMonitorResult accel_test_status = E_ACCEl_MONITOR_OK;

	/*Define measurements' limits according to actuator's activation mode*/
	switch (g_ActuatorsPair.ActMode)
	{
	case E_ACT_MODE_MAN:
		curr_lim_max  = MAN_CURRENT_LIM_MAX;
		curr_lim_min  = MAN_CURRENT_LIM_MIN;
		accel_lim_max = MAN_VIBRATION_LIM_MAX;
		accel_lim_min = MAN_VIBRATION_LIM_MIN;
		break;
	case E_ACT_MODE_IBIT:
		curr_lim_max  = g_ActLimIbit_Curr_Max;
		curr_lim_min  = g_ActLimIbit_Curr_Min;
		accel_lim_max = g_ActLimIbit_Vib_Max;
		accel_lim_min = g_ActLimIbit_Vib_Min;
		break;
	case E_ACT_MODE_OPER:
		hal_status  = Get_Actuator_CurrLimits(Get_CTRY_ID(),Get_Active_Actuator_Num(),&curr_lim_max, &curr_lim_min);
		hal_status |= Get_Actuator_VibrLimits(Get_CTRY_ID(),Get_Active_Actuator_Num(),&accel_lim_max, &accel_lim_min);
		break;
	default:/*Undefined activation mode*/
		hal_status = HAL_ERROR;
		break;
	}

    if(HAL_OK == hal_status)
    {
    	/*Measure and test current consumption while actuator is active*/
    	if(E_CURR_TEST_FL == Test_Current_Consumption(curr_lim_max,curr_lim_min))
		{
    		curr_test_status = E_CURR_TEST_FL;
		}

    	/*Measure and test vibration while actuator is active*/
		if(E_ACCEL_MONITOR_FAIL == Perform_Acceleration_Monitoring(accel_lim_max,accel_lim_min))
		{
    		accel_test_status = E_ACCEL_MONITOR_FAIL;
		}

		if((accel_test_status == E_ACCEL_MONITOR_FAIL) || (curr_test_status == E_CURR_TEST_FL))
		{
			/*Mark active actuator as FL*/
			g_ModuleParams.Ctry_Failure_Desc |= (CTRY_FLT_MDRV_BASE << Get_Active_Actuator_Num());
			 hal_status |= Set_Actuator_Status_FL(Get_CTRY_ID(),(e_ActuatorsNum)Get_Active_Actuator_Num());
			/*Shut down all CTRY's actuators*/
			Perform_Actuators_Deactivation();
		}

		if(HAL_OK != hal_status)
		{
			Increment_SW_Error_Counter();
		}
	}
	else/*Undefined actuator's mode*/
	{
		Increment_SW_Error_Counter();
		g_ModuleParams.Ctry_Failure_Desc |= CTRY_FLT_CURR_CONSUMPTION;
		g_ModuleParams.Ctry_Failure_Desc |= CTRY_FLT_ACCELEROMETER;
	}
}

/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
