/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : vibration_meas.c
** Author    : Leonid Savchenko
** Revision  : 1.3
** Updated   : 22-11-20
**
** Description: This file contains procedures for setup, initialization and
** 				acceleration value read from ADXL375 accelerometer IC.
*/

/* Revision Log:
**
** Rev 1.0  : 25-08-20 -- Original version created.
** Rev 1.1  : 24-10-20 -- Updated : Read_Max_Acceleration_Value(),added global variable g_Acceleration_Value
** Rev 1.2  : 20-11-20 -- Static analysis update
** Rev 1.3  : 22-11-20 -- Updated due to verification remarks
**************************************************************************************
*/

/* Includes ------------------------------------------------------------------*/
#include "vibration_meas.h"

#include "gipsy_hal_i2c_ctry.h"

#include "id_conf_ctry.h"
#include "system_params_ctry.h"

/* -----Definitions ----------------------------------------------------------------*/
#define NUM_OF_ACCEL_SAMPLES 50U
#define GDIVISOR   (uint16_t)20U  /*For acceleration conversion to G,Read value shall be divided by 20*/

#define ACCEL_DEV_ID            0xE5U  /*Value of accelerometer's Device ID */
#define ACCEL_WR_ADDR           0xA6U
#define ACCEL_RD_ADDR           0xA7U
#define ACCEL_I2C_TEMEOUT_MILIS       100U

#define X_AXIS_DATA_PTR         0x32U
#define Y_AXIS_DATA_PTR         X_AXIS_DATA_PTR + 2U
#define Z_AXIS_DATA_PTR         X_AXIS_DATA_PTR + 4U

#define ADXL375_X           0x00U
#define ADXL375_Y           0x01U
#define ADXL375_Z           0x02U

/*ADXL375 register map addresses*/
#define ADXL375_DEVID_REG          0x00U/*Device ID 11100101*/
#define ADXL375_THRESH_TAP_REG     0x1DU/*shock threshold*/
#define ADXL375_OFSX_REG           0x1EU/*X-axis offset*/
#define ADXL375_OFSY_REG           0x1FU/*Y-axis offset*/
#define ADXL375_OFSZ_REG           0x20U/*Z-axis offset*/
#define ADXL375_DUR_REG            0x21U/*shock duration*/
#define ADXL375_LATENT_REG         0x22U/*shock latency*/
#define ADXL375_WINDOW_REG         0x23U/*shock window*/
#define ADXL375_THRESH_ACT_REG     0x24U/*Activity threshold*/
#define ADXL375_THRESH_INACT_REG   0x25U/*Inactivity threshold*/
#define ADXL375_TIME_INACT_REG     0x26U/*Inactivity time*/
#define ADXL375_ACT_INACT_CTL_REG  0x27U/*Axis enable control for activity and inactivity detection*/
#define ADXL375_THRESH_FF_REG      0x28U
#define ADXL375_TIME_FF_REG        0x29U
#define ADXL375_TAP_AXES_REG       0x2AU/*Axis control for single shock/double shock*/
#define ADXL375_ACT_TAP_STATUS_REG 0x2BU/*Source of single shock/double shock*/
#define ADXL375_BW_RATE_REG        0x2CU/*Data rate and power mode control*/
#define ADXL375_POWER_CTL_REG      0x2DU/*Power saving features control*/
#define ADXL375_INT_ENABLE_REG     0x2EU/*Interrupt enable control*/
#define ADXL375_INT_MAP_REG        0x2FU/*Interrupt mapping control*/
#define ADXL375_INT_SOURCE_REG     0x30U/*Interrupt source*/
#define ADXL375_DATA_FORMAT_REG    0x31U/*Data format control*/
#define ADXL375_DATAX0_REG         0x32U/*X-Axis Data 0*/
#define ADXL375_DATAX1_REG         0x33U/*X-Axis Data 1*/
#define ADXL375_DATAY0_REG         0x34U/*Y-Axis Data 0*/
#define ADXL375_DATAY1_REG         0x35U/*Y-Axis Data 1*/
#define ADXL375_DATAZ0_REG         0x36U/*Z-Axis Data 0*/
#define ADXL375_DATAZ1_REG         0x37U/*Z-Axis Data 1*/
#define ADXL375_FIFO_CTL           0x38U/*FIFO control*/
#define ADXL375_FIFO_STATUS        0x39U/*FIFO status*/

/*Data rate codes*/
#define ADXL375_3200HZ      0x0FU
#define ADXL375_1600HZ      0x0EU
#define ADXL375_800HZ       0x0DU
#define ADXL375_400HZ       0x0CU
#define ADXL375_200HZ       0x0BU
#define ADXL375_100HZ       0x0AU
#define ADXL375_50HZ        0x09U
#define ADXL375_25HZ        0x08U
#define ADXL375_12HZ5       0x07U
#define ADXL375_6HZ25       0x06U

/*Registers' bit mapping*/
#define ADXL375_AIC_ACT_AC_BIT      7U
#define ADXL375_AIC_ACT_X_BIT       6U
#define ADXL375_AIC_ACT_Y_BIT       5U
#define ADXL375_AIC_ACT_Z_BIT       4U
#define ADXL375_AIC_INACT_AC_BIT    3U
#define ADXL375_AIC_INACT_X_BIT     2U
#define ADXL375_AIC_INACT_Y_BIT     1U
#define ADXL375_AIC_INACT_Z_BIT     0U

#define ADXL375_TAPAXIS_SUP_BIT     3U
#define ADXL375_TAPAXIS_X_BIT       2U
#define ADXL375_TAPAXIS_Y_BIT       1U
#define ADXL375_TAPAXIS_Z_BIT       0U

#define ADXL375_TAPSTAT_ACTX_BIT    6U
#define ADXL375_TAPSTAT_ACTY_BIT    5U
#define ADXL375_TAPSTAT_ACTZ_BIT    4U
#define ADXL375_TAPSTAT_ASLEEP_BIT  3U
#define ADXL375_TAPSTAT_TAPX_BIT    2U
#define ADXL375_TAPSTAT_TAPY_BIT    1U
#define ADXL375_TAPSTAT_TAPZ_BIT    0U

#define ADXL375_BW_LOWPOWER_BIT     4U
#define ADXL375_BW_RATE_BIT         3U
#define ADXL375_BW_RATE_LENGTH      4U

#define ADXL375_PCTL_LINK_BIT       5U
#define ADXL375_PCTL_AUTOSLEEP_BIT  4U
#define ADXL375_PCTL_MEASURE_BIT    3U
#define ADXL375_PCTL_SLEEP_BIT      2U
#define ADXL375_PCTL_WAKEUP_BIT     1U
#define ADXL375_PCTL_WAKEUP_LENGTH  2U

#define ADXL375_INT_DATA_READY_BIT  7U
#define ADXL375_INT_SINGLE_TAP_BIT  6U
#define ADXL375_INT_DOUBLE_TAP_BIT  5U
#define ADXL375_INT_ACTIVITY_BIT    4U
#define ADXL375_INT_INACTIVITY_BIT  3U
#define ADXL375_INT_FREE_FALL_BIT   2U
#define ADXL375_INT_WATERMARK_BIT   1U
#define ADXL375_INT_OVERRUN_BIT     0U

#define ADXL375_FORMAT_SELFTEST_BIT 7U
#define ADXL375_FORMAT_SPIMODE_BIT  6U
#define ADXL375_FORMAT_INTMODE_BIT  5U
#define ADXL375_FORMAT_FULL_RES_BIT 3U
#define ADXL375_FORMAT_JUSTIFY_BIT  2U
#define ADXL375_FORMAT_RANGE_BIT    1U
#define ADXL375_FORMAT_RANGE_LENGTH 2U

#define ADXL375_FIFO_MODE_BIT       7U
#define ADXL375_FIFO_MODE_LENGTH    2U
#define ADXL375_FIFO_TRIGGER_BIT    5U
#define ADXL375_FIFO_SAMPLES_BIT    4U
#define ADXL375_FIFO_SAMPLES_LENGTH 5U

#define ADXL375_FIFO_MODE_BYPASS    0X00U
#define ADXL375_FIFO_MODE_FIFO      0X01U
#define ADXL375_FIFO_MODE_STREAM    0X10U
#define ADXL375_FIFO_MODE_TRIGGER   0X03U
#define ADXL375_FIFOSTAT_TRIGGER_BIT        7U
#define ADXL375_FIFOSTAT_LENGTH_BIT         5U
#define ADXL375_FIFOSTAT_LENGTH_LENGTH      6U
/* -----Macros ---------------------------------------------------------------------*/
/* -----External variables ---------------------------------------------------------*/
extern I2C_HandleTypeDef g_HandleI2C_3_Accel; /*Hanle for I2C_3 bus*/
/* -----Global variables -----------------------------------------------------------*/
//uint8_t Last_Acceleration_Value = 0U;
/* -----Static Function prototypes -------------------------------------------------*/
static HAL_StatusTypeDef accel_write_reg(uint8_t accel_reg_addr, uint8_t accel_reg_data);
static HAL_StatusTypeDef accel_read_multi_reg(uint8_t accel_reg_addr, uint8_t *accel_reg_data);


/* -----Modules implementation -----------------------------------------------------*/



//HAL_StatusTypeDef Debug_Read_Max_Acceleration_Value(uint8_t *max_accel_val,uint8_t axis_type)
//{
//	uint8_t accel_samples[NUM_OF_ACCEL_SAMPLES] = {0U};
//	HAL_StatusTypeDef hal_status = HAL_OK;
//	uint8_t max_acceleration_value = 0U;
//	for(uint8_t i = 0U; (i < NUM_OF_ACCEL_SAMPLES) && (HAL_OK == hal_status); i++)
//	{
//		/*Perform acceleration sample reading*/
//		hal_status |= Debug_Get_Axis_Acceleration(&accel_samples[i],axis_type);
//		/*Find maximal acceleration value*/
//		if(max_acceleration_value < accel_samples[i])
//		{
//			max_acceleration_value = accel_samples[i];
//		}
//	}
//	if(HAL_OK == hal_status)
//	{
//		*max_accel_val = max_acceleration_value;
//	}
//	else
//	{
//		*max_accel_val = 0U;
//	}
//	return hal_status;
//}



HAL_StatusTypeDef Debug_Get_All_Axis_Acceleration(int16_t *accel_x_value,int16_t *accel_y_value,int16_t *accel_z_value)
{
	HAL_StatusTypeDef accel_status = HAL_OK;
	uint8_t ucAxisRegData[6U] = {0U};
	int16_t read_accel_value = 0U;

	accel_status |= accel_read_multi_reg(X_AXIS_DATA_PTR, ucAxisRegData);

	if(HAL_OK == accel_status)
	{

			*accel_x_value  = (int16_t)(((int16_t)ucAxisRegData[1] << 8U) + ucAxisRegData[0]);
			*accel_y_value  = (int16_t)(((int16_t)ucAxisRegData[3] << 8U) + ucAxisRegData[2]);
			*accel_z_value  = (int16_t)(((int16_t)ucAxisRegData[5] << 8U) + ucAxisRegData[4]);

	}
	else
	{
		*accel_x_value = 0;/*Set acceleration sample as 0*/
		*accel_y_value = 0;/*Set acceleration sample as 0*/
		*accel_z_value = 0;/*Set acceleration sample as 0*/
	}
	return accel_status;
}



/*************************************************************************************
** accel_write_reg - Write single byte to an 8-bit accelerometer register
**
** Params : uint8_t accel_reg_addr - Register's address
** 			uint8_t accel_reg_data - Register's data
**
** Returns: HAL_StatusTypeDef - Resister's write status
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef accel_write_reg(uint8_t accel_reg_addr, uint8_t accel_reg_data)
{
	HAL_StatusTypeDef hal_status = HAL_ERROR;
	uint8_t data_accel[2U]={accel_reg_addr,accel_reg_data};

	hal_status = HAL_I2C_Master_Transmit (&g_HandleI2C_3_Accel, (uint16_t)ACCEL_WR_ADDR, &data_accel, 2U, ACCEL_I2C_TEMEOUT_MILIS,TRUE);
	return hal_status;
}

/*************************************************************************************
** accel_read_reg - Read single byte from an 8-bit accelerometer register
**
** Params : uint8_t accel_reg_addr - Register's address
** 			uint8_t *accel_reg_data - Pointer to register's data
**
** Returns: HAL_StatusTypeDef - Resister's read status
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef accel_read_multi_reg(uint8_t accel_reg_base_addr, uint8_t *accel_reg_data)
{
	HAL_StatusTypeDef hal_status = HAL_ERROR;
	hal_status = HAL_I2C_Master_Transmit (&g_HandleI2C_3_Accel, (uint16_t)ACCEL_WR_ADDR,&accel_reg_base_addr, 1U, ACCEL_I2C_TEMEOUT_MILIS,TRUE);
	if(HAL_OK == hal_status)
	{
		hal_status = HAL_I2C_Master_Receive(&g_HandleI2C_3_Accel, (uint16_t)ACCEL_RD_ADDR, accel_reg_data, 6U, ACCEL_I2C_TEMEOUT_MILIS);
	}
	return hal_status;
}
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
