/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : id_conf_ctry.c
** Author    : Leonid Savchenko
** Revision  : 1.3
** Updated   : 20-11-2020
**
** Description: This file contains procedures for control and configuration of
** 				the Discrete-to-Digital sensor IC HI8429 and CTRY's configuration ID read.
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1  : 18-08-2020 -- Updated HI8429_Init(). Added delays during BIT test.
** Rev 1.2  : 15-11-2020 -- Added convert_config_pins_to_ctry_id(), updated HI8429_Init()
** Rev 1.3  : 20-11-2020 -- Static analysis update
**************************************************************************************
*/


/* -----Includes ---------------------------------------------------------------------*/
#include "id_conf_ctry.h"

#include "gipsy_hal_rcc.h"

#include "mcu_config_ctry.h"
#include "system_params_ctry.h"

/* -----Definitions ----------------------------------------------------------------*/
/*Size of HI8429 internal registers (bits)*/
#define HI8429_CONF_SIZE_BITS  9U
#define HI8429_DATA_SIZE_BITS  8U
#define HI8429_THRES_SIZE_BITS 24U

/* Threshold voltage value for Ground/Open operation mode
 * calculated using the formula: Vthresh(G/O)=VDD*(0.126+ D/91.6) Volt
 * When: VDD = 14.8V , D = 6bit threshold value
 * For required Low_Thres  = 3V     D=7
 * For required High_Thres = 7V     D=32
 * */
#define HI8429_GL_THRES_VOLT_VAL  7U
#define HI8429_GH_THRES_VOLT_VAL  32U
#define HI849_BIT_VAL_1           (uint8_t)(0xAAU)
#define HI849_BIT_VAL_2           (uint8_t)(0x55U)

/*Position of Threshold values in 24bit data*/
#define HI8429_GL_THRES_POSITON  19U
#define HI8429_GH_THRES_POSITON  13U


/*Mode selection inputs states*/
#define SEL_CONF_WR_RD    0U
#define SEL_THRES_WR_RD   1U
#define SEL_DATA_RD       2U
#define SEL_DEFAULT_STATE 3U

#define CTRY_ERR_ID   99U


/* -----Macros ---------------------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/
static uint8_t Ctry_ID_Value = CTRY_ERR_ID;

/* -----Static Function prototypes -------------------------------------------------*/
static void set_sel_inputs(uint8_t sel_mode);
static void hi8429_spi_write(uint8_t oper_type,uint32_t tx_data,uint8_t data_size_bits);
static uint32_t hi8429_spi_read(uint8_t oper_type,uint32_t tx_data,uint8_t data_size_bits);
static uint16_t convert_config_pins_to_ctry_id(uint16_t conf_pins_value);
/* -----Modules implementation -----------------------------------------------------*/

/*************************************************************************************
** HI8429_Init - This function performs initialization of the HI8429 by writing
** 				 config register and setting threshold values, performs IC BIT and reads CTRY ID from
** 				 configuration inputs while checking validity.
**
** Params : None.
**
** Returns: eCTRY_ID_Status - HI8429 init and CTRY's ID read status.
** Notes:
*************************************************************************************/
eCTRY_ID_Status HI8429_Init(void)
{
	uint32_t config_data = 0x000000FFU;/*Config Data : Lower 8 bits are '1'(Ground/Open Mode), 9th bit is '0'(BIT Disabled)*/
	uint32_t thres_data = 0x00000000U;
	unsigned short ctry_id_value_temp = 0U;
	uint8_t conf_id_pins_vaue = 0U;
	uint8_t conf_id_parity = 0U;
	eCTRY_ID_Status id_read_result = E_CTRY_ID_OK;

	HAL_GPIO_WritePin(CONF_ID_RESET_PORT,CONF_ID_RESET_PIN,E_GPIO_PIN_SET);/*Disable IC manual Reset*/
	/*Set SEL0/1 and CS inputs to default state*/
	set_sel_inputs(SEL_DEFAULT_STATE);

	/*Configure the HI8429 IC*/
	hi8429_spi_write(SEL_CONF_WR_RD,config_data,HI8429_CONF_SIZE_BITS);
	thres_data |=  ((uint32_t)HI8429_GL_THRES_VOLT_VAL << HI8429_GL_THRES_POSITON);
	thres_data |=  ((uint32_t)HI8429_GH_THRES_VOLT_VAL << HI8429_GH_THRES_POSITON);
	hi8429_spi_write(SEL_THRES_WR_RD,thres_data,HI8429_THRES_SIZE_BITS);
	HAL_Delay(100U);
	/*Perform HI8429 BIT*/
	hi8429_spi_write(SEL_CONF_WR_RD,(uint32_t)(0x100U | HI849_BIT_VAL_1),HI8429_CONF_SIZE_BITS);
	HAL_Delay(100U);
	if(HI849_BIT_VAL_1 == (uint8_t)(hi8429_spi_read(SEL_DATA_RD,0,HI8429_DATA_SIZE_BITS) & 0x000000FFU))
	{
		HAL_Delay(100U);
		hi8429_spi_write(SEL_CONF_WR_RD,(uint32_t)(0x100U | HI849_BIT_VAL_2),HI8429_CONF_SIZE_BITS);
		HAL_Delay(100U);
		if(HI849_BIT_VAL_2 == (uint8_t)(hi8429_spi_read(SEL_DATA_RD,0,HI8429_DATA_SIZE_BITS) & 0x000000FFU))
		{
			HAL_Delay(100U);
			/*Re-Configure the HI8429 IC and Exit BIT Mode*/
			hi8429_spi_write(SEL_CONF_WR_RD,config_data,HI8429_CONF_SIZE_BITS);
			HAL_Delay(100U);
		}
		else
		{
			id_read_result = E_CTRY_ID_BIT_FAIL;
		}
	}
	else
	{
		id_read_result = E_CTRY_ID_BIT_FAIL;
	}

	/*Read CTRY's ID from input ID configuration pins*/
	if(E_CTRY_ID_OK == id_read_result) /*Perform ID read only if BIT succeeded*/
	{
		/*Read configuration CTRY ID inputs (7bit wide)*/
		conf_id_pins_vaue = (uint8_t)(hi8429_spi_read(SEL_DATA_RD,0U,HI8429_DATA_SIZE_BITS) & 0x000000FFU);
		ctry_id_value_temp = (uint16_t)(conf_id_pins_vaue & 0x3FU);/*Get CTRY ID from configuration inputs (6bit wide)*/

		/*Perform parity check of Configuration inputs*/
		while(0U != conf_id_pins_vaue)
		{
			conf_id_pins_vaue = !conf_id_parity;
			conf_id_pins_vaue = conf_id_pins_vaue & (conf_id_pins_vaue - 1U);
		}
		/*Check Parity test result*/
		if(0U == conf_id_parity)/*if calculated even parity = 0 -> parity check passed*/
		{
			ctry_id_value_temp = ~ctry_id_value_temp;
			ctry_id_value_temp = ctry_id_value_temp & 0x003FU;
			ctry_id_value_temp = convert_config_pins_to_ctry_id((uint16_t)(ctry_id_value_temp & 0x003FU));

			/* Check if read CTRY_ID is in valid range between 1 to 20*/
			if((0U < ctry_id_value_temp) && (CTRY_MAX_NUM >= ctry_id_value_temp))
			{
				/* Save CTRY_ID in range between 0 to 19 into global variable*/
				Ctry_ID_Value = (uint8_t)((ctry_id_value_temp - 1U) & 0x003FU);
				id_read_result = E_CTRY_ID_OK;
			}
			else
			{
				id_read_result = E_CTRY_ID_FAIL;
			}

		}
		else /*if parity check failed -> CTRY's ID read failed*/
		{
			id_read_result = E_CTRY_ID_FAIL;
		}
	}
	return id_read_result;
}
/*************************************************************************************
** Get_CTRY_ID - This function returns data that represent the value of
** 					  Input discretes which represents the ID value(0-19).
** Params : None.
**
** Returns: uint8_t  - Data that was read from the sensor.
** Notes:
*************************************************************************************/
uint8_t Get_CTRY_ID(void)
{
	return Ctry_ID_Value;
}

/*************************************************************************************
** convert_config_pins_to_ctry_id - This function converts configuration pins values to CTRY Id
**
** Params : uint16_t  conf_pins_value- Configuration input pins value
**
** Returns: uint16_t  - Ctry ID evaluated from configuration pins
** Notes:
*************************************************************************************/
static uint16_t convert_config_pins_to_ctry_id(uint16_t conf_pins_value)
{
	uint16_t ctry_id_value = CTRY_ERR_ID;
	switch(conf_pins_value)
	{
	case 1U:
		ctry_id_value = 3U;
		break;
	case 2U:
		ctry_id_value = 4U;
		break;
	case 3U:
		ctry_id_value = 11U;
		break;
	case 4U:
		ctry_id_value = 12U;
		break;
	case 5U:
		ctry_id_value = 9U;
		break;
	case 6U:
		ctry_id_value = 10U;
		break;
	case 7U:
		ctry_id_value = 7U;
		break;
	case 8U:
		ctry_id_value = 8U;
		break;
	case 9U:
		ctry_id_value = 5U;
		break;
	case 10U:
		ctry_id_value = 6U;
		break;
	case 11U:
		ctry_id_value = 13U;
		break;
	case 12U:
		ctry_id_value = 14U;
		break;
	case 13U:
		ctry_id_value = 15U;
		break;
	case 14U:
		ctry_id_value = 16U;
		break;
	case 15U:
		ctry_id_value = 17U;
		break;
	case 16U:
		ctry_id_value = 18U;
		break;
	case 17U:
		ctry_id_value = 19U;
		break;
	case 18U:
		ctry_id_value = 20U;
		break;
	case 21U:
		ctry_id_value = 1U;
		break;
	case 22U:
		ctry_id_value = 2U;
		break;
	default:
		ctry_id_value = (uint16_t)CTRY_ERR_ID;
		break;
	}

	return ctry_id_value;

}
/*************************************************************************************
** set_sel_inputs - This function sets both SEL inputs according to Sensor's reuired
**                  type of operation.
** 								.
**
** Params : uint8_t sel_mode - Definition of required operation
**
** Returns: None.
** Notes:
*************************************************************************************/
static void set_sel_inputs(uint8_t sel_mode)
{
	switch(sel_mode)
	{
	case SEL_CONF_WR_RD:
		/*SEL0-'0' , SEL1-'0' - Read/Write Configuration register*/
		HAL_GPIO_WritePin(CONF_ID_SEL0_PORT,CONF_ID_SEL0_PIN,E_GPIO_PIN_RESET);
		HAL_GPIO_WritePin(CONF_ID_SEL1_PORT,CONF_ID_SEL1_PIN,E_GPIO_PIN_RESET);
		HAL_GPIO_WritePin(CONF_ID_SPI_CS_PORT,CONF_ID_SPI_CS_PIN,E_GPIO_PIN_RESET);
		break;
	case SEL_THRES_WR_RD:
		/*SEL0-'0' , SEL1-'1' - Read/Write Threshold register*/
		HAL_GPIO_WritePin(CONF_ID_SEL0_PORT,CONF_ID_SEL0_PIN,E_GPIO_PIN_RESET);
		HAL_GPIO_WritePin(CONF_ID_SEL1_PORT,CONF_ID_SEL1_PIN,E_GPIO_PIN_SET);
		HAL_GPIO_WritePin(CONF_ID_SPI_CS_PORT,CONF_ID_SPI_CS_PIN,E_GPIO_PIN_RESET);
		break;
	case SEL_DATA_RD:
		/*SEL0-'1' , SEL1-'0' - Read Data register*/
		HAL_GPIO_WritePin(CONF_ID_SEL0_PORT,CONF_ID_SEL0_PIN,E_GPIO_PIN_SET);
		HAL_GPIO_WritePin(CONF_ID_SEL1_PORT,CONF_ID_SEL1_PIN,E_GPIO_PIN_RESET);/*May be not used L.S.*/
		HAL_GPIO_WritePin(CONF_ID_SPI_CS_PORT,CONF_ID_SPI_CS_PIN,E_GPIO_PIN_RESET);
		break;
	case SEL_DEFAULT_STATE:
		/*SEL0-'1' , SEL1-'1' - Default State -> SET both SEL inputs*/
		HAL_GPIO_WritePin(CONF_ID_SPI_CS_PORT,CONF_ID_SPI_CS_PIN,E_GPIO_PIN_SET);
		HAL_GPIO_WritePin(CONF_ID_SEL0_PORT,CONF_ID_SEL0_PIN,E_GPIO_PIN_SET);
		HAL_GPIO_WritePin(CONF_ID_SEL1_PORT,CONF_ID_SEL1_PIN,E_GPIO_PIN_SET);
		break;
	default:
		break;
	}
}
/*************************************************************************************
** hi8429_spi_write - This function writes values to internal HI849 registers using
** 					  SPI "Bit-Bang" communication
**
** Params : uint8_t oper_type - Definition of required operation
**          uint32_t tx_data - Data to write
**          uint8_t data_size_bits - Amount of Bits to write to device
**
**
** Returns: None.
** Notes:
*************************************************************************************/
static void hi8429_spi_write(uint8_t oper_type,uint32_t tx_data,uint8_t data_size_bits)
{
	uint32_t data_mask = 1U << (data_size_bits - 1U);

	if((SEL_CONF_WR_RD == oper_type) || (SEL_THRES_WR_RD == oper_type))/*Check valid Write mode*/
	{
		/*Set SPI_CLK in default state '0'*/
		HAL_GPIO_WritePin(CONF_ID_SPI_PORT,CONF_ID_SPI_CLK_PIN,E_GPIO_PIN_RESET);
		/*Set SEL0/1 and CS inputs to default state*/
		set_sel_inputs(SEL_DEFAULT_STATE);
		HAL_Delay(1U);
		/*Set SEL0/1 and CS inputs according to required mode*/
		set_sel_inputs(oper_type);

		/*Perform BitBang SPI write procedure*/
		for(uint8_t i = 0U; i < data_size_bits; i++)
		{
			if(0U == (tx_data & data_mask))
			{
				HAL_GPIO_WritePin(CONF_ID_SPI_PORT,CONF_ID_SPI_MOSI_PIN,E_GPIO_PIN_RESET);
			}
			else
			{
				HAL_GPIO_WritePin(CONF_ID_SPI_PORT,CONF_ID_SPI_MOSI_PIN,E_GPIO_PIN_SET);
			}
			/*Apply single Clock cycle*/
			HAL_GPIO_WritePin(CONF_ID_SPI_PORT,CONF_ID_SPI_CLK_PIN,E_GPIO_PIN_SET);
			HAL_Delay(1U);
			HAL_GPIO_WritePin(CONF_ID_SPI_PORT,CONF_ID_SPI_CLK_PIN,E_GPIO_PIN_RESET);
			HAL_Delay(1U);
			/*Shift tx_data_mask for next Data Bit*/
			data_mask = data_mask >> 1U;
		}
		set_sel_inputs(SEL_DEFAULT_STATE);
	}
}
/*************************************************************************************
** hi8429_spi_read - This function reads values from internal HI849 registers using
** 					  SPI "Bit-Bang" communication
**
** Params : uint8_t oper_type - Definition of required operation
**          uint32_t tx_data - Data to write (if required Write/Read operation)
**          uint8_t data_size_bits - Amount of Bits to read form device
**
**
** Returns: uint32_t - Read data value.
** Notes:
*************************************************************************************/
static uint32_t hi8429_spi_read(uint8_t oper_type,uint32_t tx_data,uint8_t data_size_bits)
{
	uint32_t data_mask = (uint32_t)(1U << (data_size_bits - 1U));
	uint32_t rx_data = 0x00000000U;

	/*Set SPI_CLK in default state '0'*/
	HAL_GPIO_WritePin(CONF_ID_SPI_PORT,CONF_ID_SPI_CLK_PIN,E_GPIO_PIN_RESET);
	/*Set SEL0/1 and CS inputs to default state*/
	set_sel_inputs(SEL_DEFAULT_STATE);
	HAL_Delay(1U);
	/*Set SEL0/1 and CS inputs according to required mode*/
	set_sel_inputs(oper_type);
	HAL_Delay(1U);

	/*Perform BitBang SPI read-write procedure*/
	for(uint8_t i = 0U; i < data_size_bits; i++)
	{
		if(E_GPIO_PIN_SET == HAL_GPIO_ReadPin(CONF_ID_SPI_PORT,CONF_ID_SPI_MISO_PIN))
		{
			rx_data = rx_data | data_mask;
		}
		else
		{
			/*Do nothing*/
		}
		if(0U == (tx_data & data_mask))
		{
			HAL_GPIO_WritePin(CONF_ID_SPI_PORT,CONF_ID_SPI_MOSI_PIN,E_GPIO_PIN_RESET);
		}
		else
		{
			HAL_GPIO_WritePin(CONF_ID_SPI_PORT,CONF_ID_SPI_MOSI_PIN,E_GPIO_PIN_SET);
		}
		HAL_GPIO_WritePin(CONF_ID_SPI_PORT,CONF_ID_SPI_CLK_PIN,E_GPIO_PIN_SET);
		HAL_Delay(1U);
		HAL_GPIO_WritePin(CONF_ID_SPI_PORT,CONF_ID_SPI_CLK_PIN,E_GPIO_PIN_RESET);
		HAL_Delay(1U);
		data_mask = data_mask >> 1U;
	}
	set_sel_inputs(SEL_DEFAULT_STATE);

	return rx_data;
}
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
