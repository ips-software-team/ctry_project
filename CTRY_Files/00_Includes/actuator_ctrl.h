/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : actuator_ctrl.h
** Author    : Leonid Savchenko
** Revision  : 1.4
** Updated   : 21-05-2023
**
** Description: Header for system_params_ctry.c file
**
*/


/* Revision Log:
**
** Rev 1.0  : 16-03-2020 -- Original version created.
** Rev 1.1  : 21-10-2020 -- Added defines
** Rev 1.2  : 24-12-2020 -- Updated due to verification remarks
** Rev 1.3  : 11-07-2021 -- Added doxygen comments
** Rev 1.4  : 21-05-2023 -- Added E_ACT_OFF_REQ to e_ActuatorActivationState
**                          Added E_ACT_DEACTIVATED to e_ActuatorActivationState
**************************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef ACTUATOR_CTRL_H
#define ACTUATOR_CTRL_H

/* -----Includes -------------------------------------------------------------------*/
#include "mcu_config_ctry.h"
/* -----Definitions ----------------------------------------------------------------*/
#define DO_NOT_ACTIVATE_MOTOR  0x07U
#define NO_ACTIVE_ACTUATOR  (uint8_t)0x7U

#define LEFT_WING                 1U
#define RIGHT_WING                0U
#define NUM_OF_WINGS              2U
/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/

/*! Enumerator for actuators activation state*/
typedef enum
{
	E_ACT_NO_CHANGE = 0U,		/*!<Val:0 -  No change in activation  */
	E_ACT_ACTIVATED,			/*!<Val:1 -  Actuator activated       */
	E_ACT_DEACTIVATED,			/*!<Val:2 -  Actuators de-activated   */
	E_ACT_OFF_REQ			    /*!<Val:3 -  Actuators OFF request    */
}e_ActuatorActivationState;

/*! Enumerator for actuators activation parameters handling status*/
typedef enum
{
	E_ACT_PARAMS_CLEARED = 0U,	/*!<Val:0 -  Activation parameters cleared  */
	E_ACT_PARAMS_UPDATED,       /*!<Val:1 -  Activation parameters updated  */
	E_ACT_PARAMS_CHECKED,       /*!<Val:2 -  Activation parameters checked  */
	E_ACT_PARAMS_IGNORED,       /*!<Val:3 -  Activation parameters ignored  */
	E_ACT_PARAMS_ERROR,         /*!<Val:4 -  Activation parameters error    */
}e_ActuatorPairParamsStatus;

/*! Enumerator for actuators activation mode*/
typedef enum
{
	E_ACT_MODE_OPER = 0U,      /*!<Val:0 -  OPERational Mode */
	E_ACT_MODE_IBIT,           /*!<Val:1 -  IBIT Mode        */
	E_ACT_MODE_MAN             /*!<Val:2 -  MANual Mode      */
}e_ActuatorActivationMode;

/*! Enumerator for sanity check result*/
typedef enum
{
	E_SANITY_OK = 0U,				/*!<Val:0 -  Sanity check passed 						*/
	E_SANITY_PARAM_FL,              /*!<Val:1 -  Sanity check parameter failure 			*/
	E_SANITY_TBL_MONIT_FL,          /*!<Val:2 -  Sanity check table monitoring failure 		*/
	E_SANITY_CTRY_ID_FL,            /*!<Val:3 -  Sanity check CTRY ID failure 				*/
	E_SANITY_ACT_PORT_MONIT_FL,     /*!<Val:4 -  Sanity check actuator's port failure 		*/
	E_SANITY_OPER_INTERVAL_FL       /*!<Val:5 -  Sanity check operational interval failure 	*/
}e_CtrySanityCheckResult;

/*! Structure for active actuator's parameters*/
typedef struct
{
	uint8_t CtryId; 	/*!< Specifies CTRY ID value (Range: 0 - 19 | Unit: None)			*/
	uint8_t ActNum;		/*!< Specifies actuator's number value (Range: 1 - 6 | Unit: None)	*/
	uint8_t ActRpm;		/*!< Specifies actuator's speed value (Range: 0 - 100 | Unit: %)	*/
}ActiveActParams_st;

/*! Structure for actuator's parameters*/
typedef struct
{
	uint8_t CtryId;      /*!< Specifies CTRY ID value (Range: 0 - 19 | Unit: None)			*/
	uint8_t ActNum;      /*!< Specifies actuator's number value (Range: 1 - 6 | Unit: None)	*/
}ActParams_st;

/*! Structure for actuator's pair parameters*/
typedef struct
{
	ActiveActParams_st ActParamsCurrent[NUM_OF_WINGS];	/*!< Specifies structure for active actuator's parameters (Range: var type | Unit: None)*/
	e_ActuatorPairParamsStatus ActParamsStatus;			/*!< Specifies actuator activation parameters' status (Range: 0 - 4 | Unit: None)		*/
	e_ActuatorActivationMode ActMode;					/*!< Specifies activation mode (Range: 0 - 2 | Unit: None)								*/
	uint32_t Act_ON_Time_Current;  						/*!< Specifies activation start time (Range: var type | Unit: milliseconds)				*/
	uint32_t Act_ON_Time_Previous;  					/*!< Specifies activation stop time (Range: var type | Unit: milliseconds)				*/
}st_ActuatorsPair;

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/
extern st_ActuatorsPair g_ActuatorsPair;
/* -----Function prototypes --------------------------------------------------------*/
e_ActuatorActivationState Actuators_Activation_Management(void);
void Perform_Actuators_Deactivation(void);
uint8_t Get_Active_Actuator_Num(void);
void Set_Active_Actuator_Num(uint8_t act_num);
void Init_Actuator_Pair_Parameters(void);
void Monitor_MOTOR_OPER_Input_State(void);

e_CtrySanityCheckResult Received_Actuators_Params_Sanity_Monitoring(void);

#endif /*ACTUATOR_CTRL_H*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
