/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : vibration_meas.h
** Author    : Leonid Savchenko
** Revision  : 1.5
** Updated   : 11-07-2021
**
** Description: Header for vibration_meas.c file
**
*/


/* Revision Log:
**
** Rev 1.0  : 25-08-2020 -- Original version created.
** Rev 1.1  : 24-10-2020 -- Updated function prototypes
** Rev 1.2  : 22-11-2020 -- Updated due to verification remarks
** Rev 1.3  : 26-04-2021 -- Removed prototypes,added define
** Rev 1.4  : 29-06-2021 -- Updated due to verification remarks
** Rev 1.5  : 11-07-2021 -- Added doxygen comments
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef VIBRATION_MEAS_H
#define VIBRATION_MEAS_H

/* -----Includes -------------------------------------------------------------------*/
#include "gipsy_hal_def.h"
/* -----Definitions ----------------------------------------------------------------*/
#define ACCEL_VAL_ERR 0xFFU
/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/
/*! Enumerator for acceleration monitoring reult state definition*/
typedef enum
{
	E_ACCEl_MONITOR_OK = 0U,	/*!<Val:0 -  Monitor OK		*/
	E_ACCEL_MONITOR_FAIL		/*!<Val:0 -  Monitor Failed	*/
}e_AccelMonitorResult;
/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/
e_AccelMonitorResult Perform_Acceleration_Monitoring(uint8_t accel_lim_max, uint8_t accel_lim_min);
HAL_StatusTypeDef Read_Max_Acceleration_Value();
HAL_StatusTypeDef Init_Accelerometer(void);
HAL_StatusTypeDef Check_Accelerometer_ID(void);
uint8_t Get_Last_Acceleration_Val(void);

#endif /*VIBRATION_MEAS_H*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
