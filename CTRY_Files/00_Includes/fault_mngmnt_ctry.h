/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : fault_mngmnt_ctry.h
** Author    : Leonid Savchenko
** Revision  : 1.1
** Updated   : 05-06-2021
**
** Description: Header for fault_mngmnt_ctry.c file
**
*/


/* Revision Log:
**
** Rev 1.0  : 19-10-2020 -- Original version created.
** Rev 1.1  : 05-06-2021 -- Updated defines
**************************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef FAULT_MNGMNT_CTRY_H
#define FAULT_MNGMNT_CTRY_H

/* -----Includes -------------------------------------------------------------------*/
#include "gipsy_hal_def.h"
/* -----Definitions ----------------------------------------------------------------*/

#define CTRY_FLT_MDRV_BASE          (uint16_t)0x0001U
#define CTRY_FLT_CURR_CONSUMPTION   (uint16_t)0x0040U
#define CTRY_FLT_ACCELEROMETER      (uint16_t)0x0080U
#define CTRY_FLT_POWER_IN           (uint16_t)0x0100U
#define CTRY_FLT_POWER_5V           (uint16_t)0x0200U
#define CTRY_FLT_TEMP_SENSORS       (uint16_t)0x0400U
#define CTRY_FLT_POW_SWITCH         (uint16_t)0x0800U
#define CTRY_FLT_TABLE_MONIT        (uint16_t)0x1000U
#define CTRY_FLT_SANITY_MONIT       (uint16_t)0x2000U
#define CTRY_FLT_OVER_TEMP          (uint16_t)0x4000U
#define CTRY_FLT_SW                 (uint16_t)0x8000U

/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/
void CTRY_Fault_Management(void);
void Increment_SW_Error_Counter(void);
#endif /* FAULT_MNGMNT_CTRY_H */
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
