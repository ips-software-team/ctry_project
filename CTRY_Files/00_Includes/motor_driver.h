/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : motor_driver.h
** Author    : Leonid Savchenko
** Revision  : 1.3
** Updated   : 11-07-2021
**
** Description: Header for motor_driver.c file
**
*/


/* Revision Log:
**
** Rev 1.0  : 20-07-2020 -- Original version created.
** Rev 1.1  : 23-08-2020 -- Added define:SHUT_ALL_MDRVS,MDRV_ALL_PINS
** Rev 1.2  : 30-06-2021 -- Updated due to verification remarks
** Rev 1.3  : 11-07-2021 -- Added doxygen comments
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef MOTOR_DRIVER_H
#define MOTOR_DRIVER_H

/* -----Includes -------------------------------------------------------------------*/
#include "actuator_ctrl.h"
#include "mcu_config_ctry.h"
/* -----Definitions ----------------------------------------------------------------*/
#define MAX_NUM_OF_MOTOR_DRIVERS  6U
#define SHUT_ALL_MDRVS        6U
#define MDRV_ALL_PINS         7U
/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/
/*! Enumerator for motor driver's status*/
typedef enum MDRVStatus
{
	E_MDRV_OK = 0U,      /*!<Val:0 -  Motor driver OK 		*/
	E_MDRV_FL,           /*!<Val:1 -  Motor driver Failed 	*/
}e_MDRVStatus;

/*! Structure for motor driver's pins parameters*/
typedef struct
{
	st_PinDescriptor MDRV_CS_Pin;		/*!< Specifies Motor driver's CS pin parameters (Range: var type | Unit: See struct declaration)	*/
	st_PinDescriptor MDRV_WAKE_Pin;     /*!< Specifies Motor driver's WAKE pin parameters (Range: var type | Unit: See struct declaration)	*/
	st_PinDescriptor MDRV_EN_Pin;       /*!< Specifies Motor driver's EN pin parameters (Range: var type | Unit: See struct declaration)	*/
	st_PinDescriptor MDRV_DIAG_Pin;     /*!< Specifies Motor driver's DIAG pin parameters (Range: var type | Unit: See struct declaration)	*/
}st_MdrvPinDescriptor;

/*! Structure for motor driver's PWM channel parameters*/
typedef struct
{
	TIM_HandleTypeDef *htimMdrv;	/*!< Specifies address of TIM handler structure (Range: var type | Unit: None)					*/
	uint32_t   timChannel;			/*!< Specifies motor driver's PWM channel (Range: TIM_CHANNEL_1 - TIM_CHANNEL_4 | Unit: None)	*/
}st_MdrvPwmChannels;

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/
void MDRV_Wake_All(void);
e_MDRVStatus MDRV_Config(uint8_t mdrv_num);
void MDRV_Shut_Down(uint8_t mdrv_num);
void MDRV_Activate_Motor(uint8_t mdrv_num,uint8_t mot_speed);
e_MDRVStatus MDRV_ID_Check(uint8_t mdrv_num);
void MDRV_SPI_Write(uint8_t mdrv_num,uint8_t mdrv_reg_addr, uint8_t data_byte_to_write);
void MDRV_SPI_Read(uint8_t mdrv_num,uint8_t mdrv_reg_addr, uint8_t *config_data, uint8_t *reg_data);
void MDRV_Wake_Ctrl(uint8_t mdrv_num, GPIO_PinState pin_state);
void MDRV_Enable_Ctrl(uint8_t mdrv_num, GPIO_PinState pin_state);

#endif /*MOTOR_DRIVER_H*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
