/*************************************************************************************
 * File:		actuator_params.h
 * Purpose:
 *
 * Notes:
 **************************************************************************************/
#ifndef _ACTUATOR_PARAMS_H_
#define _ACTUATOR_PARAMS_H_

#include "module_init_ctry.h"
#include "actuator_ctrl.h"

#define ACT_SPEED_PERCENT_DEAFAULT 80U

st_CtryActParams SysCtryActParamTable[CTRY_MAX_NUM]=
{
		/*CTRY_01 Actuators' Parameters*/
		{
					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
			{       E_ACT_OK, E_ACT_NA, E_ACT_NA, E_ACT_NA, E_ACT_NA, E_ACT_NA}, /*Actuator Status*/
			{            80U,       0U,      80U,      80U,      0U,      80U}, /*Actuator operational speed*/
			{
					{   255U,       0U,     255U,     255U,       0U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,       0U,     200U,     200U,       0U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{    50U,       0U,      50U,      50U,       0U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
					{   150U,       0U,     150U,     150U,       0U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
					{    70U,       0U,      70U,      70U,       0U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
			}
		},
		/*CTRY_02 Actuators' Parameters*/
		{
					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
			{       E_ACT_OK, E_ACT_NA, E_ACT_NA, E_ACT_NA, E_ACT_NA, E_ACT_NA}, /*Actuator Status*/
			{            80U,       0U,      80U,      80U,      0U,      80U}, /*Actuator operational speed*/
			{
					{   255U,       0U,     255U,     255U,       0U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,       0U,     200U,     200U,       0U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{    50U,       0U,      50U,      50U,       0U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
					{   150U,       0U,     150U,     150U,       0U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
					{    70U,       0U,      70U,      70U,       0U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
			}
		},
		/*CTRY_03 Actuators' Parameters*/
		{
					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
			{       E_ACT_NA, E_ACT_NA, E_ACT_OK, E_ACT_NA, E_ACT_NA, E_ACT_OK}, /*Actuator Status*/
			{             0U,       0U,      80U,       0U,       0U,      80U}, /*Actuator operational speed*/
			{
					{     0U,       0U,     255U,       0U,       0U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{     0U,       0U,     200U,       0U,       0U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,       0U,      50U,       0U,       0U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
					{     0U,       0U,     150U,       0U,       0U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
					{     0U,       0U,      70U,       0U,       0U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
			}
		},
		/*CTRY_04 Actuators' Parameters*/
		{
					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
			{       E_ACT_NA, E_ACT_NA, E_ACT_OK, E_ACT_NA, E_ACT_NA, E_ACT_OK}, /*Actuator Status*/
			{             0U,       0U,      80U,       0U,       0U,      80U}, /*Actuator operational speed*/
			{
					{     0U,       0U,     255U,       0U,       0U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{     0U,       0U,     200U,       0U,       0U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,       0U,      50U,       0U,       0U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
					{     0U,       0U,     150U,       0U,       0U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
					{     0U,       0U,      70U,       0U,       0U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
			}
		},
		/*CTRY_05 Actuators' Parameters*/
		{
					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
			{       E_ACT_OK, E_ACT_NA, E_ACT_NA, E_ACT_OK, E_ACT_NA, E_ACT_OK}, /*Actuator Status*/
			{            80U,       0U,       0U,      80U,       0U,      80U}, /*Actuator operational speed*/
			{
					{   255U,       0U,       0U,     255U,       0U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,       0U,       0U,     200U,       0U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{    50U,       0U,       0U,      50U,       0U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
					{   150U,       0U,       0U,     150U,       0U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
					{    70U,       0U,       0U,      70U,       0U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
			}
		},
		/*CTRY_06 Actuators' Parameters*/
		{
					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
			{       E_ACT_OK, E_ACT_NA, E_ACT_NA, E_ACT_OK, E_ACT_NA, E_ACT_OK}, /*Actuator Status*/
			{            80U,       0U,       0U,      80U,       0U,      80U}, /*Actuator operational speed*/
			{
					{   255U,       0U,       0U,     255U,       0U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,       0U,       0U,     200U,       0U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{    50U,       0U,       0U,      50U,       0U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
					{   150U,       0U,       0U,     150U,       0U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
					{    70U,       0U,       0U,      70U,       0U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
			}
		},
		/*CTRY_7 Actuators' Parameters*/
		{
					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
			{       E_ACT_OK, E_ACT_NA, E_ACT_OK, E_ACT_OK, E_ACT_NA, E_ACT_OK}, /*Actuator Status*/
			{            0U,      80U,      80U,      80U,       0U,      80U}, /*Actuator operational speed*/
			{
					{     0U,     255U,     255U,     255U,       0U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{     0U,     200U,     200U,     200U,       0U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,      50U,      50U,      50U,       0U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
					{     0U,     150U,     150U,     150U,       0U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
					{     0U,      70U,      70U,      70U,       0U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
			}
		},
		/*CTRY_8 Actuators' Parameters*/
		{
					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
			{       E_ACT_OK, E_ACT_NA, E_ACT_OK, E_ACT_OK, E_ACT_NA, E_ACT_OK}, /*Actuator Status*/
			{            0U,      80U,      80U,      80U,       0U,      80U}, /*Actuator operational speed*/
			{
					{     0U,     255U,     255U,     255U,       0U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{     0U,     200U,     200U,     200U,       0U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,      50U,      50U,      50U,       0U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
					{     0U,     150U,     150U,     150U,       0U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
					{     0U,      70U,      70U,      70U,       0U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
			}
		},
		/*CTRY_09 Actuators' Parameters*/
		{
					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
			{       E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_NA, E_ACT_OK, E_ACT_OK}, /*Actuator Status*/
			{            80U,      80U,      80U,       0U,      80U,      80U}, /*Actuator operational speed*/
			{
					{   255U,     255U,     255U,       0U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,     200U,     200U,       0U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{    50U,      50U,      50U,       0U,      50U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
					{   150U,     150U,     150U,       0U,     150U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
					{    70U,      70U,      70U,       0U,      70U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
			}
		},
		/*CTRY_10 Actuators' Parameters*/
		{
					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
			{       E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_NA, E_ACT_OK, E_ACT_OK}, /*Actuator Status*/
			{            80U,      80U,      80U,       0U,      80U,      80U}, /*Actuator operational speed*/
			{
					{   255U,     255U,     255U,       0U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,     200U,     200U,       0U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{    50U,      50U,      50U,       0U,      50U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
					{   150U,     150U,     150U,       0U,     150U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
					{    70U,      70U,      70U,       0U,      70U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
			}
		},
		/*CTRY_11 Actuators' Parameters*/
		{
					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
			{       E_ACT_NA, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK}, /*Actuator Status*/
			{             0U,      80U,      80U,      80U,      80U,      80U}, /*Actuator operational speed*/
			{
					{     0U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{     0U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,      50U,      50U,      50U,      50U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
					{     0U,     150U,     150U,     150U,     150U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
					{     0U,      70U,      70U,      70U,      70U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
			}
		},
		/*CTRY_12 Actuators' Parameters*/
		{
					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
			{       E_ACT_NA, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK}, /*Actuator Status*/
			{             0U,      80U,      80U,      80U,      80U,      80U}, /*Actuator operational speed*/
			{
					{     0U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{     0U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,      50U,      50U,      50U,      50U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
					{     0U,     150U,     150U,     150U,     150U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
					{     0U,      70U,      70U,      70U,      70U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
			}
		},

		/*CTRY_13 Actuators' Parameters*/
		{
					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
			{       E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK}, /*Actuator Status*/
			{            80U,      80U,      80U,      80U,      80U,      80U}, /*Actuator operational speed*/
			{
					{   255U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{    50U,      50U,      50U,      50U,      50U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
					{   150U,     150U,     150U,     150U,     150U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
					{    70U,      70U,      70U,      70U,      70U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
			}
		},
		/*CTRY_14 Actuators' Parameters*/
		{
					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
			{       E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK}, /*Actuator Status*/
			{            80U,      80U,      80U,      80U,      80U,      80U}, /*Actuator operational speed*/
			{
					{   255U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{    50U,      50U,      50U,      50U,      50U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
					{   150U,     150U,     150U,     150U,     150U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
					{    70U,      70U,      70U,      70U,      70U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
			}
		},
		/*CTRY_15 Actuators' Parameters*/
		{
					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
			{       E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK}, /*Actuator Status*/
			{            80U,      80U,      80U,      80U,      80U,      80U}, /*Actuator operational speed*/
			{
					{   255U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{    50U,      50U,      50U,      50U,      50U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
					{   150U,     150U,     150U,     150U,     150U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
					{    70U,      70U,      70U,      70U,      70U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
			}
		},
		/*CTRY_16 Actuators' Parameters*/
		{
					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
			{       E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK}, /*Actuator Status*/
			{            80U,      80U,      80U,      80U,      80U,      80U}, /*Actuator operational speed*/
			{
					{   255U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{    50U,      50U,      50U,      50U,      50U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
					{   150U,     150U,     150U,     150U,     150U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
					{    70U,      70U,      70U,      70U,      70U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
			}
		},
		/*CTRY_17 Actuators' Parameters*/
		{
					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
			{       E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK}, /*Actuator Status*/
			{            80U,      80U,      80U,      80U,      80U,      80U}, /*Actuator operational speed*/
			{
					{   255U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{    50U,      50U,      50U,      50U,      50U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
					{   150U,     150U,     150U,     150U,     150U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
					{    70U,      70U,      70U,      70U,      70U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
			}
		},
		/*CTRY_18 Actuators' Parameters*/
		{
					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
			{       E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK}, /*Actuator Status*/
			{            80U,      80U,      80U,      80U,      80U,      80U}, /*Actuator operational speed*/
			{
					{   255U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{    50U,      50U,      50U,      50U,      50U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
					{   150U,     150U,     150U,     150U,     150U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
					{    70U,      70U,      70U,      70U,      70U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
			}
		},
		/*CTRY_19 Actuators' Parameters*/
		{
					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
			{       E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK}, /*Actuator Status*/
			{            80U,      80U,      80U,      80U,      80U,      80U}, /*Actuator operational speed*/
			{
					{   255U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{    50U,      50U,      50U,      50U,      50U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
					{   150U,     150U,     150U,     150U,     150U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
					{    70U,      70U,      70U,      70U,      70U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
			}
		},
		/*CTRY_20 Actuators' Parameters*/
		{
					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
			{       E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK}, /*Actuator Status*/
			{            80U,      80U,      80U,      80U,      80U,      80U}, /*Actuator operational speed*/
			{
					{   255U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{    50U,      50U,      50U,      50U,      50U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
					{   150U,     150U,     150U,     150U,     150U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
					{    70U,      70U,      70U,      70U,      70U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
			}
		}

};

//st_CtryActParams SysCtryActParamTable[CTRY_MAX_NUM]=
//{
//		/*CTRY_01 Actuators' Parameters*/
//		{
//					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
//			{       E_ACT_OK, E_ACT_NA, E_ACT_OK, E_ACT_OK, E_ACT_NA, E_ACT_OK}, /*Actuator Status*/
//			{            80U,       0U,      80U,      80U,      0U,      80U}, /*Actuator operational speed*/
//			{
//					{   255U,       0U,     255U,     255U,       0U,     255U}, /*Actuator current upper limit in OPER mode*/
//					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
//					{   200U,       0U,     200U,     200U,       0U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
//					{    50U,       0U,      50U,      50U,       0U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
//					{   150U,       0U,     150U,     150U,       0U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
//					{    70U,       0U,      70U,      70U,       0U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
//			}
//		},
//		/*CTRY_02 Actuators' Parameters*/
//		{
//					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
//			{       E_ACT_OK, E_ACT_NA, E_ACT_OK, E_ACT_OK, E_ACT_NA, E_ACT_OK}, /*Actuator Status*/
//			{            80U,       0U,      80U,      80U,      0U,      80U}, /*Actuator operational speed*/
//			{
//					{   255U,       0U,     255U,     255U,       0U,     255U}, /*Actuator current upper limit in OPER mode*/
//					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
//					{   200U,       0U,     200U,     200U,       0U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
//					{    50U,       0U,      50U,      50U,       0U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
//					{   150U,       0U,     150U,     150U,       0U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
//					{    70U,       0U,      70U,      70U,       0U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
//			}
//		},
//		/*CTRY_03 Actuators' Parameters*/
//		{
//					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
//			{       E_ACT_NA, E_ACT_NA, E_ACT_OK, E_ACT_NA, E_ACT_NA, E_ACT_OK}, /*Actuator Status*/
//			{             0U,       0U,      80U,       0U,       0U,      80U}, /*Actuator operational speed*/
//			{
//					{     0U,       0U,     255U,       0U,       0U,     255U}, /*Actuator current upper limit in OPER mode*/
//					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
//					{     0U,       0U,     200U,       0U,       0U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
//					{     0U,       0U,      50U,       0U,       0U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
//					{     0U,       0U,     150U,       0U,       0U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
//					{     0U,       0U,      70U,       0U,       0U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
//			}
//		},
//		/*CTRY_04 Actuators' Parameters*/
//		{
//					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
//			{       E_ACT_NA, E_ACT_NA, E_ACT_OK, E_ACT_NA, E_ACT_NA, E_ACT_OK}, /*Actuator Status*/
//			{             0U,       0U,      80U,       0U,       0U,      80U}, /*Actuator operational speed*/
//			{
//					{     0U,       0U,     255U,       0U,       0U,     255U}, /*Actuator current upper limit in OPER mode*/
//					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
//					{     0U,       0U,     200U,       0U,       0U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
//					{     0U,       0U,      50U,       0U,       0U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
//					{     0U,       0U,     150U,       0U,       0U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
//					{     0U,       0U,      70U,       0U,       0U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
//			}
//		},
//		/*CTRY_05 Actuators' Parameters*/
//		{
//					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
//			{       E_ACT_OK, E_ACT_NA, E_ACT_NA, E_ACT_OK, E_ACT_NA, E_ACT_OK}, /*Actuator Status*/
//			{            80U,       0U,       0U,      80U,       0U,      80U}, /*Actuator operational speed*/
//			{
//					{   255U,       0U,       0U,     255U,       0U,     255U}, /*Actuator current upper limit in OPER mode*/
//					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
//					{   200U,       0U,       0U,     200U,       0U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
//					{    50U,       0U,       0U,      50U,       0U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
//					{   150U,       0U,       0U,     150U,       0U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
//					{    70U,       0U,       0U,      70U,       0U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
//			}
//		},
//		/*CTRY_06 Actuators' Parameters*/
//		{
//					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
//			{       E_ACT_OK, E_ACT_NA, E_ACT_NA, E_ACT_OK, E_ACT_NA, E_ACT_OK}, /*Actuator Status*/
//			{            80U,       0U,       0U,      80U,       0U,      80U}, /*Actuator operational speed*/
//			{
//					{   255U,       0U,       0U,     255U,       0U,     255U}, /*Actuator current upper limit in OPER mode*/
//					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
//					{   200U,       0U,       0U,     200U,       0U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
//					{    50U,       0U,       0U,      50U,       0U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
//					{   150U,       0U,       0U,     150U,       0U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
//					{    70U,       0U,       0U,      70U,       0U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
//			}
//		},
//		/*CTRY_7 Actuators' Parameters*/
//		{
//					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
//			{       E_ACT_OK, E_ACT_NA, E_ACT_OK, E_ACT_OK, E_ACT_NA, E_ACT_OK}, /*Actuator Status*/
//			{            0U,      80U,      80U,      80U,       0U,      80U}, /*Actuator operational speed*/
//			{
//					{     0U,     255U,     255U,     255U,       0U,     255U}, /*Actuator current upper limit in OPER mode*/
//					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
//					{     0U,     200U,     200U,     200U,       0U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
//					{     0U,      50U,      50U,      50U,       0U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
//					{     0U,     150U,     150U,     150U,       0U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
//					{     0U,      70U,      70U,      70U,       0U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
//			}
//		},
//		/*CTRY_8 Actuators' Parameters*/
//		{
//					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
//			{       E_ACT_OK, E_ACT_NA, E_ACT_OK, E_ACT_OK, E_ACT_NA, E_ACT_OK}, /*Actuator Status*/
//			{            0U,      80U,      80U,      80U,       0U,      80U}, /*Actuator operational speed*/
//			{
//					{     0U,     255U,     255U,     255U,       0U,     255U}, /*Actuator current upper limit in OPER mode*/
//					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
//					{     0U,     200U,     200U,     200U,       0U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
//					{     0U,      50U,      50U,      50U,       0U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
//					{     0U,     150U,     150U,     150U,       0U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
//					{     0U,      70U,      70U,      70U,       0U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
//			}
//		},
//		/*CTRY_09 Actuators' Parameters*/
//		{
//					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
//			{       E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_NA, E_ACT_OK, E_ACT_OK}, /*Actuator Status*/
//			{            80U,      80U,      80U,       0U,      80U,      80U}, /*Actuator operational speed*/
//			{
//					{   255U,     255U,     255U,       0U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
//					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
//					{   200U,     200U,     200U,       0U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
//					{    50U,      50U,      50U,       0U,      50U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
//					{   150U,     150U,     150U,       0U,     150U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
//					{    70U,      70U,      70U,       0U,      70U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
//			}
//		},
//		/*CTRY_10 Actuators' Parameters*/
//		{
//					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
//			{       E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_NA, E_ACT_OK, E_ACT_OK}, /*Actuator Status*/
//			{            80U,      80U,      80U,       0U,      80U,      80U}, /*Actuator operational speed*/
//			{
//					{   255U,     255U,     255U,       0U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
//					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
//					{   200U,     200U,     200U,       0U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
//					{    50U,      50U,      50U,       0U,      50U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
//					{   150U,     150U,     150U,       0U,     150U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
//					{    70U,      70U,      70U,       0U,      70U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
//			}
//		},
//		/*CTRY_11 Actuators' Parameters*/
//		{
//					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
//			{       E_ACT_NA, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK}, /*Actuator Status*/
//			{             0U,      80U,      80U,      80U,      80U,      80U}, /*Actuator operational speed*/
//			{
//					{     0U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
//					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
//					{     0U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
//					{     0U,      50U,      50U,      50U,      50U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
//					{     0U,     150U,     150U,     150U,     150U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
//					{     0U,      70U,      70U,      70U,      70U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
//			}
//		},
//		/*CTRY_12 Actuators' Parameters*/
//		{
//					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
//			{       E_ACT_NA, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK}, /*Actuator Status*/
//			{             0U,      80U,      80U,      80U,      80U,      80U}, /*Actuator operational speed*/
//			{
//					{     0U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
//					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
//					{     0U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
//					{     0U,      50U,      50U,      50U,      50U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
//					{     0U,     150U,     150U,     150U,     150U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
//					{     0U,      70U,      70U,      70U,      70U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
//			}
//		},
//
//		/*CTRY_13 Actuators' Parameters*/
//		{
//					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
//			{       E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK}, /*Actuator Status*/
//			{            80U,      80U,      80U,      80U,      80U,      80U}, /*Actuator operational speed*/
//			{
//					{   255U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
//					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
//					{   200U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
//					{    50U,      50U,      50U,      50U,      50U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
//					{   150U,     150U,     150U,     150U,     150U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
//					{    70U,      70U,      70U,      70U,      70U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
//			}
//		},
//		/*CTRY_14 Actuators' Parameters*/
//		{
//					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
//			{       E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK}, /*Actuator Status*/
//			{            80U,      80U,      80U,      80U,      80U,      80U}, /*Actuator operational speed*/
//			{
//					{   255U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
//					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
//					{   200U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
//					{    50U,      50U,      50U,      50U,      50U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
//					{   150U,     150U,     150U,     150U,     150U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
//					{    70U,      70U,      70U,      70U,      70U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
//			}
//		},
//		/*CTRY_15 Actuators' Parameters*/
//		{
//					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
//			{       E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK}, /*Actuator Status*/
//			{            80U,      80U,      80U,      80U,      80U,      80U}, /*Actuator operational speed*/
//			{
//					{   255U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
//					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
//					{   200U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
//					{    50U,      50U,      50U,      50U,      50U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
//					{   150U,     150U,     150U,     150U,     150U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
//					{    70U,      70U,      70U,      70U,      70U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
//			}
//		},
//		/*CTRY_16 Actuators' Parameters*/
//		{
//					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
//			{       E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK}, /*Actuator Status*/
//			{            80U,      80U,      80U,      80U,      80U,      80U}, /*Actuator operational speed*/
//			{
//					{   255U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
//					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
//					{   200U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
//					{    50U,      50U,      50U,      50U,      50U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
//					{   150U,     150U,     150U,     150U,     150U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
//					{    70U,      70U,      70U,      70U,      70U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
//			}
//		},
//		/*CTRY_17 Actuators' Parameters*/
//		{
//					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
//			{       E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK}, /*Actuator Status*/
//			{            80U,      80U,      80U,      80U,      80U,      80U}, /*Actuator operational speed*/
//			{
//					{   255U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
//					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
//					{   200U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
//					{    50U,      50U,      50U,      50U,      50U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
//					{   150U,     150U,     150U,     150U,     150U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
//					{    70U,      70U,      70U,      70U,      70U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
//			}
//		},
//		/*CTRY_18 Actuators' Parameters*/
//		{
//					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
//			{       E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK}, /*Actuator Status*/
//			{            80U,      80U,      80U,      80U,      80U,      80U}, /*Actuator operational speed*/
//			{
//					{   255U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
//					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
//					{   200U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
//					{    50U,      50U,      50U,      50U,      50U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
//					{   150U,     150U,     150U,     150U,     150U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
//					{    70U,      70U,      70U,      70U,      70U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
//			}
//		},
//		/*CTRY_19 Actuators' Parameters*/
//		{
//					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
//			{       E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK}, /*Actuator Status*/
//			{            80U,      80U,      80U,      80U,      80U,      80U}, /*Actuator operational speed*/
//			{
//					{   255U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
//					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
//					{   200U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
//					{    50U,      50U,      50U,      50U,      50U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
//					{   150U,     150U,     150U,     150U,     150U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
//					{    70U,      70U,      70U,      70U,      70U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
//			}
//		},
//		/*CTRY_20 Actuators' Parameters*/
//		{
//					/*Act_1     Act_2     Act_3     Act_4*    Act_5*    Act_6*/
//			{       E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK, E_ACT_OK}, /*Actuator Status*/
//			{            80U,      80U,      80U,      80U,      80U,      80U}, /*Actuator operational speed*/
//			{
//					{   255U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
//					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
//					{   200U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
//					{    50U,      50U,      50U,      50U,      50U,      50U}, /*Actuator vibration power lower limit in OPER mode*/
//					{   150U,     150U,     150U,     150U,     150U,     150U}, /*Actuator vibration frequency upper limit in OPER mode*/
//					{    70U,      70U,      70U,      70U,      70U,      70U}  /*Actuator vibration frequency lower limit in OPER mode*/
//			}
//		}
//
//};


#endif /*_ACTUATOR_CTRL_H_*/
/********************************************************************************/






