/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : cbit_ctry.h
** Author    : Leonid Savchenko
** Revision  : 1.0
** Updated   : 19-10-20
**
** Description: Header for cbit_ctry.c file
**
*/


/* Revision Log:
**
** Rev 1.0  : 19-10-20 -- Original version created.
**
**************************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef CBIT_CTRY_H
#define CBIT_CTRY_H

/* -----Includes -------------------------------------------------------------------*/
#include <gipsy_hal_def.h>
/* -----Definitions ----------------------------------------------------------------*/


/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/


/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/
void Perform_CBIT(void);
#endif /* CBIT_CTRY_H */
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
