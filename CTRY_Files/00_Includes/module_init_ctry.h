/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : module_init_ctry.h
** Author    : Leonid Savchenko
** Revision  : 1.1
** Updated   : 11-07-2021
**
** Description: Header for module_init_ctry.c file
**
*/


/* Revision Log:
**
** Rev 1.0  : 19-10-2020 -- Original version created.
** Rev 1.1  : 11-07-2021 -- Added doxygen comments
**************************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef MODULE_INIT_CTRY_H
#define MODULE_INIT_CTRY_H

/* -----Includes -------------------------------------------------------------------*/
#include <gipsy_hal_def.h>
/* -----Definitions ----------------------------------------------------------------*/
#define PBIT_FLT_MDRV_BASE          (uint16_t)0x0001U
#define PBIT_FLT_CURR_CONSUMPTION   (uint16_t)0x0040U
#define PBIT_FLT_ACCELEROMETER      (uint16_t)0x0080U
#define PBIT_FLT_POWER_IN           (uint16_t)0x0100U
#define PBIT_FLT_POWER_5V           (uint16_t)0x0200U
#define PBIT_FLT_TEMP_SENSORS       (uint16_t)0x0400U
#define PBIT_FLT_POW_SWITCH         (uint16_t)0x0800U
#define PBIT_FLT_CTRY_ID            (uint16_t)0x1000U
#define PBIT_FLT_CAN_BUS            (uint16_t)0x2000U


#define STATUS_OK    0U
#define STATUS_FL    1U
/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/
/*! Enumerator for CTRY's mode*/
typedef enum
{
	E_INIT_MODE = 0U, 	/*!<Val:0 -  Initialization Mode 	*/
	E_STBY_MODE,		/*!<Val:1 -  Standby Mode 			*/
	E_SAFE_MODE			/*!<Val:2 -  Safe Mode 				*/
}e_ModuleMode;

/*! Structure for CTRY's general parameters*/
typedef struct
{
	e_ModuleMode  ModuleMode;		/*!< Specifies CTRY's mode (Range: 0 - 2 | Unit: None)						*/
	uint8_t   Pbit_Status;			/*!< Specifies PBIT status (Range: 0 - 1 | Unit: None)						*/
	uint8_t   Cbit_Status;			/*!< Specifies CBIT status (Range: 0 - 1 | Unit: None)						*/
	uint16_t  Ctry_Failure_Desc; 	/*!< Specifies CTRY's failure descriptor (Range: var type | Unit: None)		*/
	uint16_t  Pbit_Failure_Desc;	/*!< Specifies CTRY's PBIT failure descriptor (Range: var type | Unit: None)*/
	uint8_t   Flt_Ch_C_Status;		/*!< Specifies CTRY's failure descriptor (Range: 0 - 1 | Unit: None)		*/
}ModuleParameters_st;


/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/
extern ModuleParameters_st g_ModuleParams;
/* -----Function prototypes --------------------------------------------------------*/



void CTRY_Module_Init(void);
e_ModuleMode Get_Module_Mode(void);
#endif /* MODULE_INIT_CTRY_H */
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
