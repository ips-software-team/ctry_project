/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : can_protocol_ctry.h
** Author    : Leonid Savchenko
** Revision  : 1.0
** Updated   : 22-03-2020
**
** Description: Header for can_protocol_ctry.c file
** 		        This file contains definition of CAN BUS interface protocol used by
** 		        CTRY unit.
*/


/* Revision Log:
**
** Rev 1.0  : 22-03-2020 -- Original version created.
**
**************************************************************************************
*/
#ifndef CAN_PROTOCOL_CTRY_H
#define CAN_PROTOCOL_CTRY_H


/* -----Includes -------------------------------------------------------------------*/
#include <gipsy_hal_def.h>
/* -----Definitions ----------------------------------------------------------------*/

/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/
void HAL_CAN_RxFifo0MsgPendingCallback(void);
void CAN_Message_Transmit(uint8_t message_type);

#endif /*CAN_PROTOCOL_CTRY_H*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/






