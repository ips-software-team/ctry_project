/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : id_conf_ctry.h
** Author    : Leonid Savchenko
** Revision  : 1.1
** Updated   : 11-07-2021
**
** Description: Header for id_conf_ctry.c file
**
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1  : 11-07-2021 -- Added doxygen comments
**************************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef ID_CONF_CTRY_H
#define ID_CONF_CTRY_H


/* -----Includes -------------------------------------------------------------------*/
#include <gipsy_hal_def.h>
/* -----Definitions ----------------------------------------------------------------*/

/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/

/*! Enumerator for ID test status*/
typedef enum
{
	E_CTRY_ID_OK = 0U,      /*!<Val:0 -  ID test passed 	*/
	E_CTRY_ID_FAIL,         /*!<Val:1 -  ID test failed	 	*/
	E_CTRY_ID_BIT_FAIL		/*!<Val:1 -  ID BIT failed	 	*/
}eCTRY_ID_Status;
/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/
eCTRY_ID_Status HI8429_Init(void);
uint8_t Get_CTRY_ID(void);

#endif /*ID_CONF_CTRY_H*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
