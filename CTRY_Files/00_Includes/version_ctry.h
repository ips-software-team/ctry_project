/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : version_ctry.h
** Author    : Leonid Savchenko
** Revision  : 1.3
** Updated   : 28-05-2021
**
** Description: This file contains definition of SW version of CTRY unit
**
** Notes: This file represents a versions ID.
** The version is formatted as Y.X where according  to AIF convention
**
** Y is the major_version(00 - 99).
** X is the minor_version (00 - 99).
*/

/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1  : 21-09-2021 -- Updated for release 1_05
** Rev 1.2  : 01-10-2021 -- Updated for release 1_06
** Rev 1.3  : 28-05-2023 -- Updated for release 1_07
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _VERSION_CTRY_H_
#define _VERSION_CTRY_H_

/* -----Includes -------------------------------------------------------------------*/

/* -----Definitions ----------------------------------------------------------------*/


 #define _VER_MAJOR_ 	1U
 #define _VER_MINOR_    7U

 #define DEVICE	        CTRY
 #define DEVICE_NAME    STR_DEVICE_NAME(DEVICE)

/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/

#endif /*_VERSION_CTRY_H_*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/

