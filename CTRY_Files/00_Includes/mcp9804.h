
/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : mcp9804.h
** Author    : Leonid Savchenko
** Revision  : 1.5
** Updated   : 11-07-2021
**
** Description: Header for mcp9804.c file
**
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1  : 15-01-2020 -- TEMPERATURE_ABS_MIN changed to -35, aded define ALL_TEMP_SENSORS_FAIL
** Rev 1.2  : 21-10-2020 -- added Temperature_Sensor_Check_ID()
** Rev 1.3  : 20-11-2020 -- Static analysis update,removed defines
** Rev 1.4  : 29-06-2021 -- Header updated
** Rev 1.5  : 11-07-2021 -- Added doxygen comments
**************************************************************************************
*/
/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef MCP9804_H
#define MCP9804_H

/* -----Includes -------------------------------------------------------------------*/

#include <gipsy_hal_def.h>
#include "gipsy_hal_i2c_ctry.h"
#include "temperature_control.h"
/* -----Definitions ----------------------------------------------------------------*/

/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/

/*! Enumerator for CTRY's boards types*/
typedef enum CTRY_Boards
{
	E_CPU_BOARD  = 0U,     	/*!<Val:0 -  CPU Board 				*/
	E_DRV_BOARD,			/*!<Val:1 -  Driver Board 			*/
	E_CTRY_NUM_OF_BOARDS	/*!<Val:2 -  Number of CTRY's boards*/
}e_CTRY_Boards;

/*! Enumerator for temperature sensor ID test status*/
typedef enum
{
	E_TEMP_SENS_ID_OK    = 0U,		/*!<Val:0 -  Temperature sensor ID test passed */
	E_TEMP_SENS_ID_FAIL				/*!<Val:1 -  Temperature sensor ID test failed */
}e_Temp_Sens_ID_Status;

/*! Enumerator for temperature sensor internal registers addresses*/
typedef enum MCP9804_Register
{
	REG_CONFIG = 			0x01U,	/*See MCP9804 datasheet section 5.1*/
	REG_TUPPER = 			0x02U,
	REG_TLOWER = 			0x03U,
	REG_TCRIT = 			0x04U,
	REG_TA =	 			0x05U,
	REG_MANUFACTURER_ID = 	0x06U,
	REG_DEVICE_ID = 		0x07U,
	REG_RESOLUTION = 		0x08U
}e_MCP9804_Register;


/*! Structure for temperature sensors HW parameters : I2C Bus handler, sensors' addresses*/
typedef struct
{
	I2C_HandleTypeDef *hi2cSensBus;             /*!< Specifies address of I2C bus handler structure (Range: var type | Unit: None)	*/
	uint8_t TempSensAddr[MAX_SENSORS_ON_BOARD]; /*!< Specifies temperature sensor's address (Range: var type | Unit: None)			*/
}BoardTempSensAddr_st;
/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/


/* -----Function prototypes --------------------------------------------------------*/

uint8_t Temperaure_Sensors_Read(BoardTempSens_t *board_temp_sens);
e_Temp_Sens_ID_Status Temperature_Sensor_Check_ID(uint8_t board_type, uint8_t sensor_num, uint16_t *sens_id);
uint8_t Temperature_Sensors_Config(void);


#endif /*_MCP9804_H_*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
