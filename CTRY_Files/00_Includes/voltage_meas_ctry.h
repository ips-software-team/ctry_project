/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : voltage_meas_ctry.h
** Author    : Leonid Savchenko
** Revision  : 1.4
** Updated   : 11-07-2021
**
** Description: Header for voltage_meas_ctry.c file
**
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1  : 12-02-2020 -- Updated Test_OnBoard_Voltage()
** Rev 1.2  : 19-02-2020 -- Updated Test_OnBoard_Voltage() and st_VoltageMeasParameters
** Rev 1.3  : 09-10-2020 -- Updated e_VoltageSourceType enumerator
** Rev 1.4  : 11-07-2021 -- Added doxygen comments
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef VOLTAGE_MEAS_CTRY_H
#define VOLTAGE_MEAS_CTRY_H

/* -----Includes -------------------------------------------------------------------*/

/* -----Definitions ----------------------------------------------------------------*/

/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/

/*! Enumerator for voltage measurement status*/
typedef enum
{
	E_VOLT_MEAS_OK = 0U, 	/*!<Val:0 -  Voltage measurement OK 	*/
	E_VOLT_MEAS_FL			/*!<Val:1 -  Voltage measurement Failed */
}e_VoltageMeasStatus;

/*! Enumerator for voltage test status*/
typedef enum
{
	E_VOLT_TEST_OK = 0U,	/*!<Val:0 -  Voltage test OK 		*/
	E_VOLT_TEST_FL			/*!<Val:1 -  Voltage test Failed 	*/
}e_VoltageTestStatus;

/*! Enumerator for voltage sources/tests types*/
typedef enum
{
	E_VOLT_5V        = 0U,  /*!<Val:0 -  Voltage test 5V 				*/
	E_VOLT_14_8V_IN,		/*!<Val:1 -  Voltage test 14.8V IN 			*/
	E_VOLT_14_8_MDRV_ON,	/*!<Val:2 -  Voltage test 14.8V MDRV-ON 	*/
	E_VOLT_14_8_MDRV_OFF,	/*!<Val:3 -  Voltage test 14.8V MDRV-OFF 	*/
	E_VOLT_MAX_TYPES		/*!<Val:4 -  Number of voltage test types 	*/
}e_VoltageSourceType;

/*! Structure for voltage test parameters*/
typedef struct
{
	float    VoltageValue;                  /*!< Specifies measured voltage value (Range: var type | Unit: Volt)	*/
	e_VoltageMeasStatus  MeasurementStatus; /*!< Specifies voltage measurement status (Range: 0 - 1 | Unit: None)	*/
	e_VoltageTestStatus  VoltageTestResult; /*!< Specifies voltage test result (Range: 0 - 1 | Unit: None)			*/
	const float    VoltageLimitMax;			/*!< Specifies voltage maximal limit (Range: var type | Unit: Volt)		*/
	const float    VoltageLimitMin;			/*!< Specifies voltage minimal limit (Range: var type | Unit: Volt)		*/
}st_VoltageMeasParameters;

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/
e_VoltageTestStatus Test_OnBoard_Voltage(e_VoltageSourceType voltage_type);

#endif /* VOLTAGE_MEAS_CTRY_H */
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
