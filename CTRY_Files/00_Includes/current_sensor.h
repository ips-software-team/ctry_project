/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : current_sensor.h
** Author    : Leonid Savchenko
** Revision  : 1.3
** Updated   : 11-07-2021
**
** Description: Header for current_sensor.c file
**
*/


/* Revision Log:
**
** Rev 1.0  : 05-02-2020 -- Original version created.
** Rev 1.1  : 22-03-2020 -- Added extern variable.
** Rev 1.2  : 22-10-2020 -- Updated due to verification remarks
** Rev 1.3  : 11-07-2021 -- Added doxygen comments
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef CURRENT_SENSOR_H
#define CURRENT_SENSOR_H

/* -----Includes -------------------------------------------------------------------*/
#include <gipsy_hal_def.h>
/* -----Definitions ----------------------------------------------------------------*/

/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/
/*! Enumerator for current measurement status*/
typedef enum
{
	E_CURR_MEAS_OK = 0U,	/*!<Val:0 -  Current measurement OK 	*/
	E_CURR_MEAS_FL          /*!<Val:1 -  Current measurement Failed */
}e_CurrentMeasStatus;

/*! Enumerator for current test status*/
typedef enum
{
	E_CURR_TEST_OK = 0U,      /*!<Val:0 -  Current test OK 		*/
	E_CURR_TEST_FL            /*!<Val:1 -  Current test Failed 	*/
}e_CurrentTestStatus;

/*! Structure for current measurement parameters*/
typedef struct
{
	uint8_t  CurrentValue;     				/*!< Specifies measured current value (Range: var type | Unit: 0.1 Amperes)	*/
	e_CurrentMeasStatus  MeasurementStatus; /*!< Specifies current measurement status (Range: 0 - 1 | Unit: None)		*/
}st_CurrSensData;


/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/
extern st_CurrSensData g_MeasuredCurrent;

/* -----Function prototypes --------------------------------------------------------*/
void Measure_Current_Consumption(void);
uint8_t Test_Current_Consumption(uint8_t Curr_Limit_High, uint8_t Curr_Limit_Low);
#endif /* CURRENT_SENSOR_H */
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
