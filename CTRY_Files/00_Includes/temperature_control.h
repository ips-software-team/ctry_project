/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : temperature_control.h
** Author    : Leonid Savchenko
** Revision  : 1.3
** Updated   : 21-09-2021
**
** Description: Header for temperature_control.c file
**
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1  : 01-10-2020 -- Removed defines to .c file
** Rev 1.2  : 11-07-2021 -- Added doxygen comments
** Rev 1.3  : 22-09-2021 -- Updated E_BOARD_TEMP_FL description
**************************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef _TEMPERATURE_CONTROL_H_
#define _TEMPERATURE_CONTROL_H_

/* -----Includes -------------------------------------------------------------------*/
#include <gipsy_hal_def.h>
/* -----Definitions ----------------------------------------------------------------*/

#define MAX_SENSORS_ON_BOARD  4U /*!< Number of temperature sensors on board*/

#define HEATER_ON          1U /*!< Heater circuit ON state*/
#define HEATER_OFF         0U /*!< Heater circuit OFF state*/

#define TEMP_ERR_VALUE  0xE3U /*!< Temperature error value definition is: -99 Celsius Deg*/
/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/

/*! Structure for CTRY's Board temperature parameters*/
typedef struct
{
	uint8_t  Temperatue[MAX_SENSORS_ON_BOARD];  /*!< Specifies Temperature Sensor's measured temperature value (Range: var type(MSB defines value sign) | Unit: Celsius Deg)*/
	uint16_t SensorID[MAX_SENSORS_ON_BOARD];	/*!< Specifies Temperature Sensor's ID value (Range: var type | Unit: None)		*/
	uint8_t  BoardTemperatureStatus;			/*!< Specifies board's temperature status (Range: 0 - 2 | Unit: None)	*/
	uint8_t  BoardHeaterState;					/*!< Specifies heater state (Range: 0 - 1 | Unit: None)					*/
	uint8_t  BoardSensorsStatus;				/*!< Specifies sensors status (Range: 0 - 0x0F | Unit: None)			*/
}BoardTempSens_t;

/*! Enumerator of CTRY board's temperature status description */
typedef enum
{
	E_BOARD_TEMP_OK     = 0U,   /*!< Val:0 - Board Temperature is OK				*/
	E_BOARD_UNDER_TEMP,			/*!< Val:1 - Board Under-Temperature event			*/
	E_BOARD_OVER_TEMP,  		/*!< Val:2 - Board Over-Temperature event			*/
	E_BOARD_TEMP_FL     		/*!< Val:3 - More than 3 board sensors are fail		*/
}e_BoardTempStatus;
/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/

void CTRY_Temperature_Management(void);
void CTRY_Control_Heaters(uint8_t board_type, uint8_t heater_state);



#endif /*_TEMPERATURE_CONTROL_H_*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
