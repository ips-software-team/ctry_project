/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC/CTRY
**
** Filename  : flash_integrity_test.h
** Author    : Leonid Savchenko
** Revision  : 1.1
** Updated   : 11-07-2021
**
** Description: Header file for flash_integrity_test.c
**

*/

/* Revision Log:
**
** Rev 1.0  : 21-06-2020 -- Original version created.
** Rev 1.1  : 11-07-2021 -- Added doxygen comments
**************************************************************************************
*/

#ifndef FLASH_INEGRITY_TEST
#define FLASH_INEGRITY_TEST

/* -----Includes -------------------------------------------------------------------*/
#include <gipsy_hal_def.h>
/* -----Definitions ----------------------------------------------------------------*/

/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/
/*! Enumerator for Flash CRC test status*/
typedef enum
{
	E_CRC_TEST_OK = 0U,       /*!<Val:0 -  Flash CRC test OK 		*/
	E_CRC_TEST_FAIL           /*!<Val:1 -  Flash CRC test Failed 	*/
}eCRC_Test_Result;
/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/
eCRC_Test_Result Flash_Integrity_Test(void);
#endif /*FLASH_INEGRITY_TEST*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/

