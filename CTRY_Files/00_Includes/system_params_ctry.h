/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : system_params_ctry.h
** Author    : Leonid Savchenko
** Revision  : 1.6
** Updated   : 28-05-2023
**
** Description: Header for system_params_ctry.c file
**
*/


/* Revision Log:
**
** Rev 1.0  : 16-03-2020 -- Original version created.
** Rev 1.1  : 21-10-2020 -- Added defines
** Rev 1.2  : 22-11-2020 -- Updated due to verification remarks
** Rev 1.3  : 01-05-2021 -- added Set_Actuator_Limits()
** Rev 1.4  : 22-06-2021 -- Updated actuators params table and changed procedures
** Rev 1.5  : 11-07-2021 -- Added doxygen comments
** Rev 1.6  : 28-05-2023 -- Added limit type defines
**************************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef SYSTEM_PARAMS_CTRY_H
#define SYSTEM_PARAMS_CTRY_H

/* -----Includes -------------------------------------------------------------------*/
#include <gipsy_hal_def.h>
/* -----Definitions ----------------------------------------------------------------*/
#define CTRY_MAX_NUM 20U

#define ALL_ACT_FL           0x3FU
#define ALL_ACT_OK           0x00U

#define ACT_LIM_OPER_MODE    0x00U
#define ACT_LIM_IBIT_MODE    0x80U
/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/
/*! Enumerator for CTRY's actuators number*/
typedef enum
{
	E_ACT1 = 0U,		/*!<Val:0 -  Actuator #1 */
	E_ACT2,             /*!<Val:1 -  Actuator #2 */
	E_ACT3,             /*!<Val:2 -  Actuator #3 */
	E_ACT4,             /*!<Val:3 -  Actuator #4 */
	E_ACT5,             /*!<Val:4 -  Actuator #5 */
	E_ACT6,             /*!<Val:5 -  Actuator #6 */
	E_ACT_MAX_NUM       /*!<Val:6 -  Max number of CTRY's actuators */
}e_ActuatorsNum;

/*! Enumerator for actuator's installation status*/
typedef enum
{
	E_ACT_EQ = 0U,	/*!<Val:0 -  Actuator equipped 		*/
	E_ACT_NA		/*!<Val:1 -  Actuator not-equipped 	*/
}e_ActuatorState;

/*! Enumerator for actuator's operational status*/
typedef enum
{
	E_ACT_OK = 0U, 	/*!<Val:0 -  Actuator OK 	*/
	E_ACT_FL		/*!<Val:1 -  Actuator Fail 	*/
}e_ActuatorStatus;

/*! Enumerator for CTRY's operational status*/
typedef enum
{
	E_CTRY_OK = 0U,	/*!<Val:0 -  CTRY OK 			*/
	E_CTRY_FL,		/*!<Val:1 -  CTRY Fail 			*/
	E_CTRY_NA		/*!<Val:2 -  CTRY Not Available */
}e_CtryStatus;

/*! Enumerator for System's table monitoring test status*/
typedef enum
{
	E_MON_TBL_OK = 0U,	/*!<Val:0 -  Table monitoring passed*/
	E_MON_TBL_FL		/*!<Val:1 -  Table monitoring failed*/
}e_MonitTableStatus;

/*! Structure for actuator's limits parameters*/
typedef struct
{
	uint8_t CurrentMax[E_ACT_MAX_NUM];	/*!< Specifies current max limit value (Range: var type | Unit: 0.1 Amperes)*/
	uint8_t CurrentMin[E_ACT_MAX_NUM];	/*!< Specifies current min limit value (Range: var type | Unit: 0.1 Amperes)*/
	uint8_t AccelPowMax[E_ACT_MAX_NUM];	/*!< Specifies vibration max limit value (Range: var type | Unit: G)		*/
	uint8_t AccelPowMin[E_ACT_MAX_NUM];	/*!< Specifies vibration min limit value (Range: var type | Unit: G)		*/
}st_CtryActLimits;

/*! Structure for CTRY's actuators parameters*/
typedef struct
{
	uint8_t CTRY_Eq_Act_Mask;                  	/*!< Specifies equipped actuators mask (Range: var type | Unit: None)*/
	e_ActuatorState  ActState[E_ACT_MAX_NUM];	/*!< Specifies actuators installation state (Range: 0 -1 | Unit: None)*/
	st_CtryActLimits stActLimits;				/*! Specifies structure for actuator's limits parameters (Range: var type | Unit: See struct declaration)*/
}st_SysCtryActParams;

/*! Structure for CTRY's and it's actuators statuses*/
typedef struct
{
	e_CtryStatus      CtryStatus;				/*!< Specifies CTRY's operational status (Range: 0 - 2 | Unit: None)					*/
	e_ActuatorStatus  ActStatus[E_ACT_MAX_NUM]; /*!< Specifies each actuator's operational status (Range: 0 - 1 | Unit: None)			*/
	uint8_t           CtryActuatorsStatus;  	/*!< Specifies CTRY's Actuators' operational status (Range: 0x00 - 0x3F | Unit: None)	*/
}st_SysCtryActStatus;

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/
HAL_StatusTypeDef Get_Actuator_State(uint8_t Ctry_Id, e_ActuatorsNum ActNum, e_ActuatorState *Act_State);
HAL_StatusTypeDef Get_Actuator_CurrLimits(uint8_t Ctry_Id, e_ActuatorsNum ActNum, uint8_t *CurrLim_Max, uint8_t *CurrLim_Min);
HAL_StatusTypeDef Get_Actuator_VibrLimits(uint8_t Ctry_Id, e_ActuatorsNum ActNum, uint8_t *VibrLim_Max, uint8_t *VibrLim_Min);

void Init_Ctrys_Status_Table(void);
HAL_StatusTypeDef Get_Actuator_Status(uint8_t Ctry_Id, e_ActuatorsNum ActNum, e_ActuatorStatus *Act_Status);
HAL_StatusTypeDef Get_CTRY_All_Actuators_Status(uint8_t Ctry_Id, uint8_t *All_Act_Status);
HAL_StatusTypeDef Set_Actuator_Status_FL(uint8_t Ctry_Id, e_ActuatorsNum ActNum);
HAL_StatusTypeDef Set_CTRY_Status(uint8_t Ctry_Id, e_CtryStatus Ctry_Status);
void Set_CTRY_All_Actuators_Status_FL(uint8_t Ctry_Id);
e_CtryStatus Get_CTRY_Status(uint8_t Ctry_Id);
e_MonitTableStatus Monitor_Status_Table(void);

void Set_Actuator_Limits(uint8_t Lim_Mode_Type, e_ActuatorsNum ActNum, uint8_t Vib_Lim_Max, uint8_t Vib_Lim_Min, uint8_t Curr_Lim_Max, uint8_t Curr_Lim_Min);
#endif /*SYSTEM_PARAMS_CTRY_H*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/




