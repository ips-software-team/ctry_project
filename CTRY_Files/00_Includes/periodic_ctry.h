/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : periodic_ctry.h
** Author    : Leonid Savchenko
** Revision  : 1.2
** Updated   : 11-07-2021
**
** Description: Header for periodic_ctry.c file
**
** Notes:

*/

/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1  : 29-06-2021 -- Updated header
** Rev 1.2  : 11-07-2021 -- Added doxygen comments
**************************************************************************************
*/

#ifndef _PERIODIC_CTRY_H_
#define _PERIODIC_CTRY_H_

/* -----Includes -------------------------------------------------------------------*/
#include <gipsy_hal_def.h>
/* -----Definitions ----------------------------------------------------------------*/

/*! Structure for periodic task last execution time*/
typedef struct
{
	uint32_t Sw_Led_Toggle;         /*!< Specifies led toggling last time (Range: var type | Unit: milliseconds)						*/
	uint32_t ActuatorStartTime;		/*!< Specifies actuators activation last time (Range: var type | Unit: milliseconds)				*/
	uint32_t TemperatureManagement; /*!< Specifies temperature management last time (Range: var type | Unit: milliseconds)				*/
	uint32_t VoltageMeasurement; 	/*!< Specifies voltage measurement last time (Range: var type | Unit: milliseconds)					*/
	uint32_t MotOperInput; 			/*!< Specifies MOT_OPER input sampling last time (Range: var type | Unit: milliseconds)				*/
	uint32_t Cbit; 					/*!< Specifies CBIT execution last time (Range: var type | Unit: milliseconds)						*/
	uint32_t CurrentConsumption; 	/*!< Specifies STBY current measurement last time (Range: var type | Unit: milliseconds)			*/
	uint32_t MdrvCommTest; 			/*!< Specifies motor drivers communication test last time (Range: var type | Unit: milliseconds)	*/
	uint32_t Mon_Accel_Curr; 		/*!< Specifies current/vibration OPER measurement last time (Range: var type | Unit: milliseconds)	*/
	uint32_t Status_Tbl_Monitoring; /*!< Specifies status table monitoring last time (Range: var type | Unit: milliseconds)				*/
}st_Task_Last_Execution_Time;



/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/
void Periodic_Tasks_Scheduler(void);
#endif /*_PERIODIC_CTRY_H_*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/

