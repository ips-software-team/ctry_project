/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC,CTRY
**
** Filename  : wdog.h
** Author    : Leonid Savchenko
** Revision  : 1.2
** Updated   : 11-07-2021
**
** Description: Header for wdog.c file.
**
** Notes:
*/

/* Revision Log:
**
** Rev 1.0  : 28-07-2020 -- Original version created.
** Rev 1.1  : 29-06-2021 -- Updated due to verification remarks
** Rev 1.2  : 11-07-2021 -- Added doxygen comments
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef WDOG_H
#define WDOG_H

/* -----Includes -------------------------------------------------------------------*/
#include "wdog.h"
/* -----Definitions ----------------------------------------------------------------*/

/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/
/*! Enumerator for WDOG test status*/
typedef enum
{
	E_WDOG_TEST_PASSED = 0U,		/*!<Val:0 -  WDOD test passed 							*/
	E_WDOG_TEST_FAILED,				/*!<Val:1 -  WDOD test failed 							*/
	E_WDOG_TEST_UNDEF_RST_SOURCE   	/*!<Val:2 -  WDOD test failed(undefined reset source)	*/
}e_WDOG_TestResult;
/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/
void WDOG_Trigger(void);
void WDOG_Perform_Test(void);
e_WDOG_TestResult WDOG_Get_Test_Result(void);
#endif /*_WDOG_H_*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
