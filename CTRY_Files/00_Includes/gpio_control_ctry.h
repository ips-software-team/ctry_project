/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : gpio_control_ctry.h
** Author    : Leonid Savchenko
** Revision  : 1.2
** Updated   : 11-07-2021
**
** Description: Header for gpio_control_ctry.c file
** 		        This file contains function for control and monitoring GPIOs.
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1  : 15-03-2020 -- Added Activate_Fault_Channel_C()
** Rev 1.2  : 11-07-2021 -- Added doxygen comments
**************************************************************************************
*/
#ifndef GPIO_CONTROL_CTRY_H
#define GPIO_CONTROL_CTRY_H





/* -----Includes -------------------------------------------------------------------*/
#include "gipsy_hal_gpio.h"

/* -----Definitions ----------------------------------------------------------------*/

/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/

/*! Enumerator for Fault Channel_C state*/
typedef enum
{
	E_FLT_CH_C_OFF = 0U,	/*!<Val:0 -  Channel_C OFF 		*/
	E_FLT_CH_C_ON			/*!<Val:1 -  Channel_C ON 		*/
}e_Flt_Ch_C_State;
/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/
GPIO_PinState Read_MotOper_Input(void);
void Fault_Channel_C_Control(e_Flt_Ch_C_State ch_c_state);
void Activate_Fault_Channel_C(void);
void Set_Motor_PWR_EN(GPIO_PinState pin_state);
void Manage_OnBoard_Leds(void);
#endif /*GPIO_CONTROL_CTRY_H*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
