/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : mcu_config_ctry.h
** Author    : Leonid Savchenko
** Revision  : 1.1
** Updated   : 07-05-2023
**
** Description: Header for mcu_config_ctry.c file
**
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2020 -- Original version created.
** Rev 1.1  : 07-05-2023 -- Removed uart header file.
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef MCU_CONFIG_CTRY_H
#define MCU_CONFIG_CTRY_H


/* -----Includes -------------------------------------------------------------------*/

#include "gipsy_hal_i2c_ctry.h"
#include <gipsy_hal_tim_pwm_ctry.h>
#include "gipsy_hal_gpio.h"

/* -----Definitions ----------------------------------------------------------------*/

/*Debug Led Output Pins definition*/
#define SW_RUN_LED_PIN	   	GPIO_PIN_1 /*LED_1*/
#define FLT_LED_PIN   	    GPIO_PIN_2 /*LED_2*/
#define LED_PORT	    GPIOC

/*Motor Drivers Pins definition*/
#define MDRV_SPI_CLK_PIN	GPIO_PIN_12
#define MDRV_SPI_MOSI_PIN	GPIO_PIN_14
#define MDRV_SPI_MISO_PIN	GPIO_PIN_13
#define MDRV_SPI_PORT	GPIOE

#define MDRV_SPI_CS1_PIN	GPIO_PIN_2
#define MDRV_SPI_CS2_PIN	GPIO_PIN_3
#define MDRV_SPI_CS3_PIN	GPIO_PIN_4
#define MDRV_SPI_CS4_PIN	GPIO_PIN_5
#define MDRV_SPI_CS5_PIN	GPIO_PIN_6
#define MDRV_SPI_CS6_PIN	GPIO_PIN_7
#define MDRV_SPI_CS_PORT	GPIOD

#define MDRV_WAKE1_PIN     GPIO_PIN_0
#define MDRV_WAKE2_PIN     GPIO_PIN_1
#define MDRV_WAKE3_PIN     GPIO_PIN_2
#define MDRV_WAKE4_PIN     GPIO_PIN_3
#define MDRV_WAKE5_PIN     GPIO_PIN_4
#define MDRV_WAKE6_PIN     GPIO_PIN_5
#define MDRV_WAKE_PORT     GPIOE

#define MDRV_EN1_PIN     GPIO_PIN_6
#define MDRV_EN2_PIN     GPIO_PIN_7
#define MDRV_EN3_PIN     GPIO_PIN_8
#define MDRV_EN4_PIN     GPIO_PIN_9
#define MDRV_EN5_PIN     GPIO_PIN_10
#define MDRV_EN6_PIN     GPIO_PIN_15
#define MDRV_EN_PORT     GPIOE

#define MDRV_DIAG1_PIN    GPIO_PIN_11
#define MDRV_DIAG1_PORT   GPIOD
#define MDRV_DIAG2_PIN    GPIO_PIN_12
#define MDRV_DIAG2_PORT   GPIOD
#define MDRV_DIAG3_PIN    GPIO_PIN_14
#define MDRV_DIAG3_PORT   GPIOB
#define MDRV_DIAG4_PIN    GPIO_PIN_15
#define MDRV_DIAG4_PORT   GPIOB
#define MDRV_DIAG5_PIN    GPIO_PIN_15
#define MDRV_DIAG5_PORT   GPIOA
#define MDRV_DIAG6_PIN    GPIO_PIN_12
#define MDRV_DIAG6_PORT   GPIOC

/*PWM Output Pins definition*/
#define PWMIN1_PIN	   GPIO_PIN_6
#define PWMIN1_PORT	   GPIOC
#define PWMIN2_PIN	   GPIO_PIN_7
#define PWMIN2_PORT	   GPIOC
#define PWMIN3_PIN	   GPIO_PIN_8
#define PWMIN3_PORT	   GPIOC
#define PWMIN4_PIN	   GPIO_PIN_13
#define PWMIN4_PORT	   GPIOD
#define PWMIN5_PIN	   GPIO_PIN_14
#define PWMIN5_PORT	   GPIOD
#define PWMIN6_PIN	   GPIO_PIN_15
#define PWMIN6_PORT	   GPIOD



/*HI894 (Config ID) Pins definition*/
#define CONF_ID_SPI_CLK_PIN    MDRV_SPI_CLK_PIN
#define CONF_ID_SPI_MOSI_PIN   MDRV_SPI_MOSI_PIN
#define CONF_ID_SPI_MISO_PIN   MDRV_SPI_MISO_PIN
#define CONF_ID_SPI_PORT       MDRV_SPI_PORT
#define CONF_ID_SPI_CS_PIN	   GPIO_PIN_8
#define CONF_ID_SPI_CS_PORT	   MDRV_SPI_CS_PORT
#define CONF_ID_SEL0_PIN	   GPIO_PIN_8
#define CONF_ID_SEL0_PORT	   GPIOB
#define CONF_ID_SEL1_PIN	   GPIO_PIN_9
#define CONF_ID_SEL1_PORT	   GPIOB
#define CONF_ID_RESET_PIN	   GPIO_PIN_1
#define CONF_ID_RESET_PORT	   GPIOB

/*Accelerometer Pins definition*/
#define ACCEL_INT1_PIN	   GPIO_PIN_9
#define ACCEL_INT1_PORT	   GPIOD
#define ACCEL_INT2_PIN	   GPIO_PIN_10
#define ACCEL_INT2_PORT	   GPIOD


/*Analog Input Pins definition*/
#define MTR_CUR_LVL_CPU_PIN	   	GPIO_PIN_3

#define V5VDCLVL_PIN	   	    GPIO_PIN_4
#define V14_8VDCLVL_PIN	   	    GPIO_PIN_5
#define V_DRIVER_MONIT_PIN	   	GPIO_PIN_2
#define ADC1_PORT	            GPIOA

/*General Purpose Input Pins definition*/
#define V15V_FEEDBACK_PIN	   	GPIO_PIN_6
#define V15V_FEEDBACK_PORT	    GPIOA

#define MOTOR_OPERATE_PIN	   	GPIO_PIN_1 /*MOTOR_OPERATE_TTL*/
#define MOTOR_OPERATE_PORT	    GPIOA

/*General Purpose Output Pins definition*/
#define MOTOR_PWR_EN_PIN	GPIO_PIN_2
#define MOTOR_PWR_EN_PORT	GPIOB

#define CPU_CON_TRAY_FAULT_PIN	   	GPIO_PIN_5 /*CPU_CON_TRAY_FAULT_TTL*/
#define CPU_CON_TRAY_FAULT_PORT	    GPIOC


#define CPU_CARD_HEATING_ON_PIN	   	GPIO_PIN_12
#define DRV_CARD_HEATING_ON_PIN	   	GPIO_PIN_13 /*DRIVER_HEATING_ON*/
#define CARD_HEATING_ON_PORT	    GPIOB

/*CAN_BUS Pins definition*/
#define TXCAN_1_PIN         GPIO_PIN_1
#define RXCAN_1_PIN         GPIO_PIN_0
#define CAN_PORT            GPIOD
/*Debug UART port definition*/
#define U3_TX_PIN           GPIO_PIN_10
#define U3_RX_PIN           GPIO_PIN_11
#define DEBUG_UART_PORT     GPIOC

/*External Watch-Dog strobe Output Pin definition*/
#define WDI_PIN	   	    GPIO_PIN_4
#define WDI_PORT	        GPIOB

/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/
extern I2C_HandleTypeDef g_HandleI2C_1_TempDrv;
extern I2C_HandleTypeDef g_HandleI2C_2_TempMcu;
extern I2C_HandleTypeDef g_HandleI2C_3_Accel;
extern TIM_HandleTypeDef g_HandleTim3;
extern TIM_HandleTypeDef g_HandleTim4;


#ifdef _DEBUG_
extern UART_HandleTypeDef g_HandleUart3Debug;
#endif


/* -----Function prototypes --------------------------------------------------------*/
void MCU_Periph_Config(void);
HAL_StatusTypeDef MCU_CAN_Interface_Init(void);

#endif /*MCU_CONFIG_CTRY_H*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
