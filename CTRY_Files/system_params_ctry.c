/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : system_params_ctry.c
** Author    : Leonid Savchenko
** Revision  : 1.7
** Updated   : 10-05-2023
**
** Description: Definition of system parameters and their values.
**
*/


/* Revision Log:
**
** Rev 1.0  : 16-03-2020 -- Original version created.
** Rev 1.1  : 19-10-2020 --  Updated Set_CTRY_All_Actuators_Status(),Set_CTRY_Status(), Init_Ctrys_Status_Table()-CTRY init status changed to "OK"
** Rev 1.2  : 20-11-2020 -- Static analysis update
** Rev 1.3  : 22-11-2020 -- Updated due to verification remarks
** Rev 1.4  : 02-12-2020 -- Updated actuators table
** Rev 1.5  : 01-05-2021 -- Updated actuators table, added Set_Actuator_Limits(), changed Set_CTRY_All_Actuators_Status() to Set_CTRY_All_Actuators_Status_FL()
** Rev 1.6  : 22-06-2021 -- Updated actuators params table and changed procedures
** Rev 1.7  : 10-05-2023 -- Added IBIT limits global variables, removed limit type defines
**************************************************************************************
*/


/* Includes ------------------------------------------------------------------*/
#include "system_params_ctry.h"
#include "id_conf_ctry.h"
/* -----Definitions ----------------------------------------------------------------*/
#define FL_CTRY_MAX_NUM        0U
#define FL_ACTUATORS_MAX_NUM   1U


/* -----Macros ---------------------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

static st_SysCtryActStatus SysCtryStatusTable[CTRY_MAX_NUM] = {0U};

static st_SysCtryActParams SysCtryParamTable[CTRY_MAX_NUM] =
{
		/*----------------------CTRY_01 (130H) Actuators' Parameters----------------*/
		{0x2DU, /*Equipped Actuators Mask*/
					/* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_NA, E_ACT_EQ, E_ACT_EQ, E_ACT_NA, E_ACT_EQ}, /*Actuator State*/
			{
					{   255U,       0U,     255U,     255U,       0U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,       0U,     200U,     200U,       0U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,       0U,       0U,       0U,       0U,       0U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_02  (131H) Actuators' Parameters----------------*/
		{0x2DU, /*Equipped Actuators Mask*/
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_NA, E_ACT_EQ, E_ACT_EQ, E_ACT_NA, E_ACT_EQ}, /*Actuator State*/
			{
					{   255U,       0U,     255U,     255U,       0U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,       0U,     200U,     200U,       0U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,       0U,       0U,       0U,       0U,       0U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_03  (132H) Actuators' Parameters----------------*/
		{0x28U, /*Equipped Actuators Mask*/
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_NA, E_ACT_NA, E_ACT_NA, E_ACT_EQ, E_ACT_NA, E_ACT_EQ}, /*Actuator State*/
			{
					{     0U,       0U,       0U,     255U,       0U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{     0U,       0U,       0U,     200U,       0U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,       0U,       0U,       0U,       0U,       0U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_04  (133H) Actuators' Parameters----------------*/
		{0x28U, /*Equipped Actuators Mask*/
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_NA, E_ACT_NA, E_ACT_NA, E_ACT_EQ, E_ACT_NA, E_ACT_EQ}, /*Actuator State*/
			{
					{     0U,       0U,       0U,     255U,       0U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{     0U,       0U,       0U,     200U,       0U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,       0U,       0U,       0U,       0U,       0U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_05  (134H) Actuators' Parameters----------------*/
		{0x0DU, /*Equipped Actuators Mask*/
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_NA, E_ACT_EQ, E_ACT_EQ, E_ACT_NA, E_ACT_NA}, /*Actuator State*/
			{
					{   255U,     0U,     255U,       255U,       0U,       0U}, /*Actuator current upper limit in OPER mode*/
					{     0U,     0U,       0U,		    0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,     0U,     200U,       200U,       0U,       0U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,     0U,       0U,         0U,       0U,       0U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_06  (135H) Actuators' Parameters----------------*/
		{0x25U, /*Equipped Actuators Mask*/
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_NA, E_ACT_EQ, E_ACT_NA, E_ACT_NA, E_ACT_EQ}, /*Actuator State*/
			{
					{   255U,     0U,     255U,       0U,       0U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,     0U,       0U,		  0U,		0U,		  0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,     0U,     200U,       0U,       0U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,     0U,       0U,       0U,       0U,       0U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_07  (136H) Actuators' Parameters----------------*/
		{0x2DU, /*Equipped Actuators Mask*/
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_NA, E_ACT_EQ, E_ACT_EQ, E_ACT_NA, E_ACT_EQ}, /*Actuator State*/
			{
					{   255U,       0U,     255U,     255U,       0U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,       0U,     200U,     200U,       0U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,       0U,       0U,       0U,       0U,       0U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_08  (137H) Actuators' Parameters----------------*/
		{0x2DU, /*Equipped Actuators Mask*/
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_NA, E_ACT_EQ, E_ACT_EQ, E_ACT_NA, E_ACT_EQ}, /*Actuator State*/
			{
					{   255U,       0U,     255U,     255U,       0U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,       0U,     200U,     200U,       0U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,       0U,       0U,       0U,       0U,       0U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_09  (138H) Actuators' Parameters----------------*/
		{0x3DU, /*Equipped Actuators Mask*/
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_NA, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ}, /*Actuator State*/
			{
					{   255U,       0U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,       0U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,       0U,       0U,       0U,       0U,       0U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_10  (139H) Actuators' Parameters----------------*/
		{0x3DU, /*Equipped Actuators Mask*/
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_NA, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ}, /*Actuator State*/
			{
					{   255U,       0U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,       0U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,       0U,       0U,       0U,       0U,       0U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_11  (140H) Actuators' Parameters----------------*/
		{0x2FU, /*Equipped Actuators Mask*/
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_NA, E_ACT_EQ}, /*Actuator State*/
			{
					{   255U,     255U,     255U,     255U,       0U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,     200U,     200U,     200U,       0U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,       0U,       0U,       0U,       0U,       0U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_12  (141H) Actuators' Parameters----------------*/
		{0x2FU, /*Equipped Actuators Mask*/
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_NA, E_ACT_EQ}, /*Actuator State*/
			{
					{   255U,     255U,     255U,     255U,       0U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,     200U,     200U,     200U,       0U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,       0U,       0U,       0U,       0U,       0U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_13  (142H) Actuators' Parameters----------------*/
		{0x3FU, /*Equipped Actuators Mask*/
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ}, /*Actuator State*/
			{
					{   255U,      255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,        0U,       0U,		0U,		   0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,      200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,        0U,       0U,       0U,       0U,       0U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_14  (143H) Actuators' Parameters----------------*/
		{0x3FU, /*Equipped Actuators Mask*/
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ}, /*Actuator State*/
			{
					{   255U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,       0U,       0U,       0U,       0U,       0U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_15  (144H) Actuators' Parameters----------------*/
		{0x3FU, /*Equipped Actuators Mask*/
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ}, /*Actuator State*/
			{
					{   255U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,       0U,       0U,       0U,       0U,       0U}  /*Actuator vibration power lower limit in OPER mode*/			}
		},
		/*----------------------CTRY_16  (145H) Actuators' Parameters----------------*/
		{0x3FU, /*Equipped Actuators Mask*/
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ}, /*Actuator State*/
			{
					{   255U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,       0U,       0U,       0U,       0U,       0U}  /*Actuator vibration power lower limit in OPER mode*/			}
		},
		/*----------------------CTRY_17  (146H) Actuators' Parameters----------------*/
		{0x3FU, /*Equipped Actuators Mask*/
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ}, /*Actuator State*/
			{
					{   255U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,       0U,       0U,       0U,       0U,       0U}  /*Actuator vibration power lower limit in OPER mode*/			}
		},
		/*----------------------CTRY_18  (147H) Actuators' Parameters----------------*/
		{0x3FU, /*Equipped Actuators Mask*/
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ}, /*Actuator State*/
			{
					{   255U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,       0U,       0U,       0U,       0U,       0U}  /*Actuator vibration power lower limit in OPER mode*/			}
		},
		/*----------------------CTRY_19  (148H) Actuators' Parameters----------------*/
		{0x3FU, /*Equipped Actuators Mask*/
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ}, /*Actuator State*/
			{
					{   255U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,       0U,       0U,       0U,       0U,       0U}  /*Actuator vibration power lower limit in OPER mode*/			}
		},
		/*----------------------CTRY_20  (149H) Actuators' Parameters----------------*/
		{0x3FU, /*Equipped Actuators Mask*/
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ}, /*Actuator State*/
			{
					{   255U,     255U,     255U,     255U,     255U,     255U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,       0U,		0U,		  0U,		0U}, /*Actuator current lower limit in OPER mode*/
					{   200U,     200U,     200U,     200U,     200U,     200U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,       0U,       0U,       0U,       0U,       0U}  /*Actuator vibration power lower limit in OPER mode*/			}
		},
};

uint8_t g_ActLimIbit_Vib_Max  = 0U;                 /*! Specifies  actuator's IBIT limit for Maximal Vibration parameter*/
uint8_t g_ActLimIbit_Vib_Min  = 0U;                 /*! Specifies  actuator's IBIT limit for Minimal Vibration parameter*/
uint8_t g_ActLimIbit_Curr_Max = 0U;                 /*! Specifies  actuator's IBIT limit for Maximal Current parameter*/
uint8_t g_ActLimIbit_Curr_Min = 0U;                 /*! Specifies  actuator's IBIT limit for Minimal Current parameter*/
/* -----External variables ---------------------------------------------------------*/

/* -----Static Function prototypes -------------------------------------------------*/

/* -----Modules implementation -----------------------------------------------------*/

/*************************************************************************************
** Set_Actuator_Limits - Sets required actuator's limits to system parameters table
**
** Params : uint8_t Lim_Mode_Type - Actuator limits type per Mode(OPER/IBIT)
** 			 e_ActuatorsNum ActNum - Actuator's number (0-5)
** 			 uint8_t Vib_Lim_Max -  Actuator's vibration maximal limit
** 			 uint8_t Vib_Lim_Min -  Actuator's vibration minimal limit
** 			 uint8_t Curr_Lim_Max - Actuator's current maximal limit
** 			 uint8_t Curr_Lim_Min - Actuator's current minimal limit
**
** Returns: None
** Notes:
*************************************************************************************/
void Set_Actuator_Limits(uint8_t Lim_Mode_Type, e_ActuatorsNum ActNum, uint8_t Vib_Lim_Max, uint8_t Vib_Lim_Min, uint8_t Curr_Lim_Max, uint8_t Curr_Lim_Min)
{
	/*Check input parameters*/
	if((CTRY_MAX_NUM > Get_CTRY_ID()) && (E_ACT_MAX_NUM > ActNum))
	{
		if(ACT_LIM_OPER_MODE == Lim_Mode_Type)
		{
			SysCtryParamTable[Get_CTRY_ID()].stActLimits.AccelPowMax[(uint8_t)ActNum] = Vib_Lim_Max;
			SysCtryParamTable[Get_CTRY_ID()].stActLimits.AccelPowMin[(uint8_t)ActNum] = Vib_Lim_Min;
			SysCtryParamTable[Get_CTRY_ID()].stActLimits.CurrentMax[(uint8_t)ActNum] = Curr_Lim_Max;
			SysCtryParamTable[Get_CTRY_ID()].stActLimits.CurrentMin[(uint8_t)ActNum] = Curr_Lim_Min;
		}
	}
}

/*************************************************************************************
** Get_Actuator_State - Gets required actuator's state from system parameters table
**
** Params : uint8_t Ctry_Id - CTRY's ID value (0-19)
** 			 e_ActuatorsNum ActNum - Actuator's number (0-5)
** 			 e_ActuatorState *Act_State - pointer to actuator's state (EQ/NA)
**
** Returns: HAL_StatusTypeDef - Method's status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef Get_Actuator_State(uint8_t Ctry_Id, e_ActuatorsNum ActNum, e_ActuatorState *Act_State)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	/*Check input parameters*/
	if((CTRY_MAX_NUM > Ctry_Id) && (E_ACT_MAX_NUM > ActNum) && (NULL != Act_State))
	{
		/*Pass actuator's state as output parameter*/
		*Act_State = SysCtryParamTable[(uint8_t)Ctry_Id].ActState[(uint8_t)ActNum];
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	/*Return status*/
	return hal_status;
}

/*************************************************************************************
** Get_Actuator_CurrLimits - Gets required actuator's current limits values from system parameters table
**
** Params : uint8_t Ctry_Id - CTRY's ID value (0-19)
** 			 e_ActuatorsNum ActNum - Actuator's number (0-5)
** 			 uint8_t *CurrLim_Max - pointer to actuator's max speed value (0-255)
** 			 uint8_t *CurrLim_Min - pointer to actuator's min speed value (0-255)
**
** Returns: HAL_StatusTypeDef - Method's status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef Get_Actuator_CurrLimits(uint8_t Ctry_Id, e_ActuatorsNum ActNum, uint8_t *CurrLim_Max, uint8_t *CurrLim_Min)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	/*Check input parameters*/
	if((CTRY_MAX_NUM > Ctry_Id) && (E_ACT_MAX_NUM > ActNum) && (NULL != CurrLim_Max) && (NULL != CurrLim_Min))
	{
		/*Pass actuator's current limits as output parameter*/
		*CurrLim_Max = SysCtryParamTable[(uint8_t)Ctry_Id].stActLimits.CurrentMax[(uint8_t)ActNum];
		*CurrLim_Min = SysCtryParamTable[(uint8_t)Ctry_Id].stActLimits.CurrentMin[(uint8_t)ActNum];
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	/*Return status*/
	return hal_status;
}
/*************************************************************************************
** Get_Actuator_VibrLimits - Gets required actuator's vibration limits values from system parameters table
**
** Params : uint8_t Ctry_Id - CTRY's ID value (0-19)
** 			 e_ActuatorsNum ActNum - Actuator's number (0-5)
** 			 uint8_t *VibrLim_Max - pointer to actuator's max vibration value (0-255)
** 			 uint8_t *VibrLim_Min - pointer to actuator's min vibration value (0-255)
**
** Returns: HAL_StatusTypeDef - Method's status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef Get_Actuator_VibrLimits(uint8_t Ctry_Id, e_ActuatorsNum ActNum, uint8_t *VibrLim_Max, uint8_t *VibrLim_Min)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	/*Check input parameters*/
	if((CTRY_MAX_NUM > Ctry_Id) && (E_ACT_MAX_NUM > ActNum) && (NULL != VibrLim_Max) && (NULL != VibrLim_Min))
	{
		/*Pass actuator's vibration limits as output parameter*/
		*VibrLim_Max = SysCtryParamTable[(uint8_t)Ctry_Id].stActLimits.AccelPowMax[(uint8_t)ActNum];
		*VibrLim_Min = SysCtryParamTable[(uint8_t)Ctry_Id].stActLimits.AccelPowMin[(uint8_t)ActNum];
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	/*Return status*/
	return hal_status;
}
/*************************************************************************************
** Init_Ctrys_Status_Table - Initialization of CTRYs' initial status.
**
** Params : None.
**
** Returns: None.
** Notes:  Init CTRY status as E_CTRY_OK, each CTRY's actuators status as E_ACT_OK
** 			and all CTRY's actuators' status as 0x00.
*************************************************************************************/
void Init_Ctrys_Status_Table(void)
{
	for(uint8_t i = 0U; i < CTRY_MAX_NUM; i++)
	{
		SysCtryStatusTable[i].CtryStatus = E_CTRY_OK;
		SysCtryStatusTable[i].CtryActuatorsStatus = 0x00U;
		for(uint8_t act_num = 0U; act_num < (uint8_t)E_ACT_MAX_NUM; act_num++)
		{
			SysCtryStatusTable[i].ActStatus[act_num] = E_ACT_OK;
		}
	}
}
/*************************************************************************************
** Get_Actuator_Status - Gets required actuator's status from system status table
**
** Params : uint8_t Ctry_Id - CTRY's ID value (0-19)
** 			 e_ActuatorsNum ActNum - Actuator's number (0-5)
** 			 e_ActuatorStatus *Act_Status - pointer to actuator's status (OK/FL)
**
** Returns: HAL_StatusTypeDef - Method's status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef Get_Actuator_Status(uint8_t Ctry_Id, e_ActuatorsNum ActNum, e_ActuatorStatus *Act_Status)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	/*Check input parameters*/
	if((CTRY_MAX_NUM > Ctry_Id) && (E_ACT_MAX_NUM > ActNum) && (NULL != Act_Status))
	{
		/*Pass actuator's status as output parameter*/
		*Act_Status = SysCtryStatusTable[(uint8_t)Ctry_Id].ActStatus[(uint8_t)ActNum];
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	/*Return status*/
	return hal_status;
}

/*************************************************************************************
** Get_CTRY_All_Actuators_Status - Gets required CTRY's all actuators' status from system status table
**
** Params : uint8_t Ctry_Id - CTRY's ID value (0-19)
** 			 uint8_t *All_Act_Status - pointer to CTRY's all actuators status (0x00-0x3F)
**
** Returns: HAL_StatusTypeDef - Method's status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef Get_CTRY_All_Actuators_Status(uint8_t Ctry_Id, uint8_t *All_Act_Status)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	/*Check input parameters*/
	if((CTRY_MAX_NUM > Ctry_Id) &&  (NULL != All_Act_Status))
	{
		/*Pass CTRY's all actuators' status as output parameter*/
		*All_Act_Status = SysCtryStatusTable[(uint8_t)Ctry_Id].CtryActuatorsStatus;
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	/*Return status*/
	return hal_status;
}

/*************************************************************************************
** Get_CTRY_Status - Gets required CTRY's status from system status table
**
** Params : uint8_t Ctry_Id - CTRY's ID value (0-19)
**
** Returns: e_CtryStatus - CTRY's status
** Notes:
*************************************************************************************/
e_CtryStatus Get_CTRY_Status(uint8_t Ctry_Id)
{
	e_CtryStatus ctry_status = E_CTRY_FL;
	/*Check input parameters*/
	if(CTRY_MAX_NUM > Ctry_Id)
	{
		ctry_status = SysCtryStatusTable[(uint8_t)Ctry_Id].CtryStatus;
	}

	return ctry_status;
}
/*************************************************************************************
** Set_Actuator_Status_FL - Sets required actuator's status 'FL' into system status table
**
** Params : uint8_t Ctry_Id - CTRY's ID value (0-19)
** 			 e_ActuatorsNum ActNum - Actuato r's number (0-5)
**
** Returns: HAL_StatusTypeDef - Method's status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef Set_Actuator_Status_FL(uint8_t Ctry_Id, e_ActuatorsNum ActNum)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	uint8_t act_num_mask = 0x01U;

	/*Check input parameters*/
	if((CTRY_MAX_NUM > Ctry_Id) && (E_ACT_MAX_NUM > ActNum))
	{
		/*Set actuator's status into status table*/
		SysCtryStatusTable[(uint8_t)Ctry_Id].ActStatus[(uint8_t)ActNum] = E_ACT_FL;

		/*Update  CTRY's actuators status value into status table*/
		act_num_mask = act_num_mask << (uint8_t)ActNum;
		SysCtryStatusTable[Ctry_Id].CtryActuatorsStatus  = SysCtryStatusTable[Ctry_Id].CtryActuatorsStatus  | act_num_mask;

		/*Check if all CTRY's actuators are 'FL' -> Mark CTRY as 'FL'*/
		if((SysCtryStatusTable[Ctry_Id].CtryActuatorsStatus & SysCtryParamTable[Ctry_Id].CTRY_Eq_Act_Mask) == SysCtryParamTable[Ctry_Id].CTRY_Eq_Act_Mask)
		{
			SysCtryStatusTable[Ctry_Id].CtryStatus = E_CTRY_FL;
		}
	}
	else
	{
		hal_status = HAL_ERROR;
	}

	/*Return status*/
	return hal_status;
}

/*************************************************************************************
** Set_CTRY_Status - Sets required CTRY's status into system status table
**
** Params : uint8_t Ctry_Id - CTRY's ID value (0-19)
** 			 e_CtryStatus Ctry_Status - CTRY's status (OK/FL)
**
** Returns: HAL_StatusTypeDef - Method's status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef Set_CTRY_Status(uint8_t Ctry_Id, e_CtryStatus Ctry_Status)
{
	HAL_StatusTypeDef hal_status = HAL_OK;

	/*Check input parameters*/
	if((CTRY_MAX_NUM > Ctry_Id) &&  ((E_CTRY_OK == Ctry_Status) || (E_CTRY_FL == Ctry_Status)))
	{
		/*Update CTRY's status only to FL status (latch FL status)*/
		if(E_CTRY_FL != SysCtryStatusTable[Ctry_Id].CtryStatus)
		{
			/*Set CTRY's status into status table*/
			SysCtryStatusTable[(uint8_t)Ctry_Id].CtryStatus = Ctry_Status;
		}
		/*In case that CTRY is FL - mark all CTRY's actuators as FL*/
		if(E_CTRY_FL == Ctry_Status)
		{
			Set_CTRY_All_Actuators_Status_FL(Ctry_Id);
		}
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	/*Return status*/
	return hal_status;
}

/*************************************************************************************
** Set_CTRY_All_Actuators_Status_FL - Sets required CTRY's all actuators' status FL into system status table
**
** Params : uint8_t Ctry_Id - CTRY's ID value (0-19)
**
** Returns: None
** Notes:
*************************************************************************************/
void Set_CTRY_All_Actuators_Status_FL(uint8_t Ctry_Id)
{
	/*Check input parameters*/
	if(CTRY_MAX_NUM > Ctry_Id)
	{
		/*Set CTRY's all actuators' status into status table*/
		SysCtryStatusTable[Ctry_Id].CtryActuatorsStatus = SysCtryParamTable[Ctry_Id].CTRY_Eq_Act_Mask;
		/*Set CTRY's status as 'FL' into status table*/
		SysCtryStatusTable[Ctry_Id].CtryStatus = E_CTRY_FL;

		/*Update CTRY's actuators' status in status table*/
		for(uint8_t act_num = 0U; act_num < (uint8_t)E_ACT_MAX_NUM; act_num++)
		{
			SysCtryStatusTable[Ctry_Id].ActStatus[act_num] = (e_ActuatorStatus)((SysCtryStatusTable[Ctry_Id].CtryActuatorsStatus >> act_num) & 0x01U);
		}
	}
}
/*************************************************************************************
** Monitor_Status_Table - Scans status table values
**
** Params : None
**
** Returns: e_MonitTableStatus - Table monitoring status
** Notes:
*************************************************************************************/
e_MonitTableStatus Monitor_Status_Table(void)
{
	e_MonitTableStatus monit_table_status = E_MON_TBL_OK;
	uint8_t fl_ctry_counter = 0U;
	uint8_t fl_act_counter = 0U;

	/*Scan entire status table*/
	for(uint8_t ctry_num = 0U; ctry_num < CTRY_MAX_NUM; ctry_num++)
	{
		/*Check CTRY's status*/
		if(E_CTRY_FL == SysCtryStatusTable[ctry_num].CtryStatus)
		{
			/*Increment fail CTRY counter*/
			fl_ctry_counter++;
		}
		/*Check current CTRY's actuators' status*/
		for(uint8_t act_num = 0U; act_num < (uint8_t)E_ACT_MAX_NUM; act_num++)
		{
			if(E_ACT_FL == SysCtryStatusTable[ctry_num].ActStatus[act_num])
			{
				/*Increment fail actuator counter*/
				fl_act_counter++;
			}
		}
	}
	/*Check if number of failed CTRs/Actuators exceed the limit or reading status table failed*/
	if((FL_CTRY_MAX_NUM < fl_ctry_counter) || (FL_ACTUATORS_MAX_NUM < fl_act_counter))
	{
		monit_table_status = E_MON_TBL_FL;
	}
	return monit_table_status;
}
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
