/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : module_init_ctry.c
** Author    : Leonid Savchenko
** Revision  : 1.4
** Updated   : 11-07-2021
**
** Description: This file contains procedures for initialization and power-up tests
** 				of CTRY unit.
*/


/* Revision Log:
**
** Rev 1.0  : 19-10-2020 -- Original version created.
** Rev 1.1  : 20-11-2020 -- Static analysis update
** Rev 1.2  : 22-06-2021 -- Integration update
** Rev 1.3  : 30-06-2021 -- Added CAN_Stop()
** Rev 1.4  : 11-07-2021 -- Added doxygen comments
**************************************************************************************
*/


/* -----Includes ---------------------------------------------------------------------*/

#include "module_init_ctry.h"
#include "main.h"
#include "gipsy_hal_rcc.h"
#include "actuator_ctrl.h"
#include "current_sensor.h"
#include "flash_inegrity_test.h"
#include "gpio_control_ctry.h"
#include "id_conf_ctry.h"
#include "mcp9804.h"
#include "mcu_config_ctry.h"
#include "motor_driver.h"
#include "system_params_ctry.h"
#include "vibration_meas.h"
#include "voltage_meas_ctry.h"
#include "wdog.h"
#include "fault_mngmnt_ctry.h"
#include "gipsy_hal_can.h"

/* -----Definitions ----------------------------------------------------------------*/
#define  WDOG_TRIGGER_ON_FAULT_DELAY_MILLIS    (uint32_t)100U


#define PBIT_CURR_LIM_MIN      (uint8_t)0U   /*0A*/
#define PBIT_CURR_LIM_MAX      (uint8_t)20U  /*2A*/


#define FLT_LED_CRC_FAILURE_TOGGLE_PERIOD_MILIS           500U  /*msec*/
#define FLT_LED_WDG_FAILURE_TOGGLE_PERIOD_MILIS           1000U /*msec*/

#define POWER_SWITCH_ACTIVATION_DELAY_MILIS               100U  /*msec*/
/* -----Macros ---------------------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/
extern BoardTempSens_t g_BoardTempSens[E_CTRY_NUM_OF_BOARDS];
/* -----Global variables -----------------------------------------------------------*/
/*!Specifies CTRY general parameters (Range: var type | Units: see var declaration)*/
/*!\sa ModuleMode - Set function: CTRY_Fault_Management(), CTRY_Module_Init(), pbit_fault_management(), perform_pbit()*/
/*!\sa ModuleMode - Get function: Get_Module_Mode()*/
/*!\sa Pbit_Status - Set function: pbit_fault_management()*/
/*!\sa Pbit_Status - Get function: CAN_Message_Transmit(), Manage_OnBoard_Leds()*/
/*!\sa Cbit_Status - Set function: CTRY_Fault_Management()*/
/*!\sa Cbit_Status - Get function: CAN_Message_Transmit(), Manage_OnBoard_Leds()*/
/*!\sa Pbit_Failure_Desc - Set function: perform_pbit()*/
/*!\sa Pbit_Failure_Desc - Get function: CAN_Message_Transmit(), CTRY_Fault_Management(), CTRY_Module_Init(), pbit_fault_management(), perform_pbit()*/
/*!\sa Ctry_Failure_Desc - Set function: Actuators_Activation_Management(), CAN_Message_Transmit(), CTRY_Temperature_Management(), Increment_SW_Error_Counter(),
 *  monitor_actuator_current_and_vibration(), Perform_CBIT(), Periodic_Tasks_Scheduler(), Received_Actuators_Params_Sanity_Monitoring()*/
/*!\sa Ctry_Failure_Desc - Get function: CAN_Message_Transmit(), CTRY_Fault_Management()*/
/*!\sa Flt_Ch_C_Status - Set function: Activate_Fault_Channel_C(), HAL_CAN_RxFifo0MsgPendingCallback() */
/*!\sa Flt_Ch_C_Status - Get function: CAN_Message_Transmit(), Manage_OnBoard_Leds()*/
ModuleParameters_st g_ModuleParams =
{
		.ModuleMode = E_INIT_MODE,
		.Pbit_Status = STATUS_OK,
		.Cbit_Status = STATUS_OK,
		.Pbit_Failure_Desc = 0x0000U,
		.Ctry_Failure_Desc = 0x0000U,
		.Flt_Ch_C_Status = STATUS_OK
};
/* -----Static Function prototypes -------------------------------------------------*/
static void perform_pbit(void);
static void pbit_fault_management(void);
/* -----Modules implementation -----------------------------------------------------*/

/*************************************************************************************
** Get_Module_Mode - Gets CTRY's mode
**
** Params : None
**
** Returns: e_ModuleMode - CTRY's mode
** Notes:
*************************************************************************************/
e_ModuleMode Get_Module_Mode(void)
{
	return g_ModuleParams.ModuleMode;
}

/*************************************************************************************
** CTRY_Module_Init - Initialization mode of CTRY. Performs MCU configuration, CTRY's
** 					  peripherals configuration, WDOG/CRC tests, PBIT.
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
void CTRY_Module_Init(void)
{
	uint32_t crc_failure_last_toggle_exec = 0U;
	uint32_t wdg_failure_last_toggle_exec = 0U;
	uint32_t failure_wdg_trigger_exec     = 0U;

	/*Initialize MCU*/
	MCU_Periph_Config();


	/*Perform Flash integrity test*/
	if(E_CRC_TEST_OK != Flash_Integrity_Test())
	{
#ifndef _FLASH_TEST_DISABLE_
		/*In case of CRC Test failure:
		 * 1.) Activate CH_C output
		 * 2.) Toggle Failure LED (LED2)
		 * 3.) Stuck the operational code execution
		 */
		Activate_Fault_Channel_C();
		while(TRUE)
		{
			if(WDOG_TRIGGER_ON_FAULT_DELAY_MILLIS <= (HAL_GetTick() - failure_wdg_trigger_exec))
			{
				WDOG_Trigger();
				failure_wdg_trigger_exec = HAL_GetTick();
			}
			if(FLT_LED_CRC_FAILURE_TOGGLE_PERIOD_MILIS <= (HAL_GetTick() - crc_failure_last_toggle_exec))
			{
				HAL_GPIO_TogglePin(LED_PORT,FLT_LED_PIN);
				crc_failure_last_toggle_exec = HAL_GetTick();
			}
		}
#endif
	}
#ifndef _WDOG_TEST_DISABLE_
	/*Perform Watch-Dog test*/
	WDOG_Perform_Test();
	if(E_WDOG_TEST_PASSED != WDOG_Get_Test_Result())
	{
		/*In case of WDOG Test failure:
		 * 1.) Activate CH_C output
		 * 2.) Toggle Failure LED (LED2)
		 * 3.) Stuck the operational code execution
		 */
		Activate_Fault_Channel_C();
		while(TRUE)
		{
			if(WDOG_TRIGGER_ON_FAULT_DELAY_MILLIS <= (HAL_GetTick() - failure_wdg_trigger_exec))
			{
				WDOG_Trigger();
				failure_wdg_trigger_exec = HAL_GetTick();
			}
			if(FLT_LED_WDG_FAILURE_TOGGLE_PERIOD_MILIS <= (HAL_GetTick() - wdg_failure_last_toggle_exec))
			{
				HAL_GPIO_TogglePin(LED_PORT,FLT_LED_PIN);
				wdg_failure_last_toggle_exec = HAL_GetTick();
			}
		}
	}
#endif

	Init_Ctrys_Status_Table(); /*Initialize status table with default parameters*/

	/*Initialize CTRY peripherals*/

	/*Configure temperature sensors' parameters*/
	if(0x00U != Temperature_Sensors_Config())
	{
		g_ModuleParams.Pbit_Failure_Desc |= PBIT_FLT_TEMP_SENSORS;
	}
	/*Configure accelerometer parameters*/
	if(HAL_OK != Init_Accelerometer())
	{
		g_ModuleParams.Pbit_Failure_Desc |= PBIT_FLT_ACCELEROMETER;
	}

	perform_pbit();
    pbit_fault_management();
    if(E_SAFE_MODE != Get_Module_Mode())
    {
    	g_ModuleParams.ModuleMode = E_STBY_MODE;
    }
}

/*************************************************************************************
** perform_pbit - Performs CTRY's PBIT
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
static void perform_pbit(void)
{
	e_ActuatorState act_state = E_ACT_NA;
    HAL_StatusTypeDef hal_status = HAL_OK;

	/*Read CTRY ID value from external input*/
	if(E_CTRY_ID_OK != HI8429_Init())
	{
		g_ModuleParams.Pbit_Failure_Desc |= PBIT_FLT_CTRY_ID;
		g_ModuleParams.ModuleMode = E_SAFE_MODE;
	}
	/*Check current consumption during initialization phase*/
	if(E_CURR_TEST_FL == Test_Current_Consumption(PBIT_CURR_LIM_MAX,PBIT_CURR_LIM_MIN))
	{
		g_ModuleParams.Pbit_Failure_Desc |= PBIT_FLT_CURR_CONSUMPTION;
	}

	/*Check 14.8V Input voltage source*/
	if(E_VOLT_TEST_FL == Test_OnBoard_Voltage(E_VOLT_14_8V_IN))
	{
		g_ModuleParams.Pbit_Failure_Desc |= PBIT_FLT_POWER_IN;
	}

	/*Check 5V voltage source*/
	if(E_VOLT_TEST_FL == Test_OnBoard_Voltage(E_VOLT_5V))
	{
		g_ModuleParams.Pbit_Failure_Desc |= PBIT_FLT_POWER_5V;
	}

	/*Check Power switch by sampling 14.8V MDRV voltage source*/
	Set_Motor_PWR_EN(E_GPIO_PIN_RESET);/*Turn OFF the power switch*/
	HAL_Delay(POWER_SWITCH_ACTIVATION_DELAY_MILIS);
	if(E_VOLT_TEST_FL == Test_OnBoard_Voltage(E_VOLT_14_8_MDRV_OFF))/*Test MDRV voltage when switch is OFF*/
	{
		g_ModuleParams.Pbit_Failure_Desc |= PBIT_FLT_POW_SWITCH;
	}
	Set_Motor_PWR_EN(E_GPIO_PIN_SET);/*Turn ON the power switch*/
	HAL_Delay(POWER_SWITCH_ACTIVATION_DELAY_MILIS);
	if(E_VOLT_TEST_FL == Test_OnBoard_Voltage(E_VOLT_14_8_MDRV_ON))/*Test MDRV voltage when switch is ON*/
	{
		g_ModuleParams.Pbit_Failure_Desc |= PBIT_FLT_POW_SWITCH;
	}

	/*Check temperature sensors by performing temperature management*/
	/*If all temperature sensors  failed ->  Set Temperature Sensors FLT*/
	if(0xFFU == Temperaure_Sensors_Read(g_BoardTempSens))/*Read all Temperature Sensors*/
	{
		g_ModuleParams.Pbit_Failure_Desc |= PBIT_FLT_TEMP_SENSORS;
	}


	/*Check accelerometer's serial interface*/
	if(0x0000U == (g_ModuleParams.Pbit_Failure_Desc & PBIT_FLT_ACCELEROMETER))
	{
		if(HAL_OK != Check_Accelerometer_ID())
		{
			g_ModuleParams.Pbit_Failure_Desc |= PBIT_FLT_ACCELEROMETER;
		}
	}

	/*Configure motor drivers' parameters and check serial interface*/
	if(E_SAFE_MODE != Get_Module_Mode())
	{
		MDRV_Wake_All(); /*Wake all Motor driver due to wake delay*/
		HAL_Delay(50U); /*Wait for motor drivers to wake up*/
		for(uint8_t mdrv_num = 0U; mdrv_num < MAX_NUM_OF_MOTOR_DRIVERS; mdrv_num++)
		{
			/*Check if current actuator is equipped*/
			if(HAL_OK == Get_Actuator_State(Get_CTRY_ID(), (e_ActuatorsNum)mdrv_num, &act_state))
			{
				if(E_ACT_EQ == act_state)
				{
					if(E_MDRV_FL == MDRV_Config(mdrv_num))
					{
						/*Mark failed motor driver*/
						hal_status |= Set_Actuator_Status_FL(Get_CTRY_ID(),(e_ActuatorsNum)mdrv_num);
						g_ModuleParams.Pbit_Failure_Desc |= ((uint16_t)PBIT_FLT_MDRV_BASE << mdrv_num);
					}
				}
			}
		}
	}
	if(HAL_OK != hal_status)
	{
		/*Input parameters failure -> increment SW error counter*/
		Increment_SW_Error_Counter();
	}

	/*Perform CAN interface initialization and loop-back test*/
	if(E_SAFE_MODE != Get_Module_Mode())
	{
		if(HAL_OK != MCU_CAN_Interface_Init())
		{
			/*In case that CAN initialization or loop-back test failed - Mark mode as SAFE*/
			g_ModuleParams.Pbit_Failure_Desc |= PBIT_FLT_CAN_BUS;
			HAL_CAN_Stop();
		}
	}
}
/*************************************************************************************
** pbit_fault_management - Check all init and PBIT critical failures and perform fault management
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
static void pbit_fault_management(void)
{
	HAL_StatusTypeDef hal_status = HAL_OK;


	if((E_SAFE_MODE == Get_Module_Mode()) || \
	   (PBIT_FLT_CAN_BUS == (g_ModuleParams.Pbit_Failure_Desc & PBIT_FLT_CAN_BUS)) ||\
	   (PBIT_FLT_POWER_IN == (g_ModuleParams.Pbit_Failure_Desc & PBIT_FLT_POWER_IN)) ||\
	   (PBIT_FLT_POWER_5V == (g_ModuleParams.Pbit_Failure_Desc & PBIT_FLT_POWER_5V)) ||\
	   (PBIT_FLT_POW_SWITCH == (g_ModuleParams.Pbit_Failure_Desc & PBIT_FLT_POW_SWITCH)) ||\
	   (PBIT_FLT_ACCELEROMETER == (g_ModuleParams.Pbit_Failure_Desc & PBIT_FLT_ACCELEROMETER)) ||\
	   (PBIT_FLT_CURR_CONSUMPTION == (g_ModuleParams.Pbit_Failure_Desc & PBIT_FLT_CURR_CONSUMPTION)))
	{
		/*Critical failure detected*/
		g_ModuleParams.ModuleMode = E_SAFE_MODE;
		g_ModuleParams.Pbit_Status = STATUS_FL; /*Mark PBIT status as FAIL*/
		Activate_Fault_Channel_C();
		MDRV_Shut_Down(MAX_NUM_OF_MOTOR_DRIVERS); /*Disable all motor drivers*/
		/*Update CTRY status to FL in case that CTRY ID is valid*/
		if(0x0000U == (g_ModuleParams.Pbit_Failure_Desc & PBIT_FLT_CTRY_ID))
		{
			/*Critical failure detected - mark CTRY status as FL and all actuators as FL*/
			hal_status = Set_CTRY_Status(Get_CTRY_ID(),E_CTRY_FL);
		}
		Set_Motor_PWR_EN(E_GPIO_PIN_RESET); /*Turn OFF power switch*/
	}
	else
	{
		/*No critical failure detected - mark CTRY status as OK*/
		hal_status = Set_CTRY_Status(Get_CTRY_ID(),E_CTRY_OK);
		g_ModuleParams.Pbit_Status = STATUS_OK; /*Mark PBIT status as OK*/
	}
	if(HAL_OK != hal_status)
	{
		/*Input parameters failure -> increment SW error counter*/
		Increment_SW_Error_Counter();
	}
}
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
