/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : main.c
** Author    : Leonid Savchenko
** Revision  : 1.2
** Updated   : 23-12-2020
**
** Description: This file provides the application entry point routine. In addition,
** 				includes system initialization routine.
*/


/* Revision Log:
**
** Rev 1.0  : 10-11-2020 -- Original version created.
** Rev 1.1  : 20-11-2020 -- Static analysis update
** Rev 1.2  : 23-12-2020 -- Updates due to verification remarks
**************************************************************************************
*/
/* -----Includes -------------------------------------------------------------------*/
#include "main.h"
#include "mcu_config_ctry.h"
#include "module_init_ctry.h"
#include "periodic_ctry.h"
/* -----Definitions ----------------------------------------------------------------*/

#define VECT_TAB_OFFSET  0x00U /*!< Vector Table base offset field.This value must be a multiple of 0x200. */

/* -----Macros ---------------------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/


/* -----Static Function prototypes -------------------------------------------------*/

/* -----Modules implementation -----------------------------------------------------*/


/*************************************************************************************
** main - The application entry point
**
**
** Params : None.
**
** Returns: int
** Notes:
*************************************************************************************/

int main(void)
{

	CTRY_Module_Init();

	while (TRUE)
	{
		Periodic_Tasks_Scheduler();
	}

	return 0;
}

/*************************************************************************************
** SystemInit - Setup the microcontroller system: Initialize the FPU setting,
**              vector table location and External memory configuration.
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
  void SystemInit(void)
  {
	  /* FPU settings ------------------------------------------------------------*/
	  SCB->CPACR |= (uint32_t)((3U << (10U*2U))|(3U << (11U*2U)));  /* set CP10 and CP11 Full Access */

	  /* Reset the RCC clock configuration to the default reset state ------------*/
	  /* Set HSION bit */
	  RCC->CR |= (uint32_t)0x00000001U;

	  /* Reset CFGR register */
	  RCC->CFGR = (uint32_t)0x00000000U;

	  /* Reset HSEON, CSSON and PLLON bits */
	  RCC->CR &= (uint32_t)0xFEF6FFFFU;

	  /* Reset PLLCFGR register */
	  RCC->PLLCFGR = (uint32_t)0x24003010U;

	  /* Reset HSEBYP bit */
	  RCC->CR &= (uint32_t)0xFFFBFFFFU;

	  /* Disable all interrupts */
	  RCC->CIR = (uint32_t)0x00000000U;

	  /* Configure the Vector Table location add offset address ------------------*/
	  SCB->VTOR = (uint32_t)FLASH_BASE | (uint32_t)VECT_TAB_OFFSET; /* Vector Table Relocation in Internal FLASH */

  }

/*************************************************************************************
** Error_Handler - This function is executed in case of fatal error occurrence
**
** Params : None.
**
** Returns: None.
** Notes:
*************************************************************************************/

void Error_Handler(void)
{

  while(TRUE)
  {
	  __NOP();
  }

}


/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
